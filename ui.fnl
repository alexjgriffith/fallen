(local bump (require "lib.bump"))

;; debug hit boxes
;;(local bump-dev-only (require "bump-plugin"))

(local default-element
       {:visible true :x 0 :y 0 :w 1 :h 1 :default-callback (fn [_element])})

(local scene {})

(fn scene.click [scene x y]
    (local [elements len] (: scene.world :queryPoint (/ x 5) (/ y 5)))
    ;; elements is not in an array if len = nil = 1 !!
    (when elements
      (if len
          (each [key element (pairs elements)]
                (tset scene :elements element.name :clicked true))
          (do           
           (tset scene :elements elements.name :clicked true)))
          ))

(fn scene.hover [scene x y]
    (local [elements len] (: scene.world :queryPoint (/ x 5) (/ y 5)))
    ;; elements is not in an array if len = nil = 1 !!
    (when elements
      (if len
          (each [key element (pairs elements)]
                (tset scene :elements element.name :hovered true))
          (do
           (tset scene :elements elements.name :hovered true)))))

(fn scene.draw [scene]
    (each [name element (pairs scene.elements)]
          (when element.visible
            (if element.clicked (element.click-draw element)
                element.hovered (element.hover-draw element)
                (element.default-draw element)))
          (tset element :hovered false)
          (tset element :clicked false))
    (love.graphics.setColor 0 1 1 1)
    ;;(bump-dev-only.draw (: scene.world :getItems) scene.world 0 0 1 1 )
    (love.graphics.setColor 1 1 1 1)
)

(fn scene.update [scene dt]
    (local [x y]  [(love.mouse.getPosition)])
    (scene.hover scene x y)
    (each [name element (pairs scene.elements)]
          (when element.set-scene-inactive
            (tset scene :active false)
            (tset element :set-scene-inactive false)
            )
          (when element.set-scene-active
            (tset scene :active true)
            (tset element :set-scene-active false)
            )
          (if
           element.clicked (element.click-callback element dt)
           element.hovered (element.hover-callback element dt)
           (element.default-callback element dt))))

(fn scene.on [scene]
    (tset scene :active true))

(fn scene.off [scene]
    (tset scene :active false))

(fn scene.add [scene params]
    (local name params.name)
    (local element 
                   {:visible (or params.visible default-element.visible)
                    :x (or params.x default-element.x)
                    :y (or params.y default-element.y)
                    :w (or params.w default-element.w)
                    :h (or params.h default-element.h)
                    :set-scene-inactive false
                    :set-scene-active false
                    :name name ;; required
                    :id params.id ;; required
                    :time 0
                    :hovered false
                    :hovered-count 0
                    :clicked false                    
                    :default-callback (or params.default-callback default-element.default-callback)
                    :default-draw params.default-draw ;; required
                    :hover-callback (or params.hover-callback (or params.default-callback default-element.default-callback))
                    :hover-draw (or params.hover-draw params.default-draw)
                    :click-callback params.click-callback ;; required
                    :click-draw (or params.click-draw params.default-draw)
                    })
    (each [key value (pairs params)]
          (when (not (. element key))
            (tset element :key value)))
    (each [key callback (pairs (or params.keymap {}))]
          (tset scene :keymap key callback))
    (tset scene :elements name element)
    (: scene.world :add element element.x element.y element.w element.h))

(fn scene.remove-element [scene name]
    (local element (. scene :elements name))
    (. scene.world :remove element)
    (tset scene :element name nil))


(local scenemt {:__index scene })

(local ui {:scenes {}})

(fn ui.update [dt]
    (each [_ scene (pairs ui.scenes)]
          (when scene.active
            (scene.update scene dt))))

(fn ui.draw [?tx ?ty ?sx ?sy]
    (love.graphics.push)
    (love.graphics.translate (math.floor(or ?tx 0))
                             (math.floor(or ?ty 0)))
    (love.graphics.scale  (or ?sx 1) (or ?sy (or ?sx 1)))
    (each [_ scene (pairs ui.scenes)]          
          (when scene.active
            (scene.draw scene)))
    (love.graphics.pop))

(fn ui.new-scene [name elements]
    (local scene (setmetatable
                  {:active false
                    :keymap {}
                    :elements {}
                    :world (bump.newWorld)}
                   scenemt))
    (each [name params (pairs elements)]
          (scene.add scene params))
    (tset ui :scenes name scene))

(fn ui.remove-scene [name]
    (tset ui name nil))

(fn ui.keypressed [key set-mode]
    ;; at some point sort scene by time added or some other
    ;; level so that overlaying scenes get acted on first
    (each [_ scene (pairs ui.scenes)]
          (when (= "function" (type (. scene.keymap key)))
            ((. scene.keymap key) scene))))

(fn ui.mousepressed [x y _button _ _]
    (each [_ scene (pairs ui.scenes)]
          (scene.click scene x y)))

;; (fn ui.mousemoved [x y _ _ _ _]
;;     (each [_ scene (pairs ui.scenes)]
;;           (scene.hover scene x y)))

(fn ui.toggle-on [name]
    (when (. ui :scenes name)
        (tset ui :scenes name :active true)))

(fn ui.is-on? [name]
    (when (. ui :scenes name)
        (. ui :scenes name :active)))
    

(fn ui.toggle-off [name]
    (when (. ui :scenes name)
      (tset ui :scenes name :active false)))

(fn ui.toggle [name]
    (when (. ui :scenes name)      
      (if (. ui :scenes name :active)
          (tset ui :scenes name :active false)
          (tset ui :scenes name :active true))))


ui
