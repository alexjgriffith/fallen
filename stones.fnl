(local image (love.graphics.newImage (.. "assets/" "all-tiles.png")))
(local attack (require "attack"))

(fn grid-to-quad [obj ?times]
    (local grid-size (or ?times 1))
    (love.graphics.newQuad
     (* grid-size obj.x)
     (* grid-size obj.y)
     (* grid-size obj.w)
     (* grid-size obj.h)     
     (: image :getWidth) (: image :getHeight)))


(fn potion-draw [x y]
    (local quad (grid-to-quad {:x 13 :y 15 :w 1 :h 1} 8))
      (love.graphics.draw image quad x y))

(fn ice-draw [x y]
    (local quad (grid-to-quad {:x 11 :y 15 :w 1 :h 1} 8))
      (love.graphics.draw image quad x y))

(fn green-draw [x y]
    (local quad (grid-to-quad {:x 10 :y 15 :w 1 :h 1} 8))
      (love.graphics.draw image quad x y))

(fn blood-draw [x y]
    (local quad (grid-to-quad {:x 12 :y 15 :w 1 :h 1} 8))
    (love.graphics.draw image quad x y))


(fn gabe-draw [x y]
    (local quad (grid-to-quad {:x 29 :y 30 :w 4 :h 3} 8))
    (love.graphics.draw image quad x y))

(local ice-stone {:x (* 120  0.57) :y (* 180 2.02)})  

(local green-stone {:x (* 4.2 180) :y  (* 7.2 120)})

(local blood-stone {:x (* 2.6 180)  :y (* 9.8 120)})

(local potion {:x 170  :y 1200})

(local gabe {:x (* 180 2.1)  :y (* 2.03 120)})

(fn draw [?tx ?ty ?sx ?sy]
    (love.graphics.push)
    (love.graphics.translate (math.floor(or ?tx 0))
                             (math.floor(or ?ty 0)))
    (love.graphics.scale  (or ?sx 1) (or ?sy (or ?sx 1)))

    (gabe-draw gabe.x gabe.y)
    (when state.stones.ice
      ;;(pp "ice")
      (ice-draw ice-stone.x ice-stone.y))
    (when state.stones.green
      (green-draw green-stone.x green-stone.y))
    (when state.stones.blood
      (blood-draw blood-stone.x blood-stone.y))
    (when state.stones.potion
      (potion-draw potion.x potion.y))
    (love.graphics.pop))
    

(fn activate []
    (when (not state.stones.potion)
      (set state.stones.potion true)
      (local hitbox (attack.new {:type :potion :color [0.5 0 0.5 1]
                                       :w 8 :h 8 :x potion.x :y potion.y}))
      (hitbox.begin hitbox)
      (tset state :stones :potion hitbox)
      (hitbox.hitbox-move hitbox potion.x potion.y)
      ))

(fn update []
    ;; (when state.stones.green
    ;;  ( state.stones.hitbox-move))
    (when (and (not state.npcs.farmer.alive)
               (not state.player.state.ice-stone)
               (not state.stones.ice))
      (tset state :stones :ice true)
      (local hitbox (attack.new {:type :ice-stone :color [0.5 0 0.5 1]
                                     :w 8 :h 8 :x ice-stone.x :y ice-stone.y}))
      (hitbox.begin hitbox)
      (tset state :stones :ice-hitbox hitbox)
      (hitbox.hitbox-move hitbox ice-stone.x ice-stone.y)
      )

    (when (and (not state.npcs.knight.alive)
               (not state.player.state.green-stone)
               (not state.stones.green))
      (tset state :stones :green true)
      (local hitbox (attack.new {:type :green-stone :color [0.5 0 0.5 1]
                                       :w 8 :h 8 :x  green-stone.x  :y  green-stone.y}))
      (hitbox.begin hitbox)
      (hitbox.hitbox-move hitbox green-stone.x green-stone.y)
      (tset state :stones :green-hitbox hitbox))    
    
    
    (when (and (not state.npcs.npc1.alive)
               (not state.player.state.blood-stone)
               (not state.stones.blood))
      (tset state :stones :blood true)
      (local hitbox (attack.new {:type :blood-stone :color [0.5 0 0.5 1]
                                       :w 8 :h 8 :x blood-stone.x :y blood-stone.y}))
      (hitbox.begin hitbox)
      (tset state :stones :blood-hitbox hitbox)
      (hitbox.hitbox-move hitbox blood-stone.x blood-stone.y)
      )
    
    )

{:update update :draw draw :activate activate}

