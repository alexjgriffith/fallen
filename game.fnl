(local bump-plugin (require "bump-plugin"))
(local bump (require "lib.bump"))
(local sound (require "sound"))
(local attack (require "attack"))
(local map (require "map")) 
(local subtile (require "subtile"))
(local pfbs (require "pfbs")) 
(local ui (require "ui"))
(local init (require "init"))
(local draw (require "draw"))
(local npc (require "npc"))
(local stones (require "stones"))

(var debug false)

(var show-map false)

(local title-font (love.graphics.newFont "assets/avara.otf" 150))

(local dialogue-gabe
       (lume.split (love.filesystem.read "text/scripts/gabe.txt") "\n"))

(local gravity 10)
(local terminal-velocity 300)

(fn update-gravity [state dt]
    (set state.v (math.min  (* terminal-velocity dt)
                            (+ state.v (* gravity dt)))))

(fn update-g-all [dt]
    (when state.player.state
      (update-gravity state.player.state dt))
    (each [_ npcin (pairs state.npcs)]      
      (update-gravity npcin dt)))

(fn player-camera-controler []
    ;;(pp state.camera)
    (set state.camera.x   (math.floor (+ (* 5 (- state.player.state.x)) 80 (math.floor state.camera.off-x))))
    (set state.camera.y  (math.floor (+ (* 5 (- state.player.state.y)) 100 (math.floor state.camera.off-y)))))

(fn hell-camera-controler []
    ;;(set state.camera.x   (- 1800))
    (set state.camera.x   (- 1780))
    ;;    :x -1870
    ;;   :y -7695
    ;;(set state.camera.y   (- 8400))
    (set state.camera.y   (- 8200))
    )


(fn opening-camera-controler [dt]
    ;;(set state.camera.x   (- 1800))
    (local camera state.camera)
    (local percent (/ camera.opening.time camera.opening.time-max))
    (set camera.x   (math.floor (+ camera.opening.x1 (* percent (- camera.opening.x2 camera.opening.x1)))));;(- (* 180 2.0782 5)))
    ;;    :x -1870
    ;;   :y -7695
    ;;(set camera.y   (- 8400))
    (set camera.y (math.floor (+ camera.opening.y1 (* percent (- camera.opening.y2 camera.opening.y1)))))  ;;(- (* 120 1.5 5)));;(- (* 120 1.866 5)))
    (when (< camera.opening.time camera.opening.time-max)
      (set camera.opening.time (+ camera.opening.time (* dt 1000))))
    )

(var fade-from-white-time-max 1000)
(var fade-from-white-time 0)
(var fade-from-white-? false)
(var fade-from-white-callback nil)

(fn fade-from-white [callback]
    (set fade-from-white-? true)
    (set fade-from-white-time 0)
    (set fade-from-white-callback callback))

(fn fade-from-white-update [dt]
    (when fade-from-white-?
      (when (> fade-from-white-time fade-from-white-time-max)
        (fade-from-white-callback))    
      (set fade-from-white-time (+ fade-from-white-time (* 1000 dt)))))

(fn fade-from-white-draw []
    (when fade-from-white-?
      (local percent (/ fade-from-white-time fade-from-white-time-max))
      (love.graphics.setColor  1 1 1 (- 1 percent))
      (love.graphics.rectangle "fill" 0 0 900 600)
      (love.graphics.setColor 1 1 1 1)))

(var dialogue-start-time 0000)
(var dstart 0)
(var dialogue-time 1000)
(var dialogue-period 3000)
(var index 0)
(var count 0) ;; continues after last text
(var chat false)

(fn shift-focus-to-player []
    (local opening state.camera.opening)
    (set opening.time 0)
    (set opening.x1 state.camera.x)
    (set opening.x2 -1870)
    (set opening.y1 state.camera.y)
    (set opening.y2 -1120))

(fn end-opening []
    (set count 100)
    (set state.player.state.anim-index "Breath")
    (set state.camera.opening.? false)    
    (set state.frozen false))

(fn opening[]
    (when state.scene.start-opening
      (set state.camera.opening.? true)
      (set state.camera.opening.time 0)
      (set state.frozen true)))

(fn dialogue-update [dt dialogue]
    (when (> dstart dialogue-start-time)
      (when (> dialogue-time dialogue-period)
        (set dialogue-time 0)
        (when chat
          (ui.toggle-off "chat-gabe"))
        (when (~= "" (. dialogue (+ 1 index))) (set index (+ index 1)))
        (set count (+ count 1)))      
      (when (. dialogue index)
        (ui.new-scene "chat-gabe"
                      ((require "ui-text-box") (. dialogue index) true))
        (set chat true)
        (ui.toggle "chat-gabe"))
      (set dialogue-time (+ dialogue-time (* dt 1000))))
    (when (= index 0) (tset state.player.state :anim-index "Scream"))
    (when (= index 8)  (shift-focus-to-player))
    (when (= index 9) (set state.player.state.anim-index "Get Spear"))
    (when (or (> count 12) (= index 11))
      (ui.toggle-off "chat-gabe")
      (set index 12)
      (when state.camera.opening.? (end-opening) ))
    (if (< count 12)
        (set dstart (+ dstart (* dt 1000)))
        (set dstart -1))
    )

(fn activate [g-scale display-width display-height level]
    (when state.game.first      
      (init.map-init pfbs map (.. "assets/" level))
      (init.collision-init bump-plugin bump  attack)
      (init.player-init pfbs attack)      
      (init.npcs-init npc)
      (fade-from-white (fn[] (set fade-from-white-? false)))
      (ui.new-scene "menu" (require "ui-menu-params"))
      (ui.new-scene "hud" (require "ui-hud"))
      (ui.toggle-on "hud")
      (stones.activate)
      (opening)
      
      (set state.game.first false)))




(fn draw-standard [message canvas-width canvas-height]
    (draw.draw-background pfbs)
    (draw.draw-sun)
    (draw.draw-clouds)
    (draw.draw-tiles)
    (stones.draw state.camera.x state.camera.y 5 5)
    (each [_ npcin (pairs state.npcs)]      
          (npc.draw npcin))
    (when state.npcs.robo-satan.render-beam
      (love.graphics.rectangle "fill" (- 440 state.npcs.robo-satan.beam-offset)
                               491  (+ state.npcs.robo-satan.beam-offset 200) 40))
    (state.player.draw state.player state.camera)
    (ui.draw 0 0 5 5)
    )

(fn draw-satan-explode [message canvas-width canvas-height]
    (love.graphics.setColor 1 1 1 1)
    (love.graphics.rectangle "fill" 0 0 900 600)
    (love.graphics.setColor 0 0 0 1)
    (love.graphics.rectangle "fill" 639 361 (* 5 32) (* 5 32))
    (love.graphics.setColor 1 1 1 1)
    (npc.draw state.npcs.robo-satan)
    (state.player.draw state.player state.camera)
    (ui.draw 0 0 5 5))

(fn game-draw [message canvas-width canvas-height]
    (if state.special.satan-explode
        (draw-satan-explode message canvas-width canvas-height)
        (draw-standard))
    
    
    (when debug
      (love.graphics.rectangle "fill" (+ 30 state.camera.x) (+ 1880 state.camera.y) 10 10 0 5 5)
      (bump-plugin.draw  state.game.collidables state.game.world
                         state.camera.x state.camera.y 5 5)
      (attack.draw state.camera.x state.camera.y 5 5))
    (when show-map
            (bump-plugin.draw  state.game.collidables state.game.world
                       (+ (/ state.camera.x 5) state.camera.off-x)
                       (+ (/ state.camera.y 5) state.camera.off-y))
      (attack.draw (+ (/ state.camera.x 5) state.camera.off-x)
                   (+ (/ state.camera.y 5) state.camera.off-y))
      )
    (fade-from-white-draw)
    state.camera)


(fn update [dt set-mode]
    (ui.toggle-off "chat-gabe")
    (when (not state.npcs.robo-satan.exists) (set-mode "final"))
    (update-g-all dt)
    (stones.update)
    (if state.player.state.in-hell
        (hell-camera-controler)
        state.camera.opening.?
        (opening-camera-controler dt)
        (player-camera-controler dt))
    (each [_ npcin (pairs state.npcs)]      
          (npc.update npcin dt))
    
    ;;(when (not state.frozen)
      (state.player.update dt set-mode state.player state.game.world state.game.collidables)
      ;;)
      (when state.camera.opening.? (dialogue-update dt dialogue-gabe))
      (ui.update dt)
      (fade-from-white-update dt)
    state.camera)

(fn keypressed [key set-mode]
    (when (= key "escape")      
      (if state.camera.opening.?          
          (end-opening)
          (ui.toggle "menu")))
    (when (= key "t")            
      (end-opening))
    (when (= key "tab")            
      (if debug
          (set debug false)
          (set debug true)))
    (when (= key "m")            
      (if show-map
          (set show-map false)
          (set show-map true)))
    (ui.keypressed ui key set-mode)
    (when (not state.frozen) (state.player.ctrl.keypressed key)))

(fn mousepressed [x y]
    (ui.mousepressed x y))

(fn mousemoved [x y]
    (ui.mousemoved x y))

(fn keyreleased [key set-mode]
    (when (not state.frozen) (state.player.ctrl.keyreleased key)))

{:activate activate           
 :draw game-draw
 :update update
 :keypressed keypressed
 :keyreleased keyreleased
 :mousepressed mousepressed}
