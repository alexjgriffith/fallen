(local attack (require "attack"))
(local ui (require "ui"))


(var chat false)
(var once true)
(var attacking false)
(var time 0)

(fn ctrl-atk [which action args?]
    (let [atk (. state.npcs.knight.attacks which)]
      (if args?
          ((. atk action) atk  (unpack args?))
          (do ((. atk action) atk )))))

(fn foe [dt knight player])

(fn friend [dt knight player p30 p60 p90 pisland]
    (if (> (# p30) 0)
        (do
         (when (not chat)
           (set chat true)
           (ui.new-scene "chat-knight"
                         ((require "ui-text-box") "A beautiful day, isn't it friend?" false))
           (ui.toggle-on "chat-knight"))
         (tset knight :anim-index "Talk"))
        (> (# p60) 0)
        (do (set chat false)
            (ui.toggle-off "chat-knight")
            (tset knight :anim-index "Wave"))
        (do
         (set chat false)
         (ui.toggle-off "chat-knight")
         (tset knight :anim-index "Breath")
         (tset knight :x (- knight.x (* 0 dt))))))


(fn chase-text [dt knight player p30 p60 p90 pisland]
    (set chat false)
    (if
     (> (# p60) 0)
     (do
         (when (not chat)
           (set chat true)
           (ui.new-scene "chat-knight"
                         ((require "ui-text-box") "Why must you do this!" false))
           (ui.toggle "chat-knight")))
        (> (# p90) 0)
        (when (not chat)
          (set chat true)
          (ui.new-scene "chat-knight"
                        ((require "ui-text-box") "Get out of here you no good scoundrel" false))
          (ui.toggle "chat-knight"))        
        (do (set chat false)
            (ui.toggle-off "chat-knight")))
    )

(fn chase-on-island [dt knight player p30 p60 p90 pisland new-x x-dist dt1000]
    ;; (if (and (> new-x knight.island-x)
    ;;          (< new-x (+ knight.island-x knight.island-w)))      
        (if (or (< x-dist 10) (< knight.punching-timer 1000))
            (if knight.punching
                (do
                 (when (> knight.punching-timer 1000)
                   (set knight.punching-timer 0)
                   (ctrl-atk "Punch" :begin [state.x state.y]))
                 (ctrl-atk "Punch" :update [knight.x knight.y knight.flip dt])
                  (set knight.punching-timer (+ knight.punching-timer dt1000)))
                (do (set knight.punching true)
                     (tset knight :anim-index "Punch")
                  (set knight.punching-timer dt1000)
                  (ctrl-atk "Punch" :begin [state.x state.y])))
            (do
             (set knight.punching false)
             (ctrl-atk "Punch" :finish)
             (tset knight :anim-index "Walk")
             (tset knight :x new-x)))
            
    
    );;)

(fn chase [dt knight player p30 p60 p90 pisland]
    (local dt1000 (* 1000 dt))
    (chase-text dt knight player p30 p60 p90 pisland)
    (var sign 1)
    (when (and (not knight.punching) knight.alive)
      (if (< player.x knight.x)
        (do (tset knight :flip true)
              (set sign -1))
        (tset knight :flip false)))
    (local x-dist (math.abs (- player.x knight.x -2)))
    (local new-x (+ knight.x (* dt 0.2 knight.speed sign)))
    (if
     (> (# pisland) 0)
     (chase-on-island dt knight player p30 p60 p90 pisland new-x x-dist dt1000)
     (do
      (set knight.punching  false)
      (ctrl-atk "Punch" :finish)
       (if (and (> new-x knight.island-x)
                (< new-x (+ knight.island-x knight.island-w)))
           (do (tset knight :x new-x)
               (tset knight :anim-index "Walk"))
           (tset knight :anim-index "Breath")))))

(fn [dt]
    (set time (+ time dt))
    (local knight state.npcs.knight)
    (local player state.player.state)
    (tset knight :ox -60)
    (set chat false)
    (ui.toggle-off "chat-knight")
    (local [p60 len][(: attack.world :queryRect
                           (- knight.x 60)
                           (- knight.y 60) 
                           120 120
                           (fn [item] (= item.type "player")))])
    
    (local [p30 len][(: attack.world :queryRect
                           (- knight.x 30)
                           (- knight.y 30) 
                           60 60
                           (fn [item] (= item.type "player")))])

    (local [p90 len][(: attack.world :queryRect
                        (- knight.x 85)
                        (- knight.y 85) 
                        170 170
                        (fn [item] (= item.type "player")))])

    (local [pisland len]
           [(: attack.world :queryRect
               knight.island-x
               knight.island-y
               knight.island-w
               200
               (fn [item] (= item.type "player")))])
    (if
     (not knight.alive)
     (do (tset knight :exists false)
         (when  (: state.game.world :hasItem knight)
         (: state.game.world :remove knight))
       (tset knight :anim-index "Dead"))
     knight.hostile
        (chase dt knight player p30 p60 p90 pisland)
        (friend dt knight player p30 p60 p90 pisland))
    )
