(local action {})

(local act {})

(local actmt {:__index act})

(local default-action {:name "no-name" :callback nil :update-callback (fn [obj cargs args])
                       :cargs nil :timer 0 :default nil
                       :early nil :lead nil :post nil :active false :flip-lock false})

(fn act.update [action-obj dt args?]
    (if (~= nil action-obj.callback)
      (do (when action-obj.update-callback
            (action-obj.update-callback action-obj dt action-obj.cargs args?))
          (set action-obj.timer (+ action-obj.timer dt))
        (when (and action-obj.default (> action-obj.timer action-obj.default))
          (set action-obj.active false))
        (when (not action-obj.active) 
          ;;(pp (.. action-obj.name " action ended"))
          (set action-obj.timer 0)
          (action-obj.callback action-obj.cargs args?)        
          (action-obj.unset action-obj)))
      (do (action-obj.unset action-obj))))

(fn act.set [action-obj new-action]
    ;;(pp action-obj)
    (when action-obj.callback
      (action-obj.callback 0))
    ;; !! REQUIRED what hapens when the timer ends 
    (set action-obj.callback new-action.callback)
    ;; what hapens each update
    (set action-obj.update-callback new-action.update-callback)
    (set action-obj.name new-action.name)
    (set action-obj.timer 0)
    ;; when does movement stop
    (set action-obj.lead (or new-action.lead default-action.lead))
    ;; when can it be inturupted    
    (set action-obj.early (or new-action.early default-action.early))
    ;; how long will it run
    (set action-obj.default (or new-action.default default-action.default))
    ;; when does movement begin again
    (set action-obj.post (or new-action.post default-action.post))
    (set action-obj.active true)
    (set action-obj.flip-lock (or new-action.flip-lock default-action.flip-lock)))

(fn act.unset [action-obj]
    (set action-obj.callback nil)
    (set action-obj.update-callback default-action.update-callback)
    (set action-obj.name default-action.name)
    (set action-obj.timer 0) 
    (set action-obj.lead nil) ;; when does movement stop
    (set action-obj.early nil) ;; when can it be inturupted
    (set action-obj.default nil) ;; how long will it run
    (set action-obj.post nil) ;; when does movement begin again
    (set action-obj.active false)
    (set action-obj.flip-lock false))
    
(fn act.can-act? [action-obj move?]
    ;; (when (and action-obj.default (> action-obj.timer action-obj.default))
    ;;   (action-obj.unset action-obj))
    ;; (not action-obj.active)
    (or (not action-obj.active) (and action-obj.default (> action-obj.timer action-obj.default))))

(fn act.can-act-early? [action-obj move?]
    ;; (when (and action-obj.early (> action-obj.timer action-obj.early))
    ;;   (action-obj.unset action-obj))
    ;; (not action-obj.active)
    (or (not action-obj.active) (and action-obj.early (> action-obj.timer action-obj.early)))
    )

(fn act.can-move? [action-obj]
    (if (or (not action-obj.active)
            (and action-obj.lead (< action-obj.timer action-obj.lead))
            (and action-obj.post (> action-obj.timer action-obj.post)))
        true
        false))

(fn act.can-flip? [action-obj]
    (not action-obj.flip-lock))


(fn action.new []
    (setmetatable default-action actmt))


action
