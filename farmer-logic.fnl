(local attack (require "attack"))
(local ui (require "ui"))


(var chat false)
(var once true)
(var attacking false)


(fn ctrl-atk [which action args?]
    (let [atk (. state.npcs.farmer.attacks which)]
      (if args?
          ((. atk action) atk  (unpack args?))
          (do ((. atk action) atk )))))

(fn foe [dt farmer player])

(fn friend [dt farmer player p30 p60 p90 pisland]
    (if (> (# p30) 0)
        (do
         (when (not chat)
           (set chat true)
           (ui.new-scene "chat-farmer"
                         ((require "ui-text-box") "How's it going there, eh?" false))
           (ui.toggle "chat-farmer"))
         (tset farmer :anim-index "Talk"))
        (> (# p60) 0)
        (do (set chat false)
            (ui.toggle-off "chat-farmer")
            (tset farmer :anim-index "Wave"))
        (do
         (set chat false)
         (ui.toggle-off "chat-farmer")
         (tset farmer :anim-index "Breath")
         (tset farmer :x (- farmer.x (* 0 dt))))))


(fn demo [dt farmer player p30 p60 p90 pisland]
    (when (> farmer.time 2)
      (when (not attacking)
        (tset farmer :anim-index "Punch")
        (set attacking true)
        (ctrl-atk "Punch" :begin [state.x state.y])
        ;; (pp (: attack.world :queryRect
        ;;                    (- state.npcs.farmer.x 30)
        ;;                    (- state.npcs.farmer.y 30) 
        ;;                    60 60))
        ;; (pp (.. "Farmer x " farmer.x "y " farmer.y))
        ;;(pp (.. "Player x " player.x "y " player.y))
        ))
    (when (> farmer.time 2.8)
      (set farmer.time 0)
      (ctrl-atk "Punch" :finish)
      (set attacking false)      
      )
    (when attacking
      (ctrl-atk "Punch" :update [farmer.x farmer.y farmer.flip dt]))
    (when (not attacking)
      (if (> (# p30) 0)
        (tset farmer :anim-index "Talk")
        (> (# p60) 0)
      (tset farmer :anim-index "Wave")
      (do (tset farmer :anim-index "Breath")
        (tset farmer :x (- farmer.x (* 0 dt)))))))



(fn chase-text [dt farmer player p30 p60 p90 pisland]
    (set chat false)
    (if
     (> (# p60) 0)
     (do
         (when (not chat)
           (set chat true)
           (ui.new-scene "chat-farmer"
                         ((require "ui-text-box") "I know what you want! You can't have it!" false))
           (ui.toggle "chat-farmer")))
        (> (# p90) 0)
        (when (not chat)
          (set chat true)
          (ui.new-scene "chat-farmer"
                        ((require "ui-text-box") "You dare come to my house, and try to take my stone!?" false))
          (ui.toggle "chat-farmer"))        
        (do (set chat false)
            (ui.toggle-off "chat-farmer")))
    )

(fn chase-on-island [dt farmer player p30 p60 p90 pisland new-x x-dist dt1000]
    ;; (if (and (> new-x farmer.island-x)
    ;;          (< new-x (+ farmer.island-x farmer.island-w)))      
        (if (or (< x-dist 10) (< farmer.punching-timer 1000))
            (if farmer.punching
                (do
                 (when (> farmer.punching-timer 1000)
                   (set farmer.punching-timer 0)
                   (ctrl-atk "Punch" :begin [state.x state.y]))
                 (ctrl-atk "Punch" :update [farmer.x farmer.y farmer.flip dt])
                  (set farmer.punching-timer (+ farmer.punching-timer dt1000)))
                (do (set farmer.punching true)
                     (tset farmer :anim-index "Punch")
                  (set farmer.punching-timer dt1000)
                  (ctrl-atk "Punch" :begin [state.x state.y])))
            (do
             (set farmer.punching false)
             (ctrl-atk "Punch" :finish)
             (tset farmer :anim-index "Walk")
             (tset farmer :x new-x)))
            
    
    );;)

(fn chase [dt farmer player p30 p60 p90 pisland]
    (local dt1000 (* 1000 dt))
    (chase-text dt farmer player p30 p60 p90 pisland)
    (var sign 1)
    (when (and (not farmer.punching) farmer.alive)
      (if (< player.x farmer.x)
        (do (tset farmer :flip true)
              (set sign -1))
        (tset farmer :flip false)))
    (local x-dist (math.abs (- player.x farmer.x -2)))
    (local new-x (+ farmer.x (* dt 0.2 farmer.speed sign)))
    (if
     (> (# pisland) 0)
     (chase-on-island dt farmer player p30 p60 p90 pisland new-x x-dist dt1000)
     (do
      (set farmer.punching false)
      (ctrl-atk "Punch" :finish)
       (if (and (> new-x farmer.island-x)
                (< new-x (+ farmer.island-x farmer.island-w)))
           (do (tset farmer :x new-x)
               (tset farmer :anim-index "Walk"))
           (tset farmer :anim-index "Breath")))))

(fn [dt]
    (local farmer state.npcs.farmer)
    (local player state.player.state)
    (set farmer.time (+ farmer.time dt))    
    (tset farmer :ox -60)
    (set chat false)
    (ui.toggle-off "chat-farmer")
    (local [p60 len][(: attack.world :queryRect
                           (- state.npcs.farmer.x 60)
                           (- state.npcs.farmer.y 60) 
                           120 120
                           (fn [item] (= item.type "player")))])
    
    (local [p30 len][(: attack.world :queryRect
                           (- state.npcs.farmer.x 30)
                           (- state.npcs.farmer.y 30) 
                           60 60
                           (fn [item] (= item.type "player")))])

    (local [p90 len][(: attack.world :queryRect
                        (- state.npcs.farmer.x 85)
                        (- state.npcs.farmer.y 85) 
                        170 170
                        (fn [item] (= item.type "player")))])

    (local [pisland len]
           [(: attack.world :queryRect
               farmer.island-x
               farmer.island-y
               farmer.island-w
               200
               (fn [item] (= item.type "player")))])
    (if
     (not farmer.alive)
     (do (tset farmer :exists false)
         ;;(ctrl-atk "Punch" :finish)
         (when  (: state.game.world :hasItem farmer)
         (: state.game.world :remove farmer))
         (tset farmer :anim-index "Dead"))
     farmer.hostile
        (chase dt farmer player p30 p60 p90 pisland)
        (friend dt farmer player p30 p60 p90 pisland))
    )
