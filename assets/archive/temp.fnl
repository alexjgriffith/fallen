{
  :width 57
  :data {
    :for1 {}
    :for2 {}
    :sun {
      59 {
        :id 0
        :x 2
        :y 1
        :w 10
        :type "sun"
        :h 8
        :l "sun"
        :library "tile"
      }
    }
    :objs {
      1220 {
        :id 0
        :x 23
        :y 21
        :w 2
        :type "tree"
        :h 4
        :l "objs"
        :library "tile"
      }
      1436 {
        :id 0
        :x 11
        :y 25
        :w 2
        :type "tree"
        :h 4
        :l "objs"
        :library "tile"
      }
      1499 {
        :id 0
        :x 17
        :y 26
        :w 4
        :type "big-tree"
        :h 6
        :l "objs"
        :library "tile"
      }
      884 {
        :id 0
        :x 29
        :y 15
        :w 4
        :type "big-tree"
        :h 6
        :l "objs"
        :library "tile"
      }
    }
    :clouds {
      477 {
        :id 0
        :x 21
        :y 8
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2341 {
        :id 0
        :x 4
        :y 41
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1389 {
        :id 0
        :x 21
        :y 24
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2060 {
        :id 0
        :x 8
        :y 36
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      800 {
        :id 0
        :x 2
        :y 14
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2084 {
        :id 0
        :x 32
        :y 36
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      806 {
        :id 0
        :x 8
        :y 14
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1070 {
        :id 0
        :x 44
        :y 18
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1540 {
        :id 0
        :x 1
        :y 27
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1341 {
        :id 0
        :x 30
        :y 23
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1030 {
        :id 0
        :x 4
        :y 18
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      503 {
        :id 0
        :x 47
        :y 8
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      780 {
        :id 0
        :x 39
        :y 13
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1313 {
        :id 0
        :x 2
        :y 23
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1753 {
        :id 0
        :x 43
        :y 30
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      517 {
        :id 0
        :x 4
        :y 9
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      123 {
        :id 0
        :x 9
        :y 2
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1267 {
        :id 0
        :x 13
        :y 22
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      386 {
        :id 0
        :x 44
        :y 6
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2077 {
        :id 0
        :x 25
        :y 36
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      195 {
        :id 0
        :x 24
        :y 3
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      205 {
        :id 0
        :x 34
        :y 3
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      202 {
        :id 0
        :x 31
        :y 3
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      697 {
        :id 0
        :x 13
        :y 12
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2327 {
        :id 0
        :x 47
        :y 40
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      423 {
        :id 0
        :x 24
        :y 7
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1904 {
        :id 0
        :x 23
        :y 33
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      930 {
        :id 0
        :x 18
        :y 16
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      681 {
        :id 0
        :x 54
        :y 11
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      215 {
        :id 0
        :x 44
        :y 3
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2110 {
        :id 0
        :x 1
        :y 37
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2487 {
        :id 0
        :x 36
        :y 43
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      926 {
        :id 0
        :x 14
        :y 16
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      922 {
        :id 0
        :x 10
        :y 16
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      438 {
        :id 0
        :x 39
        :y 7
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      655 {
        :id 0
        :x 28
        :y 11
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1997 {
        :id 0
        :x 2
        :y 35
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2009 {
        :id 0
        :x 14
        :y 35
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1487 {
        :id 0
        :x 5
        :y 26
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1786 {
        :id 0
        :x 19
        :y 31
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1666 {
        :id 0
        :x 13
        :y 29
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1172 {
        :id 0
        :x 32
        :y 20
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1164 {
        :id 0
        :x 24
        :y 20
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      602 {
        :id 0
        :x 32
        :y 10
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1459 {
        :id 0
        :x 34
        :y 25
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      350 {
        :id 0
        :x 8
        :y 6
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      580 {
        :id 0
        :x 10
        :y 10
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1885 {
        :id 0
        :x 4
        :y 33
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      346 {
        :id 0
        :x 4
        :y 6
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1877 {
        :id 0
        :x 53
        :y 32
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1144 {
        :id 0
        :x 4
        :y 20
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2264 {
        :id 0
        :x 41
        :y 39
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1658 {
        :id 0
        :x 5
        :y 29
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      168 {
        :id 0
        :x 54
        :y 2
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2296 {
        :id 0
        :x 16
        :y 40
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2521 {
        :id 0
        :x 13
        :y 44
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      793 {
        :id 0
        :x 52
        :y 13
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      372 {
        :id 0
        :x 30
        :y 6
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1578 {
        :id 0
        :x 39
        :y 27
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      188 {
        :id 0
        :x 17
        :y 3
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1009 {
        :id 0
        :x 40
        :y 17
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1019 {
        :id 0
        :x 50
        :y 17
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      91 {
        :id 0
        :x 34
        :y 1
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2038 {
        :id 0
        :x 43
        :y 35
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      526 {
        :id 0
        :x 13
        :y 9
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1193 {
        :id 0
        :x 53
        :y 20
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2361 {
        :id 0
        :x 24
        :y 41
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      540 {
        :id 0
        :x 27
        :y 9
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1946 {
        :id 0
        :x 8
        :y 34
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      71 {
        :id 0
        :x 14
        :y 1
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2426 {
        :id 0
        :x 32
        :y 42
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      141 {
        :id 0
        :x 27
        :y 2
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2504 {
        :id 0
        :x 53
        :y 43
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2145 {
        :id 0
        :x 36
        :y 37
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2493 {
        :id 0
        :x 42
        :y 43
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2555 {
        :id 0
        :x 47
        :y 44
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2472 {
        :id 0
        :x 21
        :y 43
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2237 {
        :id 0
        :x 14
        :y 39
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1452 {
        :id 0
        :x 27
        :y 25
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      394 {
        :id 0
        :x 52
        :y 6
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1858 {
        :id 0
        :x 34
        :y 32
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1627 {
        :id 0
        :x 31
        :y 28
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2230 {
        :id 0
        :x 7
        :y 39
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2458 {
        :id 0
        :x 7
        :y 43
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2129 {
        :id 0
        :x 20
        :y 37
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      276 {
        :id 0
        :x 48
        :y 4
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1615 {
        :id 0
        :x 19
        :y 28
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1348 {
        :id 0
        :x 37
        :y 23
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1655 {
        :id 0
        :x 2
        :y 29
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      841 {
        :id 0
        :x 43
        :y 14
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1093 {
        :id 0
        :x 10
        :y 19
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1918 {
        :id 0
        :x 37
        :y 33
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      529 {
        :id 0
        :x 16
        :y 9
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1105 {
        :id 0
        :x 22
        :y 19
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      778 {
        :id 0
        :x 37
        :y 13
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      690 {
        :id 0
        :x 6
        :y 12
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      288 {
        :id 0
        :x 3
        :y 5
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2113 {
        :id 0
        :x 4
        :y 37
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      302 {
        :id 0
        :x 17
        :y 5
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      674 {
        :id 0
        :x 47
        :y 11
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      119 {
        :id 0
        :x 5
        :y 2
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1814 {
        :id 0
        :x 47
        :y 31
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      298 {
        :id 0
        :x 13
        :y 5
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1838 {
        :id 0
        :x 14
        :y 32
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1591 {
        :id 0
        :x 52
        :y 27
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      433 {
        :id 0
        :x 34
        :y 7
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      704 {
        :id 0
        :x 20
        :y 12
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      860 {
        :id 0
        :x 5
        :y 15
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1320 {
        :id 0
        :x 9
        :y 23
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1547 {
        :id 0
        :x 8
        :y 27
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      306 {
        :id 0
        :x 21
        :y 5
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1316 {
        :id 0
        :x 5
        :y 23
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      606 {
        :id 0
        :x 36
        :y 10
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1235 {
        :id 0
        :x 38
        :y 21
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      610 {
        :id 0
        :x 40
        :y 10
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1238 {
        :id 0
        :x 41
        :y 21
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      85 {
        :id 0
        :x 28
        :y 1
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      765 {
        :id 0
        :x 24
        :y 13
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      153 {
        :id 0
        :x 39
        :y 2
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2348 {
        :id 0
        :x 11
        :y 41
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1768 {
        :id 0
        :x 1
        :y 31
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2275 {
        :id 0
        :x 52
        :y 39
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      888 {
        :id 0
        :x 33
        :y 15
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1270 {
        :id 0
        :x 16
        :y 22
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2047 {
        :id 0
        :x 52
        :y 35
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1505 {
        :id 0
        :x 23
        :y 26
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1776 {
        :id 0
        :x 9
        :y 31
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      621 {
        :id 0
        :x 51
        :y 10
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1262 {
        :id 0
        :x 8
        :y 22
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      75 {
        :id 0
        :x 18
        :y 1
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      613 {
        :id 0
        :x 43
        :y 10
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2099 {
        :id 0
        :x 47
        :y 36
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      59 {
        :id 0
        :x 2
        :y 1
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      937 {
        :id 0
        :x 25
        :y 16
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      933 {
        :id 0
        :x 21
        :y 16
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      79 {
        :id 0
        :x 22
        :y 1
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1154 {
        :id 0
        :x 14
        :y 20
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1378 {
        :id 0
        :x 10
        :y 24
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1469 {
        :id 0
        :x 44
        :y 25
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1336 {
        :id 0
        :x 25
        :y 23
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      593 {
        :id 0
        :x 23
        :y 10
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1441 {
        :id 0
        :x 16
        :y 25
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      589 {
        :id 0
        :x 19
        :y 10
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2196 {
        :id 0
        :x 30
        :y 38
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      773 {
        :id 0
        :x 32
        :y 13
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      758 {
        :id 0
        :x 17
        :y 13
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1062 {
        :id 0
        :x 36
        :y 18
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1359 {
        :id 0
        :x 48
        :y 23
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      826 {
        :id 0
        :x 28
        :y 14
        :w 3
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
    }
    :ground {
      6241 {
        :x 28
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-hell"
        :y 109
        :l "ground"
        :library "subtile"
      }
      2309 {
        :x 29
        :index [ 8 8 3 9 ]
        :id 0
        :type "road-green"
        :y 40
        :l "ground"
        :library "subtile"
      }
      1389 {
        :x 21
        :index [ 9 9 3 1 ]
        :id 0
        :type "road-ice"
        :y 24
        :l "ground"
        :library "subtile"
      }
      1110 {
        :x 27
        :index [ 9 9 3 1 ]
        :id 0
        :type "road-ice"
        :y 19
        :l "ground"
        :library "subtile"
      }
      1552 {
        :x 13
        :index [ 8 11 10 13 ]
        :id 0
        :type "road-ice"
        :y 27
        :l "ground"
        :library "subtile"
      }
      3240 {
        :x 48
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-green"
        :y 56
        :l "ground"
        :library "subtile"
      }
      1544 {
        :x 5
        :index [ 9 12 10 13 ]
        :id 0
        :type "road-ice"
        :y 27
        :l "ground"
        :library "subtile"
      }
      2485 {
        :x 34
        :index [ 9 9 3 9 ]
        :id 0
        :type "road-green"
        :y 43
        :l "ground"
        :library "subtile"
      }
      2995 {
        :x 31
        :index [ 8 8 3 9 ]
        :id 0
        :type "road-green"
        :y 52
        :l "ground"
        :library "subtile"
      }
      4472 {
        :x 26
        :index [ 6 9 7 10 ]
        :id 0
        :type "road-blood"
        :y 78
        :l "ground"
        :library "subtile"
      }
      6242 {
        :x 29
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-hell"
        :y 109
        :l "ground"
        :library "subtile"
      }
      3671 {
        :x 23
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-blood"
        :y 64
        :l "ground"
        :library "subtile"
      }
      1333 {
        :x 22
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-ice"
        :y 23
        :l "ground"
        :library "subtile"
      }
      4282 {
        :x 7
        :index [ 9 9 9 1 ]
        :id 0
        :type "road-blood"
        :y 75
        :l "ground"
        :library "subtile"
      }
      4569 {
        :x 9
        :index [ 9 9 3 9 ]
        :id 0
        :type "road-blood"
        :y 80
        :l "ground"
        :library "subtile"
      }
      2643 {
        :x 21
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-green"
        :y 46
        :l "ground"
        :library "subtile"
      }
      1729 {
        :x 19
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-ice"
        :y 30
        :l "ground"
        :library "subtile"
      }
      3354 {
        :x 48
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-green"
        :y 58
        :l "ground"
        :library "subtile"
      }
      2374 {
        :x 37
        :index [ 9 9 10 10 ]
        :id 0
        :type "road-green"
        :y 41
        :l "ground"
        :library "subtile"
      }
      3639 {
        :x 48
        :index [ 6 12 7 13 ]
        :id 0
        :type "road-green"
        :y 63
        :l "ground"
        :library "subtile"
      }
      4473 {
        :x 27
        :index [ 9 9 3 9 ]
        :id 0
        :type "road-blood"
        :y 78
        :l "ground"
        :library "subtile"
      }
      6243 {
        :x 30
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-hell"
        :y 109
        :l "ground"
        :library "subtile"
      }
      1510 {
        :x 28
        :index [ 8 11 10 13 ]
        :id 0
        :type "road-ice"
        :y 26
        :l "ground"
        :library "subtile"
      }
      4570 {
        :x 10
        :index [ 9 2 9 9 ]
        :id 0
        :type "road-blood"
        :y 80
        :l "ground"
        :library "subtile"
      }
      1430 {
        :x 5
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-ice"
        :y 25
        :l "ground"
        :library "subtile"
      }
      3241 {
        :x 49
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-green"
        :y 56
        :l "ground"
        :library "subtile"
      }
      1167 {
        :x 27
        :index [ 6 12 7 13 ]
        :id 0
        :type "road-ice"
        :y 20
        :l "ground"
        :library "subtile"
      }
      2486 {
        :x 35
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-green"
        :y 43
        :l "ground"
        :library "subtile"
      }
      2996 {
        :x 32
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-green"
        :y 52
        :l "ground"
        :library "subtile"
      }
      4474 {
        :x 28
        :index [ 9 9 9 1 ]
        :id 0
        :type "road-blood"
        :y 78
        :l "ground"
        :library "subtile"
      }
      6244 {
        :x 31
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-hell"
        :y 109
        :l "ground"
        :library "subtile"
      }
      3225 {
        :x 33
        :index [ 6 9 6 1 ]
        :id 0
        :type "road-green"
        :y 56
        :l "ground"
        :library "subtile"
      }
      1968 {
        :x 30
        :index [ 6 12 7 13 ]
        :id 0
        :type "road-ice"
        :y 34
        :l "ground"
        :library "subtile"
      }
      1446 {
        :x 21
        :index [ 6 12 7 13 ]
        :id 0
        :type "road-ice"
        :y 25
        :l "ground"
        :library "subtile"
      }
      4571 {
        :x 11
        :index [ 8 8 9 1 ]
        :id 0
        :type "road-blood"
        :y 80
        :l "ground"
        :library "subtile"
      }
      2644 {
        :x 22
        :index [ 8 8 3 9 ]
        :id 0
        :type "road-green"
        :y 46
        :l "ground"
        :library "subtile"
      }
      3355 {
        :x 49
        :index [ 9 9 9 1 ]
        :id 0
        :type "road-green"
        :y 58
        :l "ground"
        :library "subtile"
      }
      2375 {
        :x 38
        :index [ 9 12 10 13 ]
        :id 0
        :type "road-green"
        :y 41
        :l "ground"
        :library "subtile"
      }
      4475 {
        :x 29
        :index [ 9 12 10 13 ]
        :id 0
        :type "road-blood"
        :y 78
        :l "ground"
        :library "subtile"
      }
      6245 {
        :x 32
        :index [ 8 11 10 13 ]
        :id 0
        :type "road-hell"
        :y 109
        :l "ground"
        :library "subtile"
      }
      1912 {
        :x 31
        :index [ 8 11 10 13 ]
        :id 0
        :type "road-ice"
        :y 33
        :l "ground"
        :library "subtile"
      }
      2311 {
        :x 31
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-green"
        :y 40
        :l "ground"
        :library "subtile"
      }
      1390 {
        :x 22
        :index [ 9 9 10 10 ]
        :id 0
        :type "road-ice"
        :y 24
        :l "ground"
        :library "subtile"
      }
      1111 {
        :x 28
        :index [ 9 12 10 13 ]
        :id 0
        :type "road-ice"
        :y 19
        :l "ground"
        :library "subtile"
      }
      3736 {
        :x 31
        :index [ 6 9 7 10 ]
        :id 0
        :type "road-blood"
        :y 65
        :l "ground"
        :library "subtile"
      }
      3242 {
        :x 50
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-green"
        :y 56
        :l "ground"
        :library "subtile"
      }
      3467 {
        :x 47
        :index [ 6 9 7 10 ]
        :id 0
        :type "road-green"
        :y 60
        :l "ground"
        :library "subtile"
      }
      2487 {
        :x 36
        :index [ 9 12 9 12 ]
        :id 0
        :type "road-green"
        :y 43
        :l "ground"
        :library "subtile"
      }
      2997 {
        :x 33
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-green"
        :y 52
        :l "ground"
        :library "subtile"
      }
      3226 {
        :x 34
        :index [ 9 12 10 13 ]
        :id 0
        :type "road-green"
        :y 56
        :l "ground"
        :library "subtile"
      }
      1334 {
        :x 23
        :index [ 8 8 9 1 ]
        :id 0
        :type "road-ice"
        :y 23
        :l "ground"
        :library "subtile"
      }
      2934 {
        :x 27
        :index [ 6 12 7 13 ]
        :id 0
        :type "road-green"
        :y 51
        :l "ground"
        :library "subtile"
      }
      1055 {
        :x 29
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-ice"
        :y 18
        :l "ground"
        :library "subtile"
      }
      1730 {
        :x 20
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-ice"
        :y 30
        :l "ground"
        :library "subtile"
      }
      3356 {
        :x 50
        :index [ 9 12 10 13 ]
        :id 0
        :type "road-green"
        :y 58
        :l "ground"
        :library "subtile"
      }
      4413 {
        :x 24
        :index [ 6 9 7 10 ]
        :id 0
        :type "road-blood"
        :y 77
        :l "ground"
        :library "subtile"
      }
      1487 {
        :x 5
        :index [ 9 9 9 1 ]
        :id 0
        :type "road-ice"
        :y 26
        :l "ground"
        :library "subtile"
      }
      2822 {
        :x 29
        :index [ 8 11 10 13 ]
        :id 0
        :type "road-green"
        :y 49
        :l "ground"
        :library "subtile"
      }
      4510 {
        :x 7
        :index [ 6 9 7 10 ]
        :id 0
        :type "road-blood"
        :y 79
        :l "ground"
        :library "subtile"
      }
      2312 {
        :x 32
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-green"
        :y 40
        :l "ground"
        :library "subtile"
      }
      1786 {
        :x 19
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-ice"
        :y 31
        :l "ground"
        :library "subtile"
      }
      3785 {
        :x 23
        :index [ 6 9 7 10 ]
        :id 0
        :type "road-blood"
        :y 66
        :l "ground"
        :library "subtile"
      }
      1431 {
        :x 6
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-ice"
        :y 25
        :l "ground"
        :library "subtile"
      }
      3291 {
        :x 42
        :index [ 6 9 7 10 ]
        :id 0
        :type "road-green"
        :y 57
        :l "ground"
        :library "subtile"
      }
      3737 {
        :x 32
        :index [ 9 9 3 9 ]
        :id 0
        :type "road-blood"
        :y 65
        :l "ground"
        :library "subtile"
      }
      4414 {
        :x 25
        :index [ 9 9 10 10 ]
        :id 0
        :type "road-blood"
        :y 77
        :l "ground"
        :library "subtile"
      }
      3468 {
        :x 48
        :index [ 9 12 3 12 ]
        :id 0
        :type "road-green"
        :y 60
        :l "ground"
        :library "subtile"
      }
      2135 {
        :x 26
        :index [ 8 8 3 1 ]
        :id 0
        :type "road-green"
        :y 37
        :l "ground"
        :library "subtile"
      }
      4223 {
        :x 5
        :index [ 6 9 7 10 ]
        :id 0
        :type "road-blood"
        :y 74
        :l "ground"
        :library "subtile"
      }
      4224 {
        :x 6
        :index [ 9 9 3 9 ]
        :id 0
        :type "road-blood"
        :y 74
        :l "ground"
        :library "subtile"
      }
      4511 {
        :x 8
        :index [ 9 9 3 9 ]
        :id 0
        :type "road-blood"
        :y 79
        :l "ground"
        :library "subtile"
      }
      3674 {
        :x 26
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-blood"
        :y 64
        :l "ground"
        :library "subtile"
      }
      4288 {
        :x 13
        :index [ 6 9 7 10 ]
        :id 0
        :type "road-blood"
        :y 75
        :l "ground"
        :library "subtile"
      }
      2646 {
        :x 24
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-green"
        :y 46
        :l "ground"
        :library "subtile"
      }
      3851 {
        :x 32
        :index [ 6 9 7 10 ]
        :id 0
        :type "road-blood"
        :y 67
        :l "ground"
        :library "subtile"
      }
      4415 {
        :x 26
        :index [ 9 9 3 9 ]
        :id 0
        :type "road-blood"
        :y 77
        :l "ground"
        :library "subtile"
      }
      4225 {
        :x 7
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 74
        :l "ground"
        :library "subtile"
      }
      2313 {
        :x 33
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-green"
        :y 40
        :l "ground"
        :library "subtile"
      }
      4059 {
        :x 12
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 71
        :l "ground"
        :library "subtile"
      }
      1391 {
        :x 23
        :index [ 9 12 10 13 ]
        :id 0
        :type "road-ice"
        :y 24
        :l "ground"
        :library "subtile"
      }
      1056 {
        :x 30
        :index [ 8 11 10 13 ]
        :id 0
        :type "road-ice"
        :y 18
        :l "ground"
        :library "subtile"
      }
      2758 {
        :x 22
        :index [ 6 9 7 10 ]
        :id 0
        :type "road-green"
        :y 48
        :l "ground"
        :library "subtile"
      }
      3738 {
        :x 33
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 65
        :l "ground"
        :library "subtile"
      }
      3244 {
        :x 52
        :index [ 8 11 10 13 ]
        :id 0
        :type "road-green"
        :y 56
        :l "ground"
        :library "subtile"
      }
      4416 {
        :x 27
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 77
        :l "ground"
        :library "subtile"
      }
      2999 {
        :x 35
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-green"
        :y 52
        :l "ground"
        :library "subtile"
      }
      2136 {
        :x 27
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-green"
        :y 37
        :l "ground"
        :library "subtile"
      }
      4226 {
        :x 8
        :index [ 9 9 9 1 ]
        :id 0
        :type "road-blood"
        :y 74
        :l "ground"
        :library "subtile"
      }
      1335 {
        :x 24
        :index [ 8 11 10 13 ]
        :id 0
        :type "road-ice"
        :y 23
        :l "ground"
        :library "subtile"
      }
      2426 {
        :x 32
        :index [ 6 9 7 10 ]
        :id 0
        :type "road-green"
        :y 42
        :l "ground"
        :library "subtile"
      }
      4644 {
        :x 27
        :index [ 6 12 7 13 ]
        :id 0
        :type "road-blood"
        :y 81
        :l "ground"
        :library "subtile"
      }
      2647 {
        :x 25
        :index [ 8 8 9 1 ]
        :id 0
        :type "road-green"
        :y 46
        :l "ground"
        :library "subtile"
      }
      1488 {
        :x 6
        :index [ 9 9 10 10 ]
        :id 0
        :type "road-ice"
        :y 26
        :l "ground"
        :library "subtile"
      }
      1731 {
        :x 21
        :index [ 9 9 9 1 ]
        :id 0
        :type "road-ice"
        :y 30
        :l "ground"
        :library "subtile"
      }
      4417 {
        :x 28
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 77
        :l "ground"
        :library "subtile"
      }
      4740 {
        :x 9
        :index [ 6 12 6 12 ]
        :id 0
        :type "road-blood"
        :y 83
        :l "ground"
        :library "subtile"
      }
      4227 {
        :x 9
        :index [ 9 12 10 13 ]
        :id 0
        :type "road-blood"
        :y 74
        :l "ground"
        :library "subtile"
      }
      2314 {
        :x 34
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-green"
        :y 40
        :l "ground"
        :library "subtile"
      }
      1787 {
        :x 20
        :index [ 9 9 9 1 ]
        :id 0
        :type "road-ice"
        :y 31
        :l "ground"
        :library "subtile"
      }
      3787 {
        :x 25
        :index [ 9 9 3 9 ]
        :id 0
        :type "road-blood"
        :y 66
        :l "ground"
        :library "subtile"
      }
      3582 {
        :x 48
        :index [ 6 12 6 12 ]
        :id 0
        :type "road-green"
        :y 62
        :l "ground"
        :library "subtile"
      }
      1675 {
        :x 22
        :index [ 8 8 9 1 ]
        :id 0
        :type "road-ice"
        :y 29
        :l "ground"
        :library "subtile"
      }
      4354 {
        :x 22
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-blood"
        :y 76
        :l "ground"
        :library "subtile"
      }
      2759 {
        :x 23
        :index [ 9 12 3 12 ]
        :id 0
        :type "road-green"
        :y 48
        :l "ground"
        :library "subtile"
      }
      1432 {
        :x 7
        :index [ 8 8 9 1 ]
        :id 0
        :type "road-ice"
        :y 25
        :l "ground"
        :library "subtile"
      }
      3996 {
        :x 6
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-blood"
        :y 70
        :l "ground"
        :library "subtile"
      }
      4418 {
        :x 29
        :index [ 9 9 9 1 ]
        :id 0
        :type "road-blood"
        :y 77
        :l "ground"
        :library "subtile"
      }
      3000 {
        :x 36
        :index [ 8 8 9 1 ]
        :id 0
        :type "road-green"
        :y 52
        :l "ground"
        :library "subtile"
      }
      2137 {
        :x 28
        :index [ 8 11 10 13 ]
        :id 0
        :type "road-green"
        :y 37
        :l "ground"
        :library "subtile"
      }
      2427 {
        :x 33
        :index [ 9 9 3 9 ]
        :id 0
        :type "road-green"
        :y 42
        :l "ground"
        :library "subtile"
      }
      2648 {
        :x 26
        :index [ 8 11 10 13 ]
        :id 0
        :type "road-green"
        :y 46
        :l "ground"
        :library "subtile"
      }
      4419 {
        :x 30
        :index [ 9 9 10 10 ]
        :id 0
        :type "road-blood"
        :y 77
        :l "ground"
        :library "subtile"
      }
      2600 {
        :x 35
        :index [ 6 9 7 10 ]
        :id 0
        :type "road-green"
        :y 45
        :l "ground"
        :library "subtile"
      }
      3901 {
        :x 25
        :index [ 6 12 6 12 ]
        :id 0
        :type "road-blood"
        :y 68
        :l "ground"
        :library "subtile"
      }
      2315 {
        :x 35
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-green"
        :y 40
        :l "ground"
        :library "subtile"
      }
      4061 {
        :x 14
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 71
        :l "ground"
        :library "subtile"
      }
      3788 {
        :x 26
        :index [ 9 12 9 12 ]
        :id 0
        :type "road-blood"
        :y 66
        :l "ground"
        :library "subtile"
      }
      4356 {
        :x 24
        :index [ 8 8 3 9 ]
        :id 0
        :type "road-blood"
        :y 76
        :l "ground"
        :library "subtile"
      }
      3294 {
        :x 45
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-green"
        :y 57
        :l "ground"
        :library "subtile"
      }
      3740 {
        :x 35
        :index [ 9 9 9 1 ]
        :id 0
        :type "road-blood"
        :y 65
        :l "ground"
        :library "subtile"
      }
      3997 {
        :x 7
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-blood"
        :y 70
        :l "ground"
        :library "subtile"
      }
      4420 {
        :x 31
        :index [ 9 12 10 13 ]
        :id 0
        :type "road-blood"
        :y 77
        :l "ground"
        :library "subtile"
      }
      3001 {
        :x 37
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-green"
        :y 52
        :l "ground"
        :library "subtile"
      }
      4452 {
        :x 6
        :index [ 6 9 7 10 ]
        :id 0
        :type "road-blood"
        :y 78
        :l "ground"
        :library "subtile"
      }
      1672 {
        :x 19
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-ice"
        :y 29
        :l "ground"
        :library "subtile"
      }
      4230 {
        :x 12
        :index [ 6 9 7 10 ]
        :id 0
        :type "road-blood"
        :y 74
        :l "ground"
        :library "subtile"
      }
      310 {
        :x 25
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-heaven"
        :y 5
        :l "ground"
        :library "subtile"
      }
      4797 {
        :x 9
        :index [ 6 12 6 12 ]
        :id 0
        :type "road-blood"
        :y 84
        :l "ground"
        :library "subtile"
      }
      1842 {
        :x 18
        :index [ 6 9 6 9 ]
        :id 0
        :type "road-ice"
        :y 32
        :l "ground"
        :library "subtile"
      }
      2428 {
        :x 34
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-green"
        :y 42
        :l "ground"
        :library "subtile"
      }
      4684 {
        :x 10
        :index [ 9 12 10 13 ]
        :id 0
        :type "road-blood"
        :y 82
        :l "ground"
        :library "subtile"
      }
      3353 {
        :x 47
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-green"
        :y 58
        :l "ground"
        :library "subtile"
      }
      3966 {
        :x 33
        :index [ 6 12 7 13 ]
        :id 0
        :type "road-blood"
        :y 69
        :l "ground"
        :library "subtile"
      }
      1489 {
        :x 7
        :index [ 9 12 10 13 ]
        :id 0
        :type "road-ice"
        :y 26
        :l "ground"
        :library "subtile"
      }
      3686 {
        :x 38
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-blood"
        :y 64
        :l "ground"
        :library "subtile"
      }
      4513 {
        :x 10
        :index [ 9 12 9 12 ]
        :id 0
        :type "road-blood"
        :y 79
        :l "ground"
        :library "subtile"
      }
      316 {
        :x 31
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-heaven"
        :y 5
        :l "ground"
        :library "subtile"
      }
      4166 {
        :x 5
        :index [ 6 9 6 9 ]
        :id 0
        :type "road-blood"
        :y 73
        :l "ground"
        :library "subtile"
      }
      314 {
        :x 29
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-heaven"
        :y 5
        :l "ground"
        :library "subtile"
      }
      2601 {
        :x 36
        :index [ 9 12 10 13 ]
        :id 0
        :type "road-green"
        :y 45
        :l "ground"
        :library "subtile"
      }
      312 {
        :x 27
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-heaven"
        :y 5
        :l "ground"
        :library "subtile"
      }
      4231 {
        :x 13
        :index [ 9 9 3 9 ]
        :id 0
        :type "road-blood"
        :y 74
        :l "ground"
        :library "subtile"
      }
      4572 {
        :x 12
        :index [ 8 11 10 13 ]
        :id 0
        :type "road-blood"
        :y 80
        :l "ground"
        :library "subtile"
      }
      2316 {
        :x 36
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-green"
        :y 40
        :l "ground"
        :library "subtile"
      }
      4062 {
        :x 15
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 71
        :l "ground"
        :library "subtile"
      }
      4060 {
        :x 13
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 71
        :l "ground"
        :library "subtile"
      }
      1676 {
        :x 23
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-ice"
        :y 29
        :l "ground"
        :library "subtile"
      }
      4512 {
        :x 9
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 79
        :l "ground"
        :library "subtile"
      }
      1668 {
        :x 15
        :index [ 5 8 7 10 ]
        :id 0
        :type "road-ice"
        :y 29
        :l "ground"
        :library "subtile"
      }
      3295 {
        :x 46
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-green"
        :y 57
        :l "ground"
        :library "subtile"
      }
      1433 {
        :x 8
        :index [ 8 11 10 13 ]
        :id 0
        :type "road-ice"
        :y 25
        :l "ground"
        :library "subtile"
      }
      3998 {
        :x 8
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-blood"
        :y 70
        :l "ground"
        :library "subtile"
      }
      4167 {
        :x 6
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 73
        :l "ground"
        :library "subtile"
      }
      3002 {
        :x 38
        :index [ 8 11 10 13 ]
        :id 0
        :type "road-green"
        :y 52
        :l "ground"
        :library "subtile"
      }
      2476 {
        :x 25
        :index [ 5 8 7 10 ]
        :id 0
        :type "road-green"
        :y 43
        :l "ground"
        :library "subtile"
      }
      4745 {
        :x 14
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-blood"
        :y 83
        :l "ground"
        :library "subtile"
      }
      4232 {
        :x 14
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 74
        :l "ground"
        :library "subtile"
      }
      4744 {
        :x 13
        :index [ 5 8 7 10 ]
        :id 0
        :type "road-blood"
        :y 83
        :l "ground"
        :library "subtile"
      }
      3168 {
        :x 33
        :index [ 6 9 6 9 ]
        :id 0
        :type "road-green"
        :y 55
        :l "ground"
        :library "subtile"
      }
      3409 {
        :x 46
        :index [ 6 9 7 10 ]
        :id 0
        :type "road-green"
        :y 59
        :l "ground"
        :library "subtile"
      }
      2429 {
        :x 35
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-green"
        :y 42
        :l "ground"
        :library "subtile"
      }
      4683 {
        :x 9
        :index [ 6 9 6 1 ]
        :id 0
        :type "road-blood"
        :y 82
        :l "ground"
        :library "subtile"
      }
      4626 {
        :x 9
        :index [ 6 9 6 9 ]
        :id 0
        :type "road-blood"
        :y 81
        :l "ground"
        :library "subtile"
      }
      4359 {
        :x 27
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-blood"
        :y 76
        :l "ground"
        :library "subtile"
      }
      4283 {
        :x 8
        :index [ 9 12 10 13 ]
        :id 0
        :type "road-blood"
        :y 75
        :l "ground"
        :library "subtile"
      }
      4339 {
        :x 7
        :index [ 9 12 9 12 ]
        :id 0
        :type "road-blood"
        :y 76
        :l "ground"
        :library "subtile"
      }
      4568 {
        :x 8
        :index [ 6 9 7 10 ]
        :id 0
        :type "road-blood"
        :y 80
        :l "ground"
        :library "subtile"
      }
      4168 {
        :x 7
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 73
        :l "ground"
        :library "subtile"
      }
      1899 {
        :x 18
        :index [ 6 9 7 10 ]
        :id 0
        :type "road-ice"
        :y 33
        :l "ground"
        :library "subtile"
      }
      4455 {
        :x 9
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 78
        :l "ground"
        :library "subtile"
      }
      4746 {
        :x 15
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-blood"
        :y 83
        :l "ground"
        :library "subtile"
      }
      4233 {
        :x 15
        :index [ 9 12 9 12 ]
        :id 0
        :type "road-blood"
        :y 74
        :l "ground"
        :library "subtile"
      }
      4456 {
        :x 10
        :index [ 9 12 9 12 ]
        :id 0
        :type "road-blood"
        :y 78
        :l "ground"
        :library "subtile"
      }
      2317 {
        :x 37
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-green"
        :y 40
        :l "ground"
        :library "subtile"
      }
      4063 {
        :x 16
        :index [ 9 9 9 1 ]
        :id 0
        :type "road-blood"
        :y 71
        :l "ground"
        :library "subtile"
      }
      4454 {
        :x 8
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 78
        :l "ground"
        :library "subtile"
      }
      4453 {
        :x 7
        :index [ 9 9 3 9 ]
        :id 0
        :type "road-blood"
        :y 78
        :l "ground"
        :library "subtile"
      }
      3683 {
        :x 35
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-blood"
        :y 64
        :l "ground"
        :library "subtile"
      }
      4399 {
        :x 10
        :index [ 8 8 9 1 ]
        :id 0
        :type "road-blood"
        :y 77
        :l "ground"
        :library "subtile"
      }
      4360 {
        :x 28
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-blood"
        :y 76
        :l "ground"
        :library "subtile"
      }
      4398 {
        :x 9
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-blood"
        :y 77
        :l "ground"
        :library "subtile"
      }
      3232 {
        :x 40
        :index [ 5 8 7 10 ]
        :id 0
        :type "road-green"
        :y 56
        :l "ground"
        :library "subtile"
      }
      3999 {
        :x 9
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-blood"
        :y 70
        :l "ground"
        :library "subtile"
      }
      3726 {
        :x 21
        :index [ 6 9 7 10 ]
        :id 0
        :type "road-blood"
        :y 65
        :l "ground"
        :library "subtile"
      }
      3457 {
        :x 37
        :index [ 5 8 7 10 ]
        :id 0
        :type "road-blood"
        :y 60
        :l "ground"
        :library "subtile"
      }
      2477 {
        :x 26
        :index [ 8 11 10 13 ]
        :id 0
        :type "road-green"
        :y 43
        :l "ground"
        :library "subtile"
      }
      4747 {
        :x 16
        :index [ 8 11 10 13 ]
        :id 0
        :type "road-blood"
        :y 83
        :l "ground"
        :library "subtile"
      }
      4403 {
        :x 14
        :index [ 6 12 7 13 ]
        :id 0
        :type "road-blood"
        :y 77
        :l "ground"
        :library "subtile"
      }
      1329 {
        :x 18
        :index [ 5 8 7 10 ]
        :id 0
        :type "road-ice"
        :y 23
        :l "ground"
        :library "subtile"
      }
      3169 {
        :x 34
        :index [ 9 9 9 1 ]
        :id 0
        :type "road-green"
        :y 55
        :l "ground"
        :library "subtile"
      }
      1843 {
        :x 19
        :index [ 9 9 9 1 ]
        :id 0
        :type "road-ice"
        :y 32
        :l "ground"
        :library "subtile"
      }
      2430 {
        :x 36
        :index [ 9 12 9 12 ]
        :id 0
        :type "road-green"
        :y 42
        :l "ground"
        :library "subtile"
      }
      4346 {
        :x 14
        :index [ 6 9 6 1 ]
        :id 0
        :type "road-blood"
        :y 76
        :l "ground"
        :library "subtile"
      }
      4289 {
        :x 14
        :index [ 9 9 3 9 ]
        :id 0
        :type "road-blood"
        :y 75
        :l "ground"
        :library "subtile"
      }
      4109 {
        :x 5
        :index [ 6 9 6 9 ]
        :id 0
        :type "road-blood"
        :y 72
        :l "ground"
        :library "subtile"
      }
      4361 {
        :x 29
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-blood"
        :y 76
        :l "ground"
        :library "subtile"
      }
      4530 {
        :x 27
        :index [ 6 9 6 1 ]
        :id 0
        :type "road-blood"
        :y 79
        :l "ground"
        :library "subtile"
      }
      4396 {
        :x 7
        :index [ 9 2 9 9 ]
        :id 0
        :type "road-blood"
        :y 77
        :l "ground"
        :library "subtile"
      }
      3994 {
        :x 4
        :index [ 8 8 3 9 ]
        :id 0
        :type "road-blood"
        :y 70
        :l "ground"
        :library "subtile"
      }
      2366 {
        :x 29
        :index [ 6 9 7 10 ]
        :id 0
        :type "road-green"
        :y 41
        :l "ground"
        :library "subtile"
      }
      2819 {
        :x 26
        :index [ 5 8 7 10 ]
        :id 0
        :type "road-green"
        :y 49
        :l "ground"
        :library "subtile"
      }
      4115 {
        :x 11
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 72
        :l "ground"
        :library "subtile"
      }
      3673 {
        :x 25
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-blood"
        :y 64
        :l "ground"
        :library "subtile"
      }
      4113 {
        :x 9
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 72
        :l "ground"
        :library "subtile"
      }
      4058 {
        :x 11
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 71
        :l "ground"
        :library "subtile"
      }
      3297 {
        :x 48
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-green"
        :y 57
        :l "ground"
        :library "subtile"
      }
      3052 {
        :x 31
        :index [ 6 9 7 10 ]
        :id 0
        :type "road-green"
        :y 53
        :l "ground"
        :library "subtile"
      }
      4057 {
        :x 10
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 71
        :l "ground"
        :library "subtile"
      }
      4056 {
        :x 9
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 71
        :l "ground"
        :library "subtile"
      }
      1677 {
        :x 24
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-ice"
        :y 29
        :l "ground"
        :library "subtile"
      }
      4111 {
        :x 7
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 72
        :l "ground"
        :library "subtile"
      }
      1426 {
        :x 1
        :index [ 5 8 7 10 ]
        :id 0
        :type "road-ice"
        :y 25
        :l "ground"
        :library "subtile"
      }
      4281 {
        :x 6
        :index [ 6 9 6 9 ]
        :id 0
        :type "road-blood"
        :y 75
        :l "ground"
        :library "subtile"
      }
      3233 {
        :x 41
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-green"
        :y 56
        :l "ground"
        :library "subtile"
      }
      4338 {
        :x 6
        :index [ 6 9 6 9 ]
        :id 0
        :type "road-blood"
        :y 76
        :l "ground"
        :library "subtile"
      }
      3727 {
        :x 22
        :index [ 9 9 10 10 ]
        :id 0
        :type "road-blood"
        :y 65
        :l "ground"
        :library "subtile"
      }
      3458 {
        :x 38
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-blood"
        :y 60
        :l "ground"
        :library "subtile"
      }
      4000 {
        :x 10
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-blood"
        :y 70
        :l "ground"
        :library "subtile"
      }
      4347 {
        :x 15
        :index [ 9 12 10 13 ]
        :id 0
        :type "road-blood"
        :y 76
        :l "ground"
        :library "subtile"
      }
      4290 {
        :x 15
        :index [ 9 12 9 12 ]
        :id 0
        :type "road-blood"
        :y 75
        :l "ground"
        :library "subtile"
      }
      1486 {
        :x 4
        :index [ 9 9 3 9 ]
        :id 0
        :type "road-ice"
        :y 26
        :l "ground"
        :library "subtile"
      }
      3170 {
        :x 35
        :index [ 9 12 10 13 ]
        :id 0
        :type "road-green"
        :y 55
        :l "ground"
        :library "subtile"
      }
      3411 {
        :x 48
        :index [ 9 9 9 1 ]
        :id 0
        :type "road-green"
        :y 59
        :l "ground"
        :library "subtile"
      }
      4064 {
        :x 17
        :index [ 9 12 10 13 ]
        :id 0
        :type "road-blood"
        :y 71
        :l "ground"
        :library "subtile"
      }
      3056 {
        :x 35
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-green"
        :y 53
        :l "ground"
        :library "subtile"
      }
      4587 {
        :x 27
        :index [ 6 12 6 12 ]
        :id 0
        :type "road-blood"
        :y 80
        :l "ground"
        :library "subtile"
      }
      3680 {
        :x 32
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-blood"
        :y 64
        :l "ground"
        :library "subtile"
      }
      4363 {
        :x 31
        :index [ 8 8 9 1 ]
        :id 0
        :type "road-blood"
        :y 76
        :l "ground"
        :library "subtile"
      }
      4352 {
        :x 20
        :index [ 5 8 7 10 ]
        :id 0
        :type "road-blood"
        :y 76
        :l "ground"
        :library "subtile"
      }
      4395 {
        :x 6
        :index [ 6 9 6 9 ]
        :id 0
        :type "road-blood"
        :y 77
        :l "ground"
        :library "subtile"
      }
      4051 {
        :x 4
        :index [ 6 9 7 10 ]
        :id 0
        :type "road-blood"
        :y 71
        :l "ground"
        :library "subtile"
      }
      2367 {
        :x 30
        :index [ 9 9 10 10 ]
        :id 0
        :type "road-green"
        :y 41
        :l "ground"
        :library "subtile"
      }
      2877 {
        :x 27
        :index [ 6 9 6 1 ]
        :id 0
        :type "road-green"
        :y 50
        :l "ground"
        :library "subtile"
      }
      1900 {
        :x 19
        :index [ 9 12 3 12 ]
        :id 0
        :type "road-ice"
        :y 33
        :l "ground"
        :library "subtile"
      }
      4007 {
        :x 17
        :index [ 8 8 9 1 ]
        :id 0
        :type "road-blood"
        :y 70
        :l "ground"
        :library "subtile"
      }
      4053 {
        :x 6
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 71
        :l "ground"
        :library "subtile"
      }
      3114 {
        :x 36
        :index [ 9 12 10 13 ]
        :id 0
        :type "road-green"
        :y 54
        :l "ground"
        :library "subtile"
      }
      2319 {
        :x 39
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-green"
        :y 40
        :l "ground"
        :library "subtile"
      }
      3053 {
        :x 32
        :index [ 9 9 3 9 ]
        :id 0
        :type "road-green"
        :y 53
        :l "ground"
        :library "subtile"
      }
      4169 {
        :x 8
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 73
        :l "ground"
        :library "subtile"
      }
      4172 {
        :x 11
        :index [ 9 9 10 10 ]
        :id 0
        :type "road-blood"
        :y 73
        :l "ground"
        :library "subtile"
      }
      2543 {
        :x 35
        :index [ 9 9 3 9 ]
        :id 0
        :type "road-green"
        :y 44
        :l "ground"
        :library "subtile"
      }
      4171 {
        :x 10
        :index [ 9 9 10 10 ]
        :id 0
        :type "road-blood"
        :y 73
        :l "ground"
        :library "subtile"
      }
      4364 {
        :x 32
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-blood"
        :y 76
        :l "ground"
        :library "subtile"
      }
      3728 {
        :x 23
        :index [ 9 9 3 9 ]
        :id 0
        :type "road-blood"
        :y 65
        :l "ground"
        :library "subtile"
      }
      3234 {
        :x 42
        :index [ 8 8 3 9 ]
        :id 0
        :type "road-green"
        :y 56
        :l "ground"
        :library "subtile"
      }
      4170 {
        :x 9
        :index [ 9 9 9 1 ]
        :id 0
        :type "road-blood"
        :y 73
        :l "ground"
        :library "subtile"
      }
      4173 {
        :x 12
        :index [ 9 9 3 9 ]
        :id 0
        :type "road-blood"
        :y 73
        :l "ground"
        :library "subtile"
      }
      1549 {
        :x 10
        :index [ 5 8 7 10 ]
        :id 0
        :type "road-ice"
        :y 27
        :l "ground"
        :library "subtile"
      }
      4001 {
        :x 11
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-blood"
        :y 70
        :l "ground"
        :library "subtile"
      }
      4118 {
        :x 14
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 72
        :l "ground"
        :library "subtile"
      }
      4117 {
        :x 13
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 72
        :l "ground"
        :library "subtile"
      }
      1330 {
        :x 19
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-ice"
        :y 23
        :l "ground"
        :library "subtile"
      }
      4008 {
        :x 18
        :index [ 8 11 10 13 ]
        :id 0
        :type "road-blood"
        :y 70
        :l "ground"
        :library "subtile"
      }
      3412 {
        :x 49
        :index [ 9 12 10 13 ]
        :id 0
        :type "road-green"
        :y 59
        :l "ground"
        :library "subtile"
      }
      1844 {
        :x 20
        :index [ 9 12 10 13 ]
        :id 0
        :type "road-ice"
        :y 32
        :l "ground"
        :library "subtile"
      }
      1051 {
        :x 25
        :index [ 5 8 7 10 ]
        :id 0
        :type "road-ice"
        :y 18
        :l "ground"
        :library "subtile"
      }
      3681 {
        :x 33
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-blood"
        :y 64
        :l "ground"
        :library "subtile"
      }
      4365 {
        :x 33
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-blood"
        :y 76
        :l "ground"
        :library "subtile"
      }
      4110 {
        :x 6
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 72
        :l "ground"
        :library "subtile"
      }
      4397 {
        :x 8
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-blood"
        :y 77
        :l "ground"
        :library "subtile"
      }
      2368 {
        :x 31
        :index [ 9 9 10 10 ]
        :id 0
        :type "road-green"
        :y 41
        :l "ground"
        :library "subtile"
      }
      4174 {
        :x 13
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 73
        :l "ground"
        :library "subtile"
      }
      2878 {
        :x 28
        :index [ 9 12 10 13 ]
        :id 0
        :type "road-green"
        :y 50
        :l "ground"
        :library "subtile"
      }
      4055 {
        :x 8
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 71
        :l "ground"
        :library "subtile"
      }
      4006 {
        :x 16
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-blood"
        :y 70
        :l "ground"
        :library "subtile"
      }
      4005 {
        :x 15
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-blood"
        :y 70
        :l "ground"
        :library "subtile"
      }
      2320 {
        :x 40
        :index [ 8 11 10 13 ]
        :id 0
        :type "road-green"
        :y 40
        :l "ground"
        :library "subtile"
      }
      3299 {
        :x 50
        :index [ 9 9 9 1 ]
        :id 0
        :type "road-green"
        :y 57
        :l "ground"
        :library "subtile"
      }
      3054 {
        :x 33
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-green"
        :y 53
        :l "ground"
        :library "subtile"
      }
      1911 {
        :x 30
        :index [ 8 8 3 1 ]
        :id 0
        :type "road-ice"
        :y 33
        :l "ground"
        :library "subtile"
      }
      2544 {
        :x 36
        :index [ 9 12 9 12 ]
        :id 0
        :type "road-green"
        :y 44
        :l "ground"
        :library "subtile"
      }
      1678 {
        :x 25
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-ice"
        :y 29
        :l "ground"
        :library "subtile"
      }
      3993 {
        :x 3
        :index [ 5 8 7 10 ]
        :id 0
        :type "road-blood"
        :y 70
        :l "ground"
        :library "subtile"
      }
      1427 {
        :x 2
        :index [ 8 8 3 9 ]
        :id 0
        :type "road-ice"
        :y 25
        :l "ground"
        :library "subtile"
      }
      3729 {
        :x 24
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 65
        :l "ground"
        :library "subtile"
      }
      3235 {
        :x 43
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-green"
        :y 56
        :l "ground"
        :library "subtile"
      }
      3995 {
        :x 5
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-blood"
        :y 70
        :l "ground"
        :library "subtile"
      }
      4175 {
        :x 14
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 73
        :l "ground"
        :library "subtile"
      }
      1957 {
        :x 19
        :index [ 6 12 7 13 ]
        :id 0
        :type "road-ice"
        :y 34
        :l "ground"
        :library "subtile"
      }
      4002 {
        :x 12
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-blood"
        :y 70
        :l "ground"
        :library "subtile"
      }
      3461 {
        :x 41
        :index [ 8 11 10 13 ]
        :id 0
        :type "road-blood"
        :y 60
        :l "ground"
        :library "subtile"
      }
      3460 {
        :x 40
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-blood"
        :y 60
        :l "ground"
        :library "subtile"
      }
      3459 {
        :x 39
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-blood"
        :y 60
        :l "ground"
        :library "subtile"
      }
      2701 {
        :x 22
        :index [ 6 9 6 9 ]
        :id 0
        :type "road-green"
        :y 47
        :l "ground"
        :library "subtile"
      }
      3909 {
        :x 33
        :index [ 6 12 6 12 ]
        :id 0
        :type "road-blood"
        :y 68
        :l "ground"
        :library "subtile"
      }
      3852 {
        :x 33
        :index [ 9 12 3 12 ]
        :id 0
        :type "road-blood"
        :y 67
        :l "ground"
        :library "subtile"
      }
      3687 {
        :x 39
        :index [ 8 11 10 13 ]
        :id 0
        :type "road-blood"
        :y 64
        :l "ground"
        :library "subtile"
      }
      2192 {
        :x 26
        :index [ 6 12 7 13 ]
        :id 0
        :type "road-green"
        :y 38
        :l "ground"
        :library "subtile"
      }
      3682 {
        :x 34
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-blood"
        :y 64
        :l "ground"
        :library "subtile"
      }
      4367 {
        :x 35
        :index [ 8 11 10 13 ]
        :id 0
        :type "road-blood"
        :y 76
        :l "ground"
        :library "subtile"
      }
      3667 {
        :x 19
        :index [ 5 8 7 10 ]
        :id 0
        :type "road-blood"
        :y 64
        :l "ground"
        :library "subtile"
      }
      4112 {
        :x 8
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 72
        :l "ground"
        :library "subtile"
      }
      2369 {
        :x 32
        :index [ 9 9 3 9 ]
        :id 0
        :type "road-green"
        :y 41
        :l "ground"
        :library "subtile"
      }
      4355 {
        :x 23
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-blood"
        :y 76
        :l "ground"
        :library "subtile"
      }
      4176 {
        :x 15
        :index [ 9 12 9 12 ]
        :id 0
        :type "road-blood"
        :y 73
        :l "ground"
        :library "subtile"
      }
      3742 {
        :x 37
        :index [ 9 12 10 13 ]
        :id 0
        :type "road-blood"
        :y 65
        :l "ground"
        :library "subtile"
      }
      3741 {
        :x 36
        :index [ 9 9 10 10 ]
        :id 0
        :type "road-blood"
        :y 65
        :l "ground"
        :library "subtile"
      }
      3739 {
        :x 34
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 65
        :l "ground"
        :library "subtile"
      }
      3794 {
        :x 32
        :index [ 6 9 6 9 ]
        :id 0
        :type "road-blood"
        :y 66
        :l "ground"
        :library "subtile"
      }
      3300 {
        :x 51
        :index [ 9 12 10 13 ]
        :id 0
        :type "road-green"
        :y 57
        :l "ground"
        :library "subtile"
      }
      3055 {
        :x 34
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-green"
        :y 53
        :l "ground"
        :library "subtile"
      }
      1909 {
        :x 28
        :index [ 5 8 7 10 ]
        :id 0
        :type "road-ice"
        :y 33
        :l "ground"
        :library "subtile"
      }
      3525 {
        :x 48
        :index [ 6 12 6 12 ]
        :id 0
        :type "road-green"
        :y 61
        :l "ground"
        :library "subtile"
      }
      4627 {
        :x 10
        :index [ 9 9 9 1 ]
        :id 0
        :type "road-blood"
        :y 81
        :l "ground"
        :library "subtile"
      }
      3685 {
        :x 37
        :index [ 8 8 9 1 ]
        :id 0
        :type "road-blood"
        :y 64
        :l "ground"
        :library "subtile"
      }
      4358 {
        :x 26
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-blood"
        :y 76
        :l "ground"
        :library "subtile"
      }
      3730 {
        :x 25
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 65
        :l "ground"
        :library "subtile"
      }
      3236 {
        :x 44
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-green"
        :y 56
        :l "ground"
        :library "subtile"
      }
      4400 {
        :x 11
        :index [ 8 11 10 13 ]
        :id 0
        :type "road-blood"
        :y 77
        :l "ground"
        :library "subtile"
      }
      3679 {
        :x 31
        :index [ 5 8 6 9 ]
        :id 0
        :type "road-blood"
        :y 64
        :l "ground"
        :library "subtile"
      }
      1550 {
        :x 11
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-ice"
        :y 27
        :l "ground"
        :library "subtile"
      }
      4003 {
        :x 13
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-blood"
        :y 70
        :l "ground"
        :library "subtile"
      }
      3845 {
        :x 26
        :index [ 9 12 10 13 ]
        :id 0
        :type "road-blood"
        :y 67
        :l "ground"
        :library "subtile"
      }
      1910 {
        :x 29
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-ice"
        :y 33
        :l "ground"
        :library "subtile"
      }
      1331 {
        :x 20
        :index [ 8 8 3 9 ]
        :id 0
        :type "road-ice"
        :y 23
        :l "ground"
        :library "subtile"
      }
      2702 {
        :x 23
        :index [ 9 9 9 1 ]
        :id 0
        :type "road-green"
        :y 47
        :l "ground"
        :library "subtile"
      }
      3672 {
        :x 24
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-blood"
        :y 64
        :l "ground"
        :library "subtile"
      }
      3786 {
        :x 24
        :index [ 9 9 10 10 ]
        :id 0
        :type "road-blood"
        :y 66
        :l "ground"
        :library "subtile"
      }
      4357 {
        :x 25
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-blood"
        :y 76
        :l "ground"
        :library "subtile"
      }
      4628 {
        :x 11
        :index [ 9 12 10 13 ]
        :id 0
        :type "road-blood"
        :y 81
        :l "ground"
        :library "subtile"
      }
      1052 {
        :x 26
        :index [ 8 8 3 9 ]
        :id 0
        :type "road-ice"
        :y 18
        :l "ground"
        :library "subtile"
      }
      3675 {
        :x 27
        :index [ 8 11 9 12 ]
        :id 0
        :type "road-blood"
        :y 64
        :l "ground"
        :library "subtile"
      }
      4114 {
        :x 10
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 72
        :l "ground"
        :library "subtile"
      }
      3844 {
        :x 25
        :index [ 6 9 6 1 ]
        :id 0
        :type "road-blood"
        :y 67
        :l "ground"
        :library "subtile"
      }
      2370 {
        :x 33
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-green"
        :y 41
        :l "ground"
        :library "subtile"
      }
      1669 {
        :x 16
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-ice"
        :y 29
        :l "ground"
        :library "subtile"
      }
      1484 {
        :x 2
        :index [ 6 9 7 10 ]
        :id 0
        :type "road-ice"
        :y 26
        :l "ground"
        :library "subtile"
      }
      2645 {
        :x 23
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-green"
        :y 46
        :l "ground"
        :library "subtile"
      }
      1673 {
        :x 20
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-ice"
        :y 29
        :l "ground"
        :library "subtile"
      }
      2816 {
        :x 23
        :index [ 6 12 7 13 ]
        :id 0
        :type "road-green"
        :y 49
        :l "ground"
        :library "subtile"
      }
      3795 {
        :x 33
        :index [ 9 9 9 1 ]
        :id 0
        :type "road-blood"
        :y 66
        :l "ground"
        :library "subtile"
      }
      1674 {
        :x 21
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-ice"
        :y 29
        :l "ground"
        :library "subtile"
      }
      1508 {
        :x 26
        :index [ 5 8 7 10 ]
        :id 0
        :type "road-ice"
        :y 26
        :l "ground"
        :library "subtile"
      }
      4052 {
        :x 5
        :index [ 9 9 3 9 ]
        :id 0
        :type "road-blood"
        :y 71
        :l "ground"
        :library "subtile"
      }
      3112 {
        :x 34
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-green"
        :y 54
        :l "ground"
        :library "subtile"
      }
      1679 {
        :x 26
        :index [ 8 11 10 13 ]
        :id 0
        :type "road-ice"
        :y 29
        :l "ground"
        :library "subtile"
      }
      1428 {
        :x 3
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-ice"
        :y 25
        :l "ground"
        :library "subtile"
      }
      1671 {
        :x 18
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-ice"
        :y 29
        :l "ground"
        :library "subtile"
      }
      3731 {
        :x 26
        :index [ 9 9 9 1 ]
        :id 0
        :type "road-blood"
        :y 65
        :l "ground"
        :library "subtile"
      }
      3237 {
        :x 45
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-green"
        :y 56
        :l "ground"
        :library "subtile"
      }
      2318 {
        :x 38
        :index [ 8 8 9 1 ]
        :id 0
        :type "road-green"
        :y 40
        :l "ground"
        :library "subtile"
      }
      4362 {
        :x 30
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-blood"
        :y 76
        :l "ground"
        :library "subtile"
      }
      3282 {
        :x 33
        :index [ 6 12 7 13 ]
        :id 0
        :type "road-green"
        :y 57
        :l "ground"
        :library "subtile"
      }
      4004 {
        :x 14
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-blood"
        :y 70
        :l "ground"
        :library "subtile"
      }
      6236 {
        :x 23
        :index [ 5 8 7 10 ]
        :id 0
        :type "road-hell"
        :y 109
        :l "ground"
        :library "subtile"
      }
      2134 {
        :x 25
        :index [ 5 8 7 10 ]
        :id 0
        :type "road-green"
        :y 37
        :l "ground"
        :library "subtile"
      }
      3668 {
        :x 20
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-blood"
        :y 64
        :l "ground"
        :library "subtile"
      }
      2703 {
        :x 24
        :index [ 9 9 10 10 ]
        :id 0
        :type "road-green"
        :y 47
        :l "ground"
        :library "subtile"
      }
      4531 {
        :x 28
        :index [ 9 12 10 13 ]
        :id 0
        :type "road-blood"
        :y 79
        :l "ground"
        :library "subtile"
      }
      2998 {
        :x 34
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-green"
        :y 52
        :l "ground"
        :library "subtile"
      }
      1670 {
        :x 17
        :index [ 8 8 3 9 ]
        :id 0
        :type "road-ice"
        :y 29
        :l "ground"
        :library "subtile"
      }
      1785 {
        :x 18
        :index [ 9 9 3 9 ]
        :id 0
        :type "road-ice"
        :y 31
        :l "ground"
        :library "subtile"
      }
      3684 {
        :x 36
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-blood"
        :y 64
        :l "ground"
        :library "subtile"
      }
      4854 {
        :x 9
        :index [ 6 12 7 13 ]
        :id 0
        :type "road-blood"
        :y 85
        :l "ground"
        :library "subtile"
      }
      4116 {
        :x 12
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 72
        :l "ground"
        :library "subtile"
      }
      3110 {
        :x 32
        :index [ 6 9 7 10 ]
        :id 0
        :type "road-green"
        :y 54
        :l "ground"
        :library "subtile"
      }
      2371 {
        :x 34
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-green"
        :y 41
        :l "ground"
        :library "subtile"
      }
      2308 {
        :x 28
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-green"
        :y 40
        :l "ground"
        :library "subtile"
      }
      1788 {
        :x 21
        :index [ 9 12 10 13 ]
        :id 0
        :type "road-ice"
        :y 31
        :l "ground"
        :library "subtile"
      }
      1054 {
        :x 28
        :index [ 8 8 9 1 ]
        :id 0
        :type "road-ice"
        :y 18
        :l "ground"
        :library "subtile"
      }
      6237 {
        :x 24
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-hell"
        :y 109
        :l "ground"
        :library "subtile"
      }
      2542 {
        :x 34
        :index [ 6 9 7 10 ]
        :id 0
        :type "road-green"
        :y 44
        :l "ground"
        :library "subtile"
      }
      3796 {
        :x 34
        :index [ 9 9 10 10 ]
        :id 0
        :type "road-blood"
        :y 66
        :l "ground"
        :library "subtile"
      }
      3351 {
        :x 45
        :index [ 9 9 10 10 ]
        :id 0
        :type "road-green"
        :y 58
        :l "ground"
        :library "subtile"
      }
      2307 {
        :x 27
        :index [ 5 8 7 10 ]
        :id 0
        :type "road-green"
        :y 40
        :l "ground"
        :library "subtile"
      }
      1388 {
        :x 20
        :index [ 6 9 7 10 ]
        :id 0
        :type "road-ice"
        :y 24
        :l "ground"
        :library "subtile"
      }
      1109 {
        :x 26
        :index [ 6 9 7 10 ]
        :id 0
        :type "road-ice"
        :y 19
        :l "ground"
        :library "subtile"
      }
      3057 {
        :x 36
        :index [ 9 12 9 12 ]
        :id 0
        :type "road-green"
        :y 53
        :l "ground"
        :library "subtile"
      }
      3352 {
        :x 46
        :index [ 9 9 3 9 ]
        :id 0
        :type "road-green"
        :y 58
        :l "ground"
        :library "subtile"
      }
      3243 {
        :x 51
        :index [ 8 8 9 1 ]
        :id 0
        :type "road-green"
        :y 56
        :l "ground"
        :library "subtile"
      }
      3732 {
        :x 27
        :index [ 9 12 10 13 ]
        :id 0
        :type "road-blood"
        :y 65
        :l "ground"
        :library "subtile"
      }
      3238 {
        :x 46
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-green"
        :y 56
        :l "ground"
        :library "subtile"
      }
      1543 {
        :x 4
        :index [ 6 9 6 1 ]
        :id 0
        :type "road-ice"
        :y 27
        :l "ground"
        :library "subtile"
      }
      3111 {
        :x 33
        :index [ 9 9 3 9 ]
        :id 0
        :type "road-green"
        :y 54
        :l "ground"
        :library "subtile"
      }
      1551 {
        :x 12
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-ice"
        :y 27
        :l "ground"
        :library "subtile"
      }
      2993 {
        :x 29
        :index [ 5 8 7 10 ]
        :id 0
        :type "road-green"
        :y 52
        :l "ground"
        :library "subtile"
      }
      6238 {
        :x 25
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-hell"
        :y 109
        :l "ground"
        :library "subtile"
      }
      311 {
        :x 26
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-heaven"
        :y 5
        :l "ground"
        :library "subtile"
      }
      3669 {
        :x 21
        :index [ 8 8 3 9 ]
        :id 0
        :type "road-blood"
        :y 64
        :l "ground"
        :library "subtile"
      }
      1332 {
        :x 21
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-ice"
        :y 23
        :l "ground"
        :library "subtile"
      }
      3292 {
        :x 43
        :index [ 9 9 10 10 ]
        :id 0
        :type "road-green"
        :y 57
        :l "ground"
        :library "subtile"
      }
      308 {
        :x 23
        :index [ 5 8 7 10 ]
        :id 0
        :type "road-heaven"
        :y 5
        :l "ground"
        :library "subtile"
      }
      309 {
        :x 24
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-heaven"
        :y 5
        :l "ground"
        :library "subtile"
      }
      2704 {
        :x 25
        :index [ 9 12 10 13 ]
        :id 0
        :type "road-green"
        :y 47
        :l "ground"
        :library "subtile"
      }
      1053 {
        :x 27
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-ice"
        :y 18
        :l "ground"
        :library "subtile"
      }
      3958 {
        :x 25
        :index [ 6 12 7 13 ]
        :id 0
        :type "road-blood"
        :y 69
        :l "ground"
        :library "subtile"
      }
      1728 {
        :x 18
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-ice"
        :y 30
        :l "ground"
        :library "subtile"
      }
      317 {
        :x 32
        :index [ 8 11 10 13 ]
        :id 0
        :type "road-heaven"
        :y 5
        :l "ground"
        :library "subtile"
      }
      2372 {
        :x 35
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-green"
        :y 41
        :l "ground"
        :library "subtile"
      }
      315 {
        :x 30
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-heaven"
        :y 5
        :l "ground"
        :library "subtile"
      }
      1485 {
        :x 3
        :index [ 9 9 10 10 ]
        :id 0
        :type "road-ice"
        :y 26
        :l "ground"
        :library "subtile"
      }
      313 {
        :x 28
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-heaven"
        :y 5
        :l "ground"
        :library "subtile"
      }
      6239 {
        :x 26
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-hell"
        :y 109
        :l "ground"
        :library "subtile"
      }
      1727 {
        :x 17
        :index [ 6 9 6 9 ]
        :id 0
        :type "road-ice"
        :y 30
        :l "ground"
        :library "subtile"
      }
      3797 {
        :x 35
        :index [ 9 12 10 13 ]
        :id 0
        :type "road-blood"
        :y 66
        :l "ground"
        :library "subtile"
      }
      3293 {
        :x 44
        :index [ 9 9 3 9 ]
        :id 0
        :type "road-green"
        :y 57
        :l "ground"
        :library "subtile"
      }
      1784 {
        :x 17
        :index [ 6 9 7 10 ]
        :id 0
        :type "road-ice"
        :y 31
        :l "ground"
        :library "subtile"
      }
      4054 {
        :x 7
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-blood"
        :y 71
        :l "ground"
        :library "subtile"
      }
      3113 {
        :x 35
        :index [ 9 9 9 1 ]
        :id 0
        :type "road-green"
        :y 54
        :l "ground"
        :library "subtile"
      }
      3350 {
        :x 44
        :index [ 6 9 7 10 ]
        :id 0
        :type "road-green"
        :y 58
        :l "ground"
        :library "subtile"
      }
      1429 {
        :x 4
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-ice"
        :y 25
        :l "ground"
        :library "subtile"
      }
      4353 {
        :x 21
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-blood"
        :y 76
        :l "ground"
        :library "subtile"
      }
      4119 {
        :x 15
        :index [ 9 9 9 1 ]
        :id 0
        :type "road-blood"
        :y 72
        :l "ground"
        :library "subtile"
      }
      3239 {
        :x 47
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-green"
        :y 56
        :l "ground"
        :library "subtile"
      }
      1732 {
        :x 22
        :index [ 9 12 10 13 ]
        :id 0
        :type "road-ice"
        :y 30
        :l "ground"
        :library "subtile"
      }
      3298 {
        :x 49
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-green"
        :y 57
        :l "ground"
        :library "subtile"
      }
      2484 {
        :x 33
        :index [ 6 9 7 10 ]
        :id 0
        :type "road-green"
        :y 43
        :l "ground"
        :library "subtile"
      }
      2994 {
        :x 30
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-green"
        :y 52
        :l "ground"
        :library "subtile"
      }
      4366 {
        :x 34
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-blood"
        :y 76
        :l "ground"
        :library "subtile"
      }
      6240 {
        :x 27
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-hell"
        :y 109
        :l "ground"
        :library "subtile"
      }
      3670 {
        :x 22
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-blood"
        :y 64
        :l "ground"
        :library "subtile"
      }
      3410 {
        :x 47
        :index [ 9 9 3 9 ]
        :id 0
        :type "road-green"
        :y 59
        :l "ground"
        :library "subtile"
      }
      1509 {
        :x 27
        :index [ 8 8 10 10 ]
        :id 0
        :type "road-ice"
        :y 26
        :l "ground"
        :library "subtile"
      }
      2310 {
        :x 30
        :index [ 8 8 9 9 ]
        :id 0
        :type "road-green"
        :y 40
        :l "ground"
        :library "subtile"
      }
      2820 {
        :x 27
        :index [ 8 8 3 9 ]
        :id 0
        :type "road-green"
        :y 49
        :l "ground"
        :library "subtile"
      }
      2821 {
        :x 28
        :index [ 8 8 9 1 ]
        :id 0
        :type "road-green"
        :y 49
        :l "ground"
        :library "subtile"
      }
      2642 {
        :x 20
        :index [ 5 8 7 10 ]
        :id 0
        :type "road-green"
        :y 46
        :l "ground"
        :library "subtile"
      }
      3296 {
        :x 47
        :index [ 9 9 9 9 ]
        :id 0
        :type "road-green"
        :y 57
        :l "ground"
        :library "subtile"
      }
      4120 {
        :x 16
        :index [ 9 12 10 13 ]
        :id 0
        :type "road-blood"
        :y 72
        :l "ground"
        :library "subtile"
      }
      1600 {
        :x 4
        :index [ 6 12 7 13 ]
        :id 0
        :type "road-ice"
        :y 28
        :l "ground"
        :library "subtile"
      }
      2373 {
        :x 36
        :index [ 9 9 9 1 ]
        :id 0
        :type "road-green"
        :y 41
        :l "ground"
        :library "subtile"
      }
    }
  }
  :id 0
  :height 150
  :tilesize 16
}