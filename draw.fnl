(fn sigmoid [x xo k l]
    (/ l (+ 1 (math.exp (- (* k ( - x xo)))))))

(fn draw-sun []
    (local j (/ (- state.camera.y) 5))
    (fn trans-sun [j start end sv ev]
        (local jp (- j start))
        (local h (- end start))
        (local percent  (sigmoid (/ jp h) 0.5 1 1))
        (local a  (+ sv (* (- ev sv)  percent)))
        a
        )
    (local alpha (if (< j 30) 0
                   (< j 80) (trans-sun j 30 80 1 0)
                   (< j (- 1440 120)) 1
                   (< j 1440) (trans-sun j (- 1440 120) 1440 0 1)
                   0))
    (love.graphics.setColor 1 1 1 alpha)
    (love.graphics.draw state.game.sun 0 0 0 5 5)
    (love.graphics.setColor 1 1 1 1))

(fn draw-tiles []
    (love.graphics.draw state.game.tilesetBatch state.camera.x state.camera.y 0 5 5))

(fn draw-clouds []
    (local cloud-y-offset (- (* 5 6 120)))
        (love.graphics.draw state.game.clouds (+ (* 5 1) (/ state.camera.x 2)) (math.floor (/ (+ cloud-y-offset state.camera.y) 2)) 0 5 5))

(fn draw-background [pfbs]
    (pfbs.game.background-draw (/ (* state.game.mapin.width 16) 5) (/ (* state.game.mapin.height 16) 5) (- (/ state.camera.x 5)) (- (/ state.camera.y 5)) 5 5))

{:draw-sun draw-sun
           :draw-tiles draw-tiles
           :draw-clouds draw-clouds
           :draw-background draw-background}
