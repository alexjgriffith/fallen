;; prefabs

(local subtile (require "subtile"))
(local map (require "map"))

(local pfbs {:editor {:brush nil} :game {} :grid-size 8})

(local  sample-sprite-sheet
        (love.graphics.newImage "assets/all-tiles.png"))

(local restart (require "restart"))

(var season "winter")
(var extended-seasons? true)


(local sample-grid
       (subtile.newGrid
        pfbs.grid-size pfbs.grid-size
        (: sample-sprite-sheet :getWidth)
        (: sample-sprite-sheet :getHeight)))

(local tiles {:tree-ice (subtile.rect sample-grid 6 1 2 4)
              :tree-green (subtile.rect sample-grid 8 1 2 4)
              :tree-blood (subtile.rect sample-grid 10 1 2 4)
              :big-tree-ice (subtile.rect sample-grid 6 5 4 6)
              :big-tree-green (subtile.rect sample-grid 10 5 4 6)
              :big-tree-blood (subtile.rect sample-grid 14 5 4 6)
              :house1 (subtile.rect sample-grid 1 31 8 6)
              :house2 (subtile.rect sample-grid 10 31 6 6)
              :house3 (subtile.rect sample-grid 17 31 6 6)
              :house4 (subtile.rect sample-grid 24 31 5 6)
              :eye-ice (subtile.rect sample-grid 1 38 3 3)
              :eye-green (subtile.rect sample-grid 4 38 3 3)
              :eye-blood (subtile.rect sample-grid 7 38 3 3)              
              :stone-ice (subtile.rect sample-grid 11 11 2 4)
              :stone-green (subtile.rect sample-grid 13 11 2 4)
              :stone-blood (subtile.rect sample-grid 15 11 2 4)
              :cloud (subtile.rect sample-grid 1 10 3 2)
              :road-ice  (subtile.square10 sample-grid 1 1 3 1)
              :road-green  (subtile.square10 sample-grid 1 4 3 4)
              :road-blood  (subtile.square10 sample-grid 1 7 3 7)
              :road-heaven  (subtile.square10 sample-grid 1 25 3 25)
              :road-hell  (subtile.square10 sample-grid 1 28 3 28)
              :road-text  (subtile.square10 sample-grid 1 22 3 22)
              :sun (subtile.rect sample-grid 1 13 10 8)
              :ui-box (subtile.square10 sample-grid 1 22 3 22)
              })

;; (fn set-tiles-winter []
;;     (tset tiles :tree (subtile.rect sample-grid 6 1 2 4))
;;     (tset tiles :road  (subtile.square10 sample-grid 1 1 3 1))
;;     (tset tiles :big-tree (subtile.rect sample-grid 6 5 4 6)))

;; (fn set-tiles-summer []
;;     (tset tiles :tree (subtile.rect sample-grid 8 1 2 4))
;;     (tset tiles :road  (subtile.square10 sample-grid 1 4 3 4))
;;     (tset tiles :big-tree (subtile.rect sample-grid 10 5 4 6)))

(fn make-season-prefab [tile-ice tile-green tile-blood w h ?level]
    (fn  [mapin x y]    
         (match season
                "summer" (map.add-tile mapin x y (or ?level :objs) (map.tile tile-green :tile w h))
                "blood" (map.add-tile mapin x y (or ?level :objs) (map.tile tile-blood :tile w h))
                (map.add-tile mapin x y (or ?level :objs) (map.tile tile-ice :tile w h)))))

(fn make-prefab [tile w h ?level]
    (fn  [mapin x y]    
         (map.add-tile mapin x y (or ?level :objs) (map.tile tile :tile w h))))

(local make-tree (make-season-prefab :tree-ice :tree-green :tree-blood 2 4))

(local make-big-tree (make-season-prefab :big-tree-ice :big-tree-green :big-tree-blood 4 6))

(local make-eye (make-season-prefab :eye-ice :eye-green :eye-blood 3 3 :for1) )

(local make-stone (make-season-prefab :stone-ice :stone-green :stone-blood 2 4))


(local make-house1 (make-prefab :house1 8 6))
(local make-house2 (make-prefab :house2 6 6))
(local make-house3 (make-prefab :house3 6 6))
(local make-house4 (make-prefab :house4 5 6))

;; (fn make-big-tree [mapin x y]
;;     (map.add-tile mapin x y :objs (map.tile :big-tree :tile 4 6)))

(fn make-sun [mapin x y]
    (map.add-tile mapin x  y :sun (map.tile :sun :tile 10 8)))

(fn make-cloud [mapin x y]
    (map.add-tile mapin x y :clouds (map.tile :cloud :tile 3 2)))

(fn make-road-ice [mapin x y]
    (map.add-tile mapin x y :ground (map.tile :road-ice :subtile)))

(fn make-road-green [mapin x y]
    (map.add-tile mapin x y :ground (map.tile :road-green :subtile)))

(fn make-road-blood [mapin x y]
    (map.add-tile mapin x y :ground (map.tile :road-blood :subtile)))

(fn make-road-heaven [mapin x y]
    (map.add-tile mapin x y :ground (map.tile :road-heaven :subtile)))

(fn make-road-hell [mapin x y]
    (map.add-tile mapin x y :ground (map.tile :road-hell :subtile)))

(fn make-road-text [mapin x y]
    (map.add-tile mapin x y :ground (map.tile :road-text :subtile)))

(fn make-road-season [mapin x y]
    (match season
           "winter" (make-road-ice mapin x y)
           "summer" (make-road-green mapin x y)
           "blood" (make-road-blood mapin x y)
           "text" (make-road-text mapin x y)
           "heaven" (make-road-heaven mapin x y)
           "hell" (make-road-hell mapin x y))

    )

(fn make-ui-box [mapin x y]
    (map.add-tile mapin x y :for1 (map.tile :ui-box :subtile))
    (map.update-neighbours mapin :for1))

;; perhaps move this to a UI page?
(fn make-text-box [mapin x y w h]
    (for [i 0 (- w 1)]
         (for [j 0 (- h 1)]
              (map.add-tile mapin (+ x i) (+ y j) :for1 (map.tile :ui-box :subtile))))
    (map.update-neighbours mapin :for1))


(fn sigmoid [x xo k l]
    (/ l (+ 1 (math.exp (- (* k ( - x xo)))))))

(fn newBackground [w h]
    (local data (love.image.newImageData w h))
    (local white [1 1 1 1])
    (local black [(/ 34 256) (/ 32 256) (/ 52 256 ) 1])
    (local blue [(/ 134 256) (/ 206 256) (/ 236 256) 1])
    (fn set-heaven [j]
        (local [r g b a] white)
        (for [i 0 (- w 1)]
             (: data :setPixel i j r g b a)))
    (fn set-hell [j]
        (local [r g b a] black)
        (for [i 0 (- w 1)]
             (: data :setPixel i j r g b a)))
    (fn world [j]
        (local [r g b a] blue)
        (for [i 0 (- w 1)]
             (: data :setPixel i j r g b a)))
    (fn gradient-colours [c1 c2 j h]
        (local percent  (sigmoid (/ j h) 0.5 10 1))
        (local r  (+ (. c1 1) (* (- (. c2 1) (. c1 1))  percent)))
        (local g  (+ (. c1 2) (* (- (. c2 2) (. c1 2))  percent)))
        (local b  (+ (. c1 3) (* (- (. c2 3) (. c1 3))  percent)))
        (local a  (+ (. c1 4) (* (- (. c2 4) (. c1 4)))  percent))
        [ r g b a ])
    (fn trans-heaven [j start end]
        (local [r g b a] (gradient-colours white blue (- j start) (- end start)))
        (for [i 0 (- w 1)]
             (: data :setPixel i j r g b a)))
    (fn trans-hell [j start end]
        (local [r g b a] (gradient-colours blue black (- j start) (- end start)))
        (for [i 0 (- w 1)]
             (: data :setPixel i j r g b a)))
    
    (for [j 0 (- h 1)]
         (if (< j 120) (set-heaven j)
             (< j 360) (trans-heaven j 120 360)
             (< j (- 1440 120)) (world j)
             (< j 1680) (trans-hell j (- 1440 120) 1680)
             (set-hell j)))
    
    (love.graphics.newImage data)
)

(local background-image (newBackground 900 1800))

(fn draw-sky[canvas-width canvas-height]
    (local map-width 900)
    (local map-height 1800)
    (local quad  (love.graphics.newQuad 0 0 map-width map-height map-width map-height))
    (love.graphics.draw background-image quad 0 0)
    )


(fn draw-sky-game [canvas-width canvas-height x y sx sy]
    (local map-width 900)
    (local map-height 1800)    
    (local quad  (love.graphics.newQuad x y canvas-width canvas-height map-width map-height))
    (love.graphics.draw background-image quad 0 0 0 sx sy)
    )



(fn erase-brush [mapin x y]
    (map.remove mapin x y :sun)
    (map.remove mapin x y :clouds)
    (map.remove mapin x y :ground)
    (map.remove mapin x y :objs)
    (map.remove mapin x y :for1)
    (map.remove mapin x y :for2))


(fn set-brush[brush]
    (fn []
        (tset pfbs :editor :brush brush)))

(local editor-keymap
       {:1 (set-brush make-road-season)           
           :2 (set-brush erase-brush)
           :3 (set-brush make-tree)
           :4 (set-brush make-big-tree)
           :5 (set-brush make-stone)
           :6 (set-brush make-eye)
           :7 (set-brush make-house1)
           :8 (set-brush make-house2)
           :9 (set-brush make-house3)
           :0 (set-brush make-house4)           
           "-" (set-brush make-cloud)
           "=" (set-brush make-sun)})
           

(fn pfbs.editor.keypressed [key set-mode]
    (when (= "escape" key)
      (if (= season "summer")
          (do (set season "winter"))
          (= season "winter")
          (do (set season "blood"))
          (and extended-seasons? (= season "blood"))
          (do (set season "text"))
          (= season "text")
          (do (set season "heaven"))
          (= season "heaven")
          (do (set season "hell"))
          (do (set season "summer"))))
      (when (= (type (. editor-keymap key)) "function")
        ((. editor-keymap key) key)))


(fn pfbs.player [x y world collidables attack]
    (local loader (require "loader"))
    (local attacks {:Overhead (attack.new
                               {:type :player-atk :color [0 1 1 1]
                                :start 200 :end 800 :w 12 :h 10
                                :list [{:time 200 :y -13 :x 0}
                                       {:time 400 :y -3 :x 6}]})
                    "Two Thrust" (attack.new
                                {:type :player-atk :color [0 1 1 1]
                                 :start 100 :end 400 :w 10 :h 10
                                 :list [{:time 200 :y -3 :x 4}
                                        {:time 300 :y -3 :x 8}]      })})
    (local hitbox (attack.new {:type :player :color [0.5 0 0.5 1]
                                     :w 4 :h 6 :ox 3 :oy -3}))
    (hitbox.begin hitbox)
    (local player {:update nil :draw nil
                           :state {:x x :y y :speed 60
                                      :flip true :anim-index (if state.scene.start-opening
                                                                 "Scream"
                                                                 "Sleep")
                                      :anim nil
                                      :v 0
                                      :health 6
                                      :hit false
                                      :alive true
                                      :hit-timer 0
                                      ;;:ox 12
                                      ;;:oy 16
                                      ;; need to make a collider specific offset
                                      :ox 30
                                      :oy 5
                                      :scale 5
                                      :w 32
                                      :grounded false
                                      :attacks attacks
                                      :collision {:w 8 :h 4 :ox -2 :oy 0}
                                      :hitbox hitbox
                                      :in-heaven false
                                      :entering-hell false
                                      :in-hell false
                                      :previous-x x
                                      :death-suspended false
                                      ;;{:w 300 :h 300 :ox 0 :oy 0}
                                      :explosion {:? false :time 0 :time-max 1900}
                                      
                                      }})
    
    (tset player.state :anim (loader "assets/twohandspear"))
    (local ctrl (require "ctrl"))    
    (ctrl.activate player world collidables)
    (fn draw-player [self x y]
        (when self.state.hit
          (love.graphics.setColor 1 0 0 0.5))
        (: (. self.state.anim.animations self.state.anim-index) :draw self.state.anim.image
           (math.floor x) (math.floor y) 0 self.state.scale self.state.scale)
        (love.graphics.setColor 1 1 1 1))
    
    (tset player :ctrl ctrl)
    (tset player :draw
          (fn [self camera]
              (var x1 0)
              (var y1 0)
              (if (and (not self.state.in-hell ) (not camera.opening.?))
                  (do (set x1 (+ (math.floor camera.off-x) player.state.ox))
                      (set y1 (+ (math.floor camera.off-y) player.state.oy)))
                  self.state.in-hell
                  (do (set x1 (math.floor (+ (* 5  player.state.x) -55  camera.x 5)))
                      (set y1 (math.floor (+ (* 5 player.state.y) -100  camera.y 5))))
                  camera.opening.?
                  (do (set x1 (math.floor (+ (* 5  player.state.x) -55  camera.x 5)))
                      (set y1 (math.floor (+ (* 5 player.state.y) -100  camera.y 5))))
                  )
              ;;(pp x1)
              ;; (when camera.opening.?
              ;;   (tset player.state :anim-index "Scream"))
                (draw-player self x1 y1)
              ))

    (tset player :update
          (fn [dt set-mode self world collidables]
              (set self.state (self.ctrl.update dt nil self.state state world collidables))
              (self.state.hitbox.hitbox-move self.state.hitbox self.state.x self.state.y self.state.flip)
              (tset  self.state.anim :animations self.state.anim-index :flippedH self.state.flip)

              (when (and state.player.state.green-stone
                         state.player.state.blood-stone
                         state.player.state.ice-stone)                
                (set state.npcs.robo-satan.invincible false))
              
              (when self.state.hit
                (tset self.state :hit-timer (+ self.state.hit-timer (* 1000 dt)))
                (when (> self.state.hit-timer 500)
                  (tset self.state :hit-timer 0)
                  (tset self.state :hit false)))
              (when (> state.player.state.explosion.time state.player.state.explosion.time-max)
                (tset state :frozen false)
                (tset state.player.state :death-suspended false)
                (set state.player.state.explosion.? true)
                (restart))
              (when (and (not self.state.death-suspended) (< self.state.health 1))
                   (restart))
              (: (. self.state.anim.animations self.state.anim-index) :update dt)
              self))
    
    player
    )

(tset pfbs :game {})
(tset pfbs :game :background-draw draw-sky-game)
(tset pfbs :editor :background-draw draw-sky)
(tset pfbs :editor :brush make-road-season)
(tset pfbs :tiles tiles)
(tset pfbs :sample-sprite-sheet sample-sprite-sheet)
pfbs
