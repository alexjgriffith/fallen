(local json (require "lib.json"))
(local anim8 (require "lib.anim8"))

(local json-list ["Angry Farmer""robo-satan-with-background" "Gabe" "robo-satan" "Knight" "twohandspear-to-transcibe" "NPC1"   "twohandspear" "godz"])

(fn read-file [file]
    (pp file)
    (let [f (assert (io.open file,"rb"))
          c (: f :read "*all")]
      (: f :close)
      c))

(fn write-file [file content]
    (let [f (assert (io.open file,"wb"))
          c (: f :write  (lume.serialize content))]
      (: f :close)
      c))



(fn json-to-lua[file]
    (local param (: json :decode (read-file (.. "assets/" file ".json"))))
    (write-file (.. "assets/" file ".lua") param)
    )

(fn[]
   (each [_ json (ipairs json-list)]
         (pp json)
         (json-to-lua json)))
