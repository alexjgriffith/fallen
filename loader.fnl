(local json (require "lib.json"))
(local anim8 (require "lib.anim8"))

(fn read-file [file]
    (let [f (assert (io.open file,"rb"))
          c (: f :read "*all")]
      (: f :close)
      c))

;; (fn read-file [name]
;;     (local file (love.filesystem.newFile name))
;;     (: file :open "r")
;;     (local data (: file :read))
;;     (: file :close)
;;     data)

;; (fn read-file [name]
;;     (love.filesystem.read name)
;;     )

(fn get-durations [param from to]
    (let [ret {}]
      (for [i from to]
           (tset ret (+ 1 (# ret))
                 (/ (. param.frames (+ 1 i) "duration") 1000)))
      ret ))

(fn loader [file]
    ;;(local param (: json :decode (read-file (.. file ".json"))))
    ;;(pp (.. file ".lua"))
    (local param (lume.deserialize (love.filesystem.read (.. file ".lua"))))
    ;;(local param (require file))
    ;; (local param (: json :decode (love.filesystem.read file)))
    (local image (love.graphics.newImage (.. "assets/" param.meta.image)))
    (local grid (anim8.newGrid 32 32 (: image :getWidth)(: image :getHeight)))
    (local animations {})
    (each [_ frame (ipairs param.meta.frameTags)]          
          (tset animations frame.name
                (anim8.newAnimation (grid (.. (+ 1 frame.from) "-" (+ 1 frame.to)) 1) (get-durations param frame.from frame.to))))
    {:animations animations :image image :grid grid :param param})


loader

