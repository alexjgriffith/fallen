love.conf = function(t)
   t.gammacorrect = true
   t.title, t.identity = "Fallen", "Fallen"
   t.modules.joystick = false
   t.modules.physics = false
   t.window.width = 900
   t.window.height = 600
   t.window.vsync = true
   t.version = "11.2"
end
