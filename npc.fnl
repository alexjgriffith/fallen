(local loader (require "loader"))
(local attack (require "attack"))

(local npc {})

(local default-npc
       {:name nil :x 0 :y 0 :speed 40 :anim nil :anim-index "Breath" :collision {:ox 0 :oy 0 :h 1 :w 1} :hitbox nil :hit false :hit-timer 0 :alive true :can-damage true :flip false :v 0 :scale 5 :grounded false :attacks nil :hitbox nil
              :ai nil
              :exists true
              :renderable true})

(fn npc.make [name params]
    (local new-npc {})
    (each [key value (pairs default-npc)]
          (tset new-npc key value))
    (each [key value (pairs params)]
          (var val value)
          (match key
                 "attacks" (do
                            (set val {})
                            (each [name param (pairs value)]
                                  (tset val name (attack.new param))))
                 "hitbox" (set val (attack.new value))
                 "anim" (set val (loader (.. "" value)))
                 "ai" (set val value)
                 )
          (tset new-npc key val))
    (when (not new-npc.name) (tset new-npc :name name))
    (when new-npc.hitbox
      (new-npc.hitbox.begin new-npc.hitbox))
    (when new-npc.collision
      (table.insert state.game.collidables new-npc)
      (: state.game.world :add new-npc  (-  new-npc.x  new-npc.collision.ox)  (- new-npc.y  new-npc.collision.oy) new-npc.collision.w new-npc.collision.h))
    new-npc)

(fn npc.draw [npcin]
    (when  npcin.renderable
      (when npcin.hit
        (love.graphics.setColor 1 0 0 0.5))    
    (: (. npcin.anim.animations npcin.anim-index) :draw npcin.anim.image
       (math.floor (+ state.camera.x (* 5 npcin.x) npcin.ox)) (math.floor (+ state.camera.y  (* 5 npcin.y) npcin.oy)) 0 npcin.scale npcin.scale)
    (love.graphics.setColor 1 1 1 1))
    )

(fn npc.update [npcin dt]
    ;; have ai move npc
    (when npcin.ai
      ((require npcin.ai) dt))
    (when npcin.exists
      (local goal-y (+ npcin.y  npcin.v ) )
      (local [aX aY col len] [(: state.game.world :move npcin   npcin.x  goal-y)])
      (tset npcin :x aX)
      (tset npcin :y aY))
    (when npcin.hitbox
      (npcin.hitbox.hitbox-move npcin.hitbox npcin.x npcin.y npcin.flip))
    (when npcin.anim
      (tset  npcin.anim :animations npcin.anim-index :flippedH npcin.flip)
      (: (. npcin.anim.animations npcin.anim-index) :update dt))
    (when npcin.hit
      (tset npcin :hit-timer (+ npcin.hit-timer (* 1000 dt)))
      (when (> npcin.hit-timer 500)
        (tset npcin :hit-timer 0)
        (tset npcin :hit false)))
 
    )
    
npc
