(local loader (require "loader"))
(local state (require "state"))

(local ctrl (require "ctrl"))

(global g_ctrl ctrl)

(fn activate []
    (ctrl.activate)
    (tset state :anim (loader "twohandspear.json")))

(fn draw []
    (tset (. state.anim.animations state.char.anim) :flippedH state.char.flip)
    (: (. state.anim.animations state.char.anim) :draw state.anim.image
       state.char.x state.char.y 0 5 5))

(fn update [dt set-mode]
    (ctrl.update dt)
    ;;(set state.char (character-controler state.char dt))
    (: (. state.anim.animations state.char.anim) :update dt))

{:activate activate
 :draw draw
 :update update
 :keypressed (fn [key set-mode]
                 (when (= key "escape")
                   (love.event.quit))
                 (ctrl.keypressed key)
                 )
  :keyreleased (fn [key]
                 (ctrl.keyreleased key))}
