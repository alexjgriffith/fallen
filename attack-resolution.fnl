(fn ctrl-atk [which action args?]
    (let [atk (. state.attacks which)]
      (if args?
          ((. atk action) atk  (unpack args?))
          (do ((. atk action) atk )))))

(var first true)

(fn [atk cols len]
    (each [_ col (ipairs cols)]
          ;; When Player hits [NPC]
          (when (and (= col.item.type "player-atk")
                     (= col.other.type "npc")
                     (not col.item.has-hit))
            (local npc (. state :npcs col.other.npc))
            (when npc.alive             
             (tset npc :hostile true)
             (when (not npc.invincible)
               (tset npc :hit true)
               (tset npc :health (- npc.health 1)))
             (when (< npc.health 1)
               (tset npc :alive false))
             (set col.item.has-hit true)))
          ;; When [NPC] hits player
          (when (and (= col.item.type "npc-atk")
                     (= col.other.type "player")
                     (not col.item.has-hit)
                     state.player.state.alive)           
            (tset state.player.state :hit true)
            (tset state.player.state :health (- state.player.state.health 1))
            (set col.item.has-hit true)
            )
          (when (and (= col.item.type "player")
                     (= col.other.type "green-stone"))
            (tset state.player.state :green-stone true)
            (set state.stones.green false)
            )
          (when (and (= col.item.type "player")
                     (= col.other.type "ice-stone"))
            (set state.stones.ice false)
            (tset state.player.state :ice-stone true)
            )

          (when (and (= col.item.type "player")
                     (= col.other.type "blood-stone"))
            (tset state.player.state :blood-stone true)
            (set state.stones.blood false)
            )
          (when (and (= col.item.type "player")
                     (= col.other.type "potion"))
            (set state.stones.potion false)
            (set state.player.state.health 7)
            )

    ))
