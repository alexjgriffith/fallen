(local action (require "action"))
(local action-obj (action.new))
(global g_act action-obj)

(var state nil)
(var goal-x nil)
(var goal-y nil)
;;(var frozen false)

(fn reset-anim [name]
    (let [animation (. state.anim.animations name)]
      (: animation :gotoFrame 1)))

(fn breath []
    (reset-anim "Breath")
    (set state.anim-index "Breath"))

(fn yawn []    
    (when (= state.anim-index "Breath")
      (reset-anim "Yawn")
      (tset (. state.anim.animations "Yawn") :onLoop breath)
      (set state.anim-index "Yawn")))


(fn anim-stop [current default]
    (if (= current state.anim-index)
        (default)
        (breath)))

(fn walk-left [this]
    (when (action-obj.can-act? action-obj)
      (action-obj.unset action-obj)
        (reset-anim "Walk")
        (set state.anim-index "Walk"))
    (when (action-obj.can-flip? action-obj)
      (set state.flip true))
    (set this.state true))

(fn walk-right [this]
    (when (action-obj.can-act? action-obj)
      (action-obj.unset action-obj)
      (reset-anim "Walk")
        (set state.anim-index "Walk"))
    (when (action-obj.can-flip? action-obj)
      (set state.flip false))    
    (set this.state true))

(fn walk-left-update [dt]
    (when (action-obj.can-act? action-obj)
      (action-obj.unset action-obj)
      (set state.anim-index "Walk"))
    (when (action-obj.can-flip? action-obj)
      (set state.flip true))
    (when (action-obj.can-move? action-obj)
      (set goal-x (- state.x (* dt state.speed)))))

(fn walk-right-update [dt]
    (when (action-obj.can-act? action-obj)
      (action-obj.unset action-obj)
      (set state.anim-index "Walk"))
    (when (action-obj.can-flip? action-obj)
      (set state.flip false))
    (when (action-obj.can-move? action-obj)
      (set goal-x (+ state.x (* dt state.speed)))))
    
(fn stop [this]
    (set this.state false)
    (when (action-obj.can-act? action-obj)
      (action-obj.unset action-obj)
      (anim-stop "Walk" breath)))


(fn non-release [_])

(fn non-update [_])

;; Jump
(var jumps 0)
(var max-jumps 2)
(fn jump-callback [dt]
    (anim-stop "Jump" breath))

(fn jump-update [action-obj _ _ [state _ _]]
    (when (and state.grounded (~= action-obj.timer 0))
      (set action-obj.active false)))

(local jump-action {:name "Jump" :callback jump-callback :update-callback jump-update :default nil :early 0 :lead 0 :post 0 :flip-lock false})



(fn jump-fn [_ falling]
    (if  falling
        (do (reset-anim "Jump")
            ;;(action-obj.set action-obj jump-action)
          (set state.anim-index "Jump"))
    (when (and (< jumps max-jumps) (action-obj.can-act-early? action-obj))
      (set jumps (+ jumps 1))
      (set state.v  -4)
      (reset-anim "Jump")
      (action-obj.set action-obj jump-action)
      (set state.anim-index "Jump"))))

;; Attacks


(fn ctrl-atk [which action args?]
    (let [atk (. state.attacks which)]
      (if args?
          ((. atk action) atk  (unpack args?))
          (do ((. atk action) atk )))))

(fn overhead-update [action-obj dt _ [_ _ _]]
    (ctrl-atk "Overhead" :update [state.x state.y state.flip dt]))

(fn overhead-callback [dt]
    (ctrl-atk "Overhead" :finish)
    (anim-stop "Overhead" breath))

(fn twothrust-update [action-obj dt _ [_ _ _]]
    (ctrl-atk "Two Thrust" :update [state.x state.y state.flip dt]))

(fn twothrust-callback [dt]
    (ctrl-atk "Two Thrust" :finish)
    (anim-stop "Two Thrust" breath))

(local overhead {:name "Overhead" :callback overhead-callback :update-callback overhead-update :default 1.0 :early 0.4 :lead 0.6 :post 0.8 :flip-lock false})

(local twothrust {:name "Two Thrust" :callback twothrust-callback :update-callback twothrust-update :default 0.8 :early 0.8 :lead 0.0 :post 0.2 :flip-lock true})


(fn overhead-fn [_]
    (when (action-obj.can-act? action-obj)
       (action-obj.unset action-obj)
      (if (= "Walk" state.anim-index)
          (do (reset-anim "Overhead")
              (set state.anim-index "Overhead")
            (action-obj.set action-obj overhead)
            (ctrl-atk "Overhead" :begin [state.x state.y]))
          (do (reset-anim "Two Thrust")
              (set state.anim-index "Two Thrust")
            (ctrl-atk "Two Thrust" :begin [state.x state.y])
          (action-obj.set action-obj twothrust)))))


(local keymap {"a" {:pressed walk-left
                      :released stop
                      :state false
                      :update walk-left-update}
               "d" {:pressed walk-right
                    :released stop
                    :state false
                    :update walk-right-update}
                "w" {:pressed jump-fn
                    :released non-release
                    :state false
                    :update non-update}
                "space" {:pressed overhead-fn
                    :released non-release
                    :state false
                    :update non-update}                
               })

(fn falling-to-hell? [player world]
    (when (and (>= player.y 1620) (not player.entering-hell))
      (tset player :entering-hell true)
      (tset player :previous-x player.x)
      (: world :update player (* 2 120) player.y)
      (tset player :x (* 2 120))
      (set goal-x (* 2.4 180))
))


(fn falling-bellow-hell? [player world]
    (when
        (> player.y 1900)
      (: world :update player (* 2 120) 1620)
      (tset player :x (* 2 120))
      (tset player :y 1620)
      (set goal-y 1620)
      (set goal-x (* 2.4 180)
            
      )))

(fn in-hell? [player world]
    (when (and (>= player.y 1690))
      (tset player :in-hell true)
))

(fn rising-from-hell? [player]
    (when (and (< player.y 1590 ) player.in-hell)
      (tset player :x player.previous-x)))

(fn rising-to-heaven? [player]
    (when (and (< player.y (* 120 1) ) (not player.in-heaven))
      (tset player :in-heaven true)
      (tset player :previous-x player.x)
    (tset player :x (* 2.5 120))))

(fn falling-from-heaven? [player]
    (when (and (> player.y (* 120 1) ) player.in-heaven)
      (tset player :x player.previous-x)
      true))

(fn shift-in-x [player world]
    (falling-to-hell? player world)
    (in-hell? player world)
    (falling-bellow-hell? player world)
    ;;(rising-from-hell? player world)
;;    (rising-to-heaven? player world)
    ;;  (falling-from-heaven? player world)
    )

(fn update [dt set-mode player real-state world collidables]
    (set goal-x (or real-state.player.state.x 0))
    (set goal-y (or real-state.player.state.y 0))
    (when (not real-state.frozen)
      (lume.map keymap (fn[x] (when x.state
                                (x.update dt))))
      )
    (if ( and state.grounded (>= state.v -0.1))
        (set state.v 0))
    (when (> state.v 2)
      (jump-fn nil true))
    (action-obj.update action-obj dt [state world collidables])
    (set goal-y (+ state.y state.v ))
    (not (shift-in-x player world))
    (local [aX aY col len] [(: world :move state  (- goal-x state.collision.ox) goal-y)])
    (var ground false)
    (each [_ value (ipairs col)]
          ;; check if the tile is ground and if the normal is from above         
          (when ;;( and
              ;;(= (. value :other :l)
                     ;;"ground")
                    (= (. value :normal :y) -1)
                    ;(> state.v 0.0)
                ;;    )
            (set ground true)
              ))
    (set state.grounded ground)
    (when state.grounded
      (set jumps 0))
    (set state.v (-  aY state.y))
    (set state.y (math.floor aY))
    (set state.x  (+ aX state.collision.ox))
    (when (< (math.random 0 10000) 2)
      (yawn))    
    state)

(fn keypressed [key]
    (let [f (. keymap key)]
      (when (= (type f) "table")
        (when (= (type f.pressed) "function")          
          (f.pressed f)))))

(fn keyreleased [key]
    (let [f (. keymap key)]
      (when (= (type f) "table")
        (when (= (type f.released) "function") 
          (f.released f)))))



(global ctrl_keys keymap)


;; (global stop_move (fn []
;;                       (set goal-x 0)
;;                       (set goal-y 0)
;;                       (each [key _ (ipairs ctrl_keys)]
;;                             (pp (.. "stoping" key))
;;                             (tset ctrl_keys key :state false))))

(fn activate [player world collidables]    
    (set state player.state)
    (table.insert collidables state)
    (: world :add state  (- state.x state.collision.ox)  (- state.y state.collision.oy) state.collision.w state.collision.h))

{:activate activate :keypressed keypressed :keyreleased keyreleased :update update :restart keymap }
