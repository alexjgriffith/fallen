{
  :width 57
  :data {
    :for1 {
      1110 {
        :x 27
        :id 0
        :y 19
        :index [ 5 11 7 13 ]
        :w 3
        :type "eye-ice"
        :h 3
        :l "for1"
        :library "tile"
      }
      4061 {
        :x 14
        :id 0
        :y 71
        :index [ 5 11 7 13 ]
        :w 3
        :type "eye-green"
        :h 3
        :l "for1"
        :library "tile"
      }
      3730 {
        :x 25
        :id 0
        :y 65
        :index [ 5 11 7 13 ]
        :w 3
        :type "eye-blood"
        :h 3
        :l "for1"
        :library "tile"
      }
      2371 {
        :x 34
        :id 0
        :y 41
        :index [ 5 11 7 13 ]
        :w 3
        :type "eye-green"
        :h 3
        :l "for1"
        :library "tile"
      }
      1729 {
        :x 19
        :id 0
        :y 30
        :index [ 5 11 7 13 ]
        :w 3
        :type "eye-ice"
        :h 3
        :l "for1"
        :library "tile"
      }
      3738 {
        :x 33
        :id 0
        :y 65
        :index [ 5 11 7 13 ]
        :w 3
        :type "eye-blood"
        :h 3
        :l "for1"
        :library "tile"
      }
      3055 {
        :x 34
        :id 0
        :y 53
        :index [ 5 11 7 13 ]
        :w 3
        :type "eye-green"
        :h 3
        :l "for1"
        :library "tile"
      }
    }
    :for2 {}
    :sun {
      59 {
        :y 1
        :x 2
        :w 10
        :id 0
        :type "sun"
        :h 8
        :l "sun"
        :library "tile"
      }
    }
    :objs {
      3125 {
        :y 54
        :x 47
        :w 2
        :id 0
        :type "stone-green"
        :h 4
        :l "objs"
        :library "tile"
      }
      2021 {
        :y 35
        :x 26
        :w 2
        :id 0
        :type "tree-green"
        :h 4
        :l "objs"
        :library "tile"
      }
      1220 {
        :y 21
        :x 23
        :w 2
        :id 0
        :type "tree-ice"
        :h 4
        :l "objs"
        :library "tile"
      }
      2824 {
        :y 49
        :x 31
        :w 5
        :id 0
        :type "house4"
        :h 6
        :l "objs"
        :library "tile"
      }
      4247 {
        :y 74
        :x 29
        :w 2
        :id 0
        :type "stone-blood"
        :h 4
        :l "objs"
        :library "tile"
      }
      3825 {
        :y 67
        :x 6
        :w 4
        :id 0
        :type "big-tree-blood"
        :h 6
        :l "objs"
        :library "tile"
      }
      878 {
        :y 15
        :x 23
        :w 8
        :id 0
        :type "house1"
        :h 6
        :l "objs"
        :library "tile"
      }
      2529 {
        :y 44
        :x 21
        :w 2
        :id 0
        :type "tree-green"
        :h 4
        :l "objs"
        :library "tile"
      }
      1436 {
        :y 25
        :x 11
        :w 2
        :id 0
        :type "tree-ice"
        :h 4
        :l "objs"
        :library "tile"
      }
      2139 {
        :y 37
        :x 30
        :w 4
        :id 0
        :type "big-tree-green"
        :h 6
        :l "objs"
        :library "tile"
      }
      884 {
        :y 15
        :x 29
        :w 4
        :id 0
        :type "big-tree-ice"
        :h 6
        :l "objs"
        :library "tile"
      }
      3830 {
        :y 67
        :x 11
        :w 6
        :id 0
        :type "house3"
        :h 6
        :l "objs"
        :library "tile"
      }
      1315 {
        :y 23
        :x 4
        :w 2
        :id 0
        :type "stone-ice"
        :h 4
        :l "objs"
        :library "tile"
      }
      3555 {
        :y 62
        :x 21
        :w 2
        :id 0
        :type "tree-blood"
        :h 4
        :l "objs"
        :library "tile"
      }
      3510 {
        :y 61
        :x 33
        :w 4
        :id 0
        :type "big-tree-blood"
        :h 6
        :l "objs"
        :library "tile"
      }
      2829 {
        :y 49
        :x 36
        :w 4
        :id 0
        :type "big-tree-green"
        :h 6
        :l "objs"
        :library "tile"
      }
      1503 {
        :y 26
        :x 21
        :w 6
        :id 0
        :type "house2"
        :h 6
        :l "objs"
        :library "tile"
      }
      1499 {
        :y 26
        :x 17
        :w 4
        :id 0
        :type "big-tree-ice"
        :h 6
        :l "objs"
        :library "tile"
      }
      3346 {
        :y 58
        :x 40
        :w 2
        :id 0
        :type "tree-blood"
        :h 4
        :l "objs"
        :library "tile"
      }
    }
    :clouds {
      2341 {
        :y 41
        :x 4
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1389 {
        :y 24
        :x 21
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2060 {
        :y 36
        :x 8
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      800 {
        :y 14
        :x 2
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2084 {
        :y 36
        :x 32
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1062 {
        :y 18
        :x 36
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1070 {
        :y 18
        :x 44
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1540 {
        :y 27
        :x 1
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1341 {
        :y 23
        :x 30
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1030 {
        :y 18
        :x 4
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      503 {
        :y 8
        :x 47
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      780 {
        :y 13
        :x 39
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1313 {
        :y 23
        :x 2
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      3450 {
        :y 60
        :x 30
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      3153 {
        :y 55
        :x 18
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      3607 {
        :y 63
        :x 16
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2237 {
        :y 39
        :x 14
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      517 {
        :y 9
        :x 4
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      123 {
        :y 2
        :x 9
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1267 {
        :y 22
        :x 13
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      386 {
        :y 6
        :x 44
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2077 {
        :y 36
        :x 25
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      4139 {
        :y 72
        :x 35
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      195 {
        :y 3
        :x 24
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2707 {
        :y 47
        :x 28
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2715 {
        :y 47
        :x 36
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      205 {
        :y 3
        :x 34
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      202 {
        :y 3
        :x 31
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      697 {
        :y 12
        :x 13
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      3616 {
        :y 63
        :x 25
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2327 {
        :y 40
        :x 47
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      423 {
        :y 7
        :x 24
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1904 {
        :y 33
        :x 23
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2812 {
        :y 49
        :x 19
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1378 {
        :y 24
        :x 10
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      215 {
        :y 3
        :x 44
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      3977 {
        :y 69
        :x 44
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2110 {
        :y 37
        :x 1
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2487 {
        :y 43
        :x 36
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      926 {
        :y 16
        :x 14
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      433 {
        :y 7
        :x 34
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      3491 {
        :y 61
        :x 14
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      438 {
        :y 7
        :x 39
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2009 {
        :y 35
        :x 14
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1487 {
        :y 26
        :x 5
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      4271 {
        :y 74
        :x 53
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1786 {
        :y 31
        :x 19
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      3062 {
        :y 53
        :x 41
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      3572 {
        :y 62
        :x 38
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1666 {
        :y 29
        :x 13
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1172 {
        :y 20
        :x 32
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      3713 {
        :y 65
        :x 8
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1164 {
        :y 20
        :x 24
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2472 {
        :y 43
        :x 21
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      350 {
        :y 6
        :x 8
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      580 {
        :y 10
        :x 10
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      346 {
        :y 6
        :x 4
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1359 {
        :y 23
        :x 48
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1144 {
        :y 20
        :x 4
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2264 {
        :y 39
        :x 41
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1658 {
        :y 29
        :x 5
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      168 {
        :y 2
        :x 54
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2566 {
        :y 45
        :x 1
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2521 {
        :y 44
        :x 13
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      793 {
        :y 13
        :x 52
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2959 {
        :y 51
        :x 52
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      372 {
        :y 6
        :x 30
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1578 {
        :y 27
        :x 39
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      773 {
        :y 13
        :x 32
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1009 {
        :y 17
        :x 40
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1019 {
        :y 17
        :x 50
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      3085 {
        :y 54
        :x 7
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2038 {
        :y 35
        :x 43
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      526 {
        :y 9
        :x 13
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      3972 {
        :y 69
        :x 39
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1946 {
        :y 34
        :x 8
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2833 {
        :y 49
        :x 40
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      141 {
        :y 2
        :x 27
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      690 {
        :y 12
        :x 6
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2458 {
        :y 43
        :x 7
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      930 {
        :y 16
        :x 18
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      922 {
        :y 16
        :x 10
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      888 {
        :y 15
        :x 33
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      841 {
        :y 14
        :x 43
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1193 {
        :y 20
        :x 53
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      3708 {
        :y 65
        :x 3
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1858 {
        :y 32
        :x 34
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1627 {
        :y 28
        :x 31
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      826 {
        :y 14
        :x 28
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      806 {
        :y 14
        :x 8
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2857 {
        :y 50
        :x 7
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      765 {
        :y 13
        :x 24
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1615 {
        :y 28
        :x 19
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1348 {
        :y 23
        :x 37
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      758 {
        :y 13
        :x 17
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      71 {
        :y 1
        :x 14
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      933 {
        :y 16
        :x 21
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1918 {
        :y 33
        :x 37
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      681 {
        :y 11
        :x 54
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1105 {
        :y 19
        :x 22
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      613 {
        :y 10
        :x 43
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      602 {
        :y 10
        :x 32
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      937 {
        :y 16
        :x 25
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      3286 {
        :y 57
        :x 37
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      302 {
        :y 5
        :x 17
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      593 {
        :y 10
        :x 23
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      3479 {
        :y 61
        :x 2
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1547 {
        :y 27
        :x 8
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      298 {
        :y 5
        :x 13
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1838 {
        :y 32
        :x 14
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1336 {
        :y 23
        :x 25
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2129 {
        :y 37
        :x 20
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      4707 {
        :y 82
        :x 33
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      4262 {
        :y 74
        :x 44
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1452 {
        :y 25
        :x 27
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2681 {
        :y 47
        :x 2
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      306 {
        :y 5
        :x 21
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      4131 {
        :y 72
        :x 27
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      4127 {
        :y 72
        :x 23
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      3963 {
        :y 69
        :x 30
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      394 {
        :y 6
        :x 52
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1238 {
        :y 21
        :x 41
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      3746 {
        :y 65
        :x 41
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2840 {
        :y 49
        :x 47
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      3540 {
        :y 62
        :x 6
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      3303 {
        :y 57
        :x 54
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1768 {
        :y 31
        :x 1
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2275 {
        :y 39
        :x 52
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      153 {
        :y 2
        :x 39
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1270 {
        :y 22
        :x 16
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2047 {
        :y 35
        :x 52
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1505 {
        :y 26
        :x 23
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1776 {
        :y 31
        :x 9
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      3106 {
        :y 54
        :x 28
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      3072 {
        :y 53
        :x 51
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2749 {
        :y 48
        :x 13
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      3733 {
        :y 65
        :x 28
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2099 {
        :y 36
        :x 47
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      59 {
        :y 1
        :x 2
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2978 {
        :y 52
        :x 14
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2504 {
        :y 43
        :x 53
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2493 {
        :y 43
        :x 42
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1154 {
        :y 20
        :x 14
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2426 {
        :y 42
        :x 32
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1469 {
        :y 25
        :x 44
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2361 {
        :y 41
        :x 24
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      119 {
        :y 2
        :x 5
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1441 {
        :y 25
        :x 16
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      589 {
        :y 10
        :x 19
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2196 {
        :y 38
        :x 30
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      2113 {
        :y 37
        :x 4
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1877 {
        :y 32
        :x 53
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1814 {
        :y 31
        :x 47
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1591 {
        :y 27
        :x 52
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      4152 {
        :y 72
        :x 48
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
    }
    :ground {
      2819 {
        :x 26
        :y 49
        :id 0
        :type "road-green"
        :index [ 5 8 7 10 ]
        :l "ground"
        :library "subtile"
      }
      2309 {
        :x 29
        :y 40
        :id 0
        :type "road-green"
        :index [ 8 8 3 9 ]
        :l "ground"
        :library "subtile"
      }
      1389 {
        :x 21
        :y 24
        :id 0
        :type "road-ice"
        :index [ 9 9 3 1 ]
        :l "ground"
        :library "subtile"
      }
      1110 {
        :x 27
        :y 19
        :id 0
        :type "road-ice"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1552 {
        :x 13
        :y 27
        :id 0
        :type "road-ice"
        :index [ 8 11 10 13 ]
        :l "ground"
        :library "subtile"
      }
      3240 {
        :x 48
        :y 56
        :id 0
        :type "road-green"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1544 {
        :x 5
        :y 27
        :id 0
        :type "road-ice"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      2485 {
        :x 34
        :y 43
        :id 0
        :type "road-green"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      2995 {
        :x 31
        :y 52
        :id 0
        :type "road-green"
        :index [ 8 8 3 9 ]
        :l "ground"
        :library "subtile"
      }
      4472 {
        :x 26
        :y 78
        :id 0
        :type "road-blood"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      6242 {
        :x 29
        :y 109
        :id 0
        :type "road-hell"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      3671 {
        :x 23
        :y 64
        :id 0
        :type "road-blood"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1333 {
        :x 22
        :y 23
        :id 0
        :type "road-ice"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4282 {
        :x 7
        :y 75
        :id 0
        :type "road-blood"
        :index [ 9 9 9 1 ]
        :l "ground"
        :library "subtile"
      }
      4569 {
        :x 9
        :y 80
        :id 0
        :type "road-blood"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      1054 {
        :x 28
        :y 18
        :id 0
        :type "road-ice"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1729 {
        :x 19
        :y 30
        :id 0
        :type "road-ice"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3354 {
        :x 48
        :y 58
        :id 0
        :type "road-green"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      2374 {
        :x 37
        :y 41
        :id 0
        :type "road-green"
        :index [ 9 9 10 10 ]
        :l "ground"
        :library "subtile"
      }
      1486 {
        :x 4
        :y 26
        :id 0
        :type "road-ice"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      4473 {
        :x 27
        :y 78
        :id 0
        :type "road-blood"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      2820 {
        :x 27
        :y 49
        :id 0
        :type "road-green"
        :index [ 8 8 3 9 ]
        :l "ground"
        :library "subtile"
      }
      1510 {
        :x 28
        :y 26
        :id 0
        :type "road-ice"
        :index [ 8 11 10 13 ]
        :l "ground"
        :library "subtile"
      }
      4570 {
        :x 10
        :y 80
        :id 0
        :type "road-blood"
        :index [ 9 2 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1430 {
        :x 5
        :y 25
        :id 0
        :type "road-ice"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1175 {
        :x 35
        :y 20
        :id 0
        :type "road-ice"
        :index [ 8 11 10 13 ]
        :l "ground"
        :library "subtile"
      }
      3241 {
        :x 49
        :y 56
        :id 0
        :type "road-green"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1167 {
        :x 27
        :y 20
        :id 0
        :type "road-ice"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      2486 {
        :x 35
        :y 43
        :id 0
        :type "road-green"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      2996 {
        :x 32
        :y 52
        :id 0
        :type "road-green"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4474 {
        :x 28
        :y 78
        :id 0
        :type "road-blood"
        :index [ 9 9 9 1 ]
        :l "ground"
        :library "subtile"
      }
      6244 {
        :x 31
        :y 109
        :id 0
        :type "road-hell"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      3225 {
        :x 33
        :y 56
        :id 0
        :type "road-green"
        :index [ 6 9 6 1 ]
        :l "ground"
        :library "subtile"
      }
      1968 {
        :x 30
        :y 34
        :id 0
        :type "road-ice"
        :index [ 6 12 7 13 ]
        :l "ground"
        :library "subtile"
      }
      1446 {
        :x 21
        :y 25
        :id 0
        :type "road-ice"
        :index [ 6 12 7 13 ]
        :l "ground"
        :library "subtile"
      }
      4571 {
        :x 11
        :y 80
        :id 0
        :type "road-blood"
        :index [ 8 8 9 1 ]
        :l "ground"
        :library "subtile"
      }
      2644 {
        :x 22
        :y 46
        :id 0
        :type "road-green"
        :index [ 8 8 3 9 ]
        :l "ground"
        :library "subtile"
      }
      3355 {
        :x 49
        :y 58
        :id 0
        :type "road-green"
        :index [ 9 9 9 1 ]
        :l "ground"
        :library "subtile"
      }
      2375 {
        :x 38
        :y 41
        :id 0
        :type "road-green"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      4475 {
        :x 29
        :y 78
        :id 0
        :type "road-blood"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      2821 {
        :x 28
        :y 49
        :id 0
        :type "road-green"
        :index [ 8 8 9 1 ]
        :l "ground"
        :library "subtile"
      }
      1912 {
        :x 31
        :y 33
        :id 0
        :type "road-ice"
        :index [ 8 11 10 13 ]
        :l "ground"
        :library "subtile"
      }
      2311 {
        :x 31
        :y 40
        :id 0
        :type "road-green"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1390 {
        :x 22
        :y 24
        :id 0
        :type "road-ice"
        :index [ 9 9 10 10 ]
        :l "ground"
        :library "subtile"
      }
      1111 {
        :x 28
        :y 19
        :id 0
        :type "road-ice"
        :index [ 9 9 9 1 ]
        :l "ground"
        :library "subtile"
      }
      3736 {
        :x 31
        :y 65
        :id 0
        :type "road-blood"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      3242 {
        :x 50
        :y 56
        :id 0
        :type "road-green"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3467 {
        :x 47
        :y 60
        :id 0
        :type "road-green"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      2487 {
        :x 36
        :y 43
        :id 0
        :type "road-green"
        :index [ 9 12 9 12 ]
        :l "ground"
        :library "subtile"
      }
      2134 {
        :x 25
        :y 37
        :id 0
        :type "road-green"
        :index [ 5 8 7 10 ]
        :l "ground"
        :library "subtile"
      }
      3226 {
        :x 34
        :y 56
        :id 0
        :type "road-green"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      1334 {
        :x 23
        :y 23
        :id 0
        :type "road-ice"
        :index [ 8 8 9 1 ]
        :l "ground"
        :library "subtile"
      }
      2934 {
        :x 27
        :y 51
        :id 0
        :type "road-green"
        :index [ 6 12 7 13 ]
        :l "ground"
        :library "subtile"
      }
      1055 {
        :x 29
        :y 18
        :id 0
        :type "road-ice"
        :index [ 8 8 9 1 ]
        :l "ground"
        :library "subtile"
      }
      1730 {
        :x 20
        :y 30
        :id 0
        :type "road-ice"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3356 {
        :x 50
        :y 58
        :id 0
        :type "road-green"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      4413 {
        :x 24
        :y 77
        :id 0
        :type "road-blood"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      1224 {
        :x 27
        :y 21
        :id 0
        :type "road-ice"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      2822 {
        :x 29
        :y 49
        :id 0
        :type "road-green"
        :index [ 8 11 10 13 ]
        :l "ground"
        :library "subtile"
      }
      4510 {
        :x 7
        :y 79
        :id 0
        :type "road-blood"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      2312 {
        :x 32
        :y 40
        :id 0
        :type "road-green"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1786 {
        :x 19
        :y 31
        :id 0
        :type "road-ice"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3785 {
        :x 23
        :y 66
        :id 0
        :type "road-blood"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      1431 {
        :x 6
        :y 25
        :id 0
        :type "road-ice"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3291 {
        :x 42
        :y 57
        :id 0
        :type "road-green"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      3737 {
        :x 32
        :y 65
        :id 0
        :type "road-blood"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      1168 {
        :x 28
        :y 20
        :id 0
        :type "road-ice"
        :index [ 9 12 9 12 ]
        :l "ground"
        :library "subtile"
      }
      3468 {
        :x 48
        :y 60
        :id 0
        :type "road-green"
        :index [ 9 12 3 12 ]
        :l "ground"
        :library "subtile"
      }
      2135 {
        :x 26
        :y 37
        :id 0
        :type "road-green"
        :index [ 8 8 3 1 ]
        :l "ground"
        :library "subtile"
      }
      4223 {
        :x 5
        :y 74
        :id 0
        :type "road-blood"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      4224 {
        :x 6
        :y 74
        :id 0
        :type "road-blood"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      4511 {
        :x 8
        :y 79
        :id 0
        :type "road-blood"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      3674 {
        :x 26
        :y 64
        :id 0
        :type "road-blood"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4288 {
        :x 13
        :y 75
        :id 0
        :type "road-blood"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      2646 {
        :x 24
        :y 46
        :id 0
        :type "road-green"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3851 {
        :x 32
        :y 67
        :id 0
        :type "road-blood"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      4415 {
        :x 26
        :y 77
        :id 0
        :type "road-blood"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      4225 {
        :x 7
        :y 74
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      2313 {
        :x 33
        :y 40
        :id 0
        :type "road-green"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4059 {
        :x 12
        :y 71
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1391 {
        :x 23
        :y 24
        :id 0
        :type "road-ice"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      1112 {
        :x 29
        :y 19
        :id 0
        :type "road-ice"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      1056 {
        :x 30
        :y 18
        :id 0
        :type "road-ice"
        :index [ 8 11 10 13 ]
        :l "ground"
        :library "subtile"
      }
      2758 {
        :x 22
        :y 48
        :id 0
        :type "road-green"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      3738 {
        :x 33
        :y 65
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3244 {
        :x 52
        :y 56
        :id 0
        :type "road-green"
        :index [ 8 11 10 13 ]
        :l "ground"
        :library "subtile"
      }
      4416 {
        :x 27
        :y 77
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1287 {
        :x 33
        :y 22
        :id 0
        :type "road-ice"
        :index [ 6 12 7 13 ]
        :l "ground"
        :library "subtile"
      }
      2136 {
        :x 27
        :y 37
        :id 0
        :type "road-green"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      4226 {
        :x 8
        :y 74
        :id 0
        :type "road-blood"
        :index [ 9 9 9 1 ]
        :l "ground"
        :library "subtile"
      }
      1335 {
        :x 24
        :y 23
        :id 0
        :type "road-ice"
        :index [ 8 11 10 13 ]
        :l "ground"
        :library "subtile"
      }
      2426 {
        :x 32
        :y 42
        :id 0
        :type "road-green"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      4644 {
        :x 27
        :y 81
        :id 0
        :type "road-blood"
        :index [ 6 12 7 13 ]
        :l "ground"
        :library "subtile"
      }
      1048 {
        :x 22
        :y 18
        :id 0
        :type "road-ice"
        :index [ 5 8 7 10 ]
        :l "ground"
        :library "subtile"
      }
      2647 {
        :x 25
        :y 46
        :id 0
        :type "road-green"
        :index [ 8 8 9 1 ]
        :l "ground"
        :library "subtile"
      }
      1488 {
        :x 6
        :y 26
        :id 0
        :type "road-ice"
        :index [ 9 9 10 10 ]
        :l "ground"
        :library "subtile"
      }
      1731 {
        :x 21
        :y 30
        :id 0
        :type "road-ice"
        :index [ 9 9 9 1 ]
        :l "ground"
        :library "subtile"
      }
      4417 {
        :x 28
        :y 77
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1225 {
        :x 28
        :y 21
        :id 0
        :type "road-ice"
        :index [ 9 12 3 12 ]
        :l "ground"
        :library "subtile"
      }
      4740 {
        :x 9
        :y 83
        :id 0
        :type "road-blood"
        :index [ 6 12 6 12 ]
        :l "ground"
        :library "subtile"
      }
      4227 {
        :x 9
        :y 74
        :id 0
        :type "road-blood"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      2314 {
        :x 34
        :y 40
        :id 0
        :type "road-green"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1787 {
        :x 20
        :y 31
        :id 0
        :type "road-ice"
        :index [ 9 9 9 1 ]
        :l "ground"
        :library "subtile"
      }
      3787 {
        :x 25
        :y 66
        :id 0
        :type "road-blood"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      3582 {
        :x 48
        :y 62
        :id 0
        :type "road-green"
        :index [ 6 12 6 12 ]
        :l "ground"
        :library "subtile"
      }
      1675 {
        :x 22
        :y 29
        :id 0
        :type "road-ice"
        :index [ 8 8 9 1 ]
        :l "ground"
        :library "subtile"
      }
      4354 {
        :x 22
        :y 76
        :id 0
        :type "road-blood"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      2759 {
        :x 23
        :y 48
        :id 0
        :type "road-green"
        :index [ 9 12 3 12 ]
        :l "ground"
        :library "subtile"
      }
      1432 {
        :x 7
        :y 25
        :id 0
        :type "road-ice"
        :index [ 8 8 9 1 ]
        :l "ground"
        :library "subtile"
      }
      3996 {
        :x 6
        :y 70
        :id 0
        :type "road-blood"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4418 {
        :x 29
        :y 77
        :id 0
        :type "road-blood"
        :index [ 9 9 9 1 ]
        :l "ground"
        :library "subtile"
      }
      3000 {
        :x 36
        :y 52
        :id 0
        :type "road-green"
        :index [ 8 8 9 1 ]
        :l "ground"
        :library "subtile"
      }
      2137 {
        :x 28
        :y 37
        :id 0
        :type "road-green"
        :index [ 8 11 10 13 ]
        :l "ground"
        :library "subtile"
      }
      2427 {
        :x 33
        :y 42
        :id 0
        :type "road-green"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      6245 {
        :x 32
        :y 109
        :id 0
        :type "road-hell"
        :index [ 8 11 10 13 ]
        :l "ground"
        :library "subtile"
      }
      6243 {
        :x 30
        :y 109
        :id 0
        :type "road-hell"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      2648 {
        :x 26
        :y 46
        :id 0
        :type "road-green"
        :index [ 8 11 10 13 ]
        :l "ground"
        :library "subtile"
      }
      6241 {
        :x 28
        :y 109
        :id 0
        :type "road-hell"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      1911 {
        :x 30
        :y 33
        :id 0
        :type "road-ice"
        :index [ 8 8 3 1 ]
        :l "ground"
        :library "subtile"
      }
      2998 {
        :x 34
        :y 52
        :id 0
        :type "road-green"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4419 {
        :x 30
        :y 77
        :id 0
        :type "road-blood"
        :index [ 9 9 10 10 ]
        :l "ground"
        :library "subtile"
      }
      6238 {
        :x 25
        :y 109
        :id 0
        :type "road-hell"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      2600 {
        :x 35
        :y 45
        :id 0
        :type "road-green"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      3901 {
        :x 25
        :y 68
        :id 0
        :type "road-blood"
        :index [ 6 12 6 12 ]
        :l "ground"
        :library "subtile"
      }
      3292 {
        :x 43
        :y 57
        :id 0
        :type "road-green"
        :index [ 9 9 10 10 ]
        :l "ground"
        :library "subtile"
      }
      3525 {
        :x 48
        :y 61
        :id 0
        :type "road-green"
        :index [ 6 12 6 12 ]
        :l "ground"
        :library "subtile"
      }
      2315 {
        :x 35
        :y 40
        :id 0
        :type "road-green"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4061 {
        :x 14
        :y 71
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3788 {
        :x 26
        :y 66
        :id 0
        :type "road-blood"
        :index [ 9 12 9 12 ]
        :l "ground"
        :library "subtile"
      }
      3350 {
        :x 44
        :y 58
        :id 0
        :type "road-green"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      4797 {
        :x 9
        :y 84
        :id 0
        :type "road-blood"
        :index [ 6 12 6 12 ]
        :l "ground"
        :library "subtile"
      }
      4396 {
        :x 7
        :y 77
        :id 0
        :type "road-blood"
        :index [ 9 2 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4356 {
        :x 24
        :y 76
        :id 0
        :type "road-blood"
        :index [ 8 8 3 9 ]
        :l "ground"
        :library "subtile"
      }
      3294 {
        :x 45
        :y 57
        :id 0
        :type "road-green"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3740 {
        :x 35
        :y 65
        :id 0
        :type "road-blood"
        :index [ 9 9 9 1 ]
        :l "ground"
        :library "subtile"
      }
      3997 {
        :x 7
        :y 70
        :id 0
        :type "road-blood"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4420 {
        :x 31
        :y 77
        :id 0
        :type "road-blood"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      3001 {
        :x 37
        :y 52
        :id 0
        :type "road-green"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      4452 {
        :x 6
        :y 78
        :id 0
        :type "road-blood"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      4453 {
        :x 7
        :y 78
        :id 0
        :type "road-blood"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      4230 {
        :x 12
        :y 74
        :id 0
        :type "road-blood"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      310 {
        :x 25
        :y 5
        :id 0
        :type "road-heaven"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      4572 {
        :x 12
        :y 80
        :id 0
        :type "road-blood"
        :index [ 8 11 10 13 ]
        :l "ground"
        :library "subtile"
      }
      1842 {
        :x 18
        :y 32
        :id 0
        :type "road-ice"
        :index [ 6 9 6 9 ]
        :l "ground"
        :library "subtile"
      }
      2428 {
        :x 34
        :y 42
        :id 0
        :type "road-green"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4744 {
        :x 13
        :y 83
        :id 0
        :type "road-blood"
        :index [ 5 8 7 10 ]
        :l "ground"
        :library "subtile"
      }
      1049 {
        :x 23
        :y 18
        :id 0
        :type "road-ice"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      3966 {
        :x 33
        :y 69
        :id 0
        :type "road-blood"
        :index [ 6 12 7 13 ]
        :l "ground"
        :library "subtile"
      }
      1489 {
        :x 7
        :y 26
        :id 0
        :type "road-ice"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      4684 {
        :x 10
        :y 82
        :id 0
        :type "road-blood"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      4683 {
        :x 9
        :y 82
        :id 0
        :type "road-blood"
        :index [ 6 9 6 1 ]
        :l "ground"
        :library "subtile"
      }
      316 {
        :x 31
        :y 5
        :id 0
        :type "road-heaven"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      4166 {
        :x 5
        :y 73
        :id 0
        :type "road-blood"
        :index [ 6 9 6 9 ]
        :l "ground"
        :library "subtile"
      }
      314 {
        :x 29
        :y 5
        :id 0
        :type "road-heaven"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      2601 {
        :x 36
        :y 45
        :id 0
        :type "road-green"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      312 {
        :x 27
        :y 5
        :id 0
        :type "road-heaven"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      4231 {
        :x 13
        :y 74
        :id 0
        :type "road-blood"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      1910 {
        :x 29
        :y 33
        :id 0
        :type "road-ice"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      1788 {
        :x 21
        :y 31
        :id 0
        :type "road-ice"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      4062 {
        :x 15
        :y 71
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3852 {
        :x 33
        :y 67
        :id 0
        :type "road-blood"
        :index [ 9 12 3 12 ]
        :l "ground"
        :library "subtile"
      }
      4626 {
        :x 9
        :y 81
        :id 0
        :type "road-blood"
        :index [ 6 9 6 9 ]
        :l "ground"
        :library "subtile"
      }
      1676 {
        :x 23
        :y 29
        :id 0
        :type "road-ice"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      3994 {
        :x 4
        :y 70
        :id 0
        :type "road-blood"
        :index [ 8 8 3 9 ]
        :l "ground"
        :library "subtile"
      }
      1668 {
        :x 15
        :y 29
        :id 0
        :type "road-ice"
        :index [ 5 8 7 10 ]
        :l "ground"
        :library "subtile"
      }
      3295 {
        :x 46
        :y 57
        :id 0
        :type "road-green"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1433 {
        :x 8
        :y 25
        :id 0
        :type "road-ice"
        :index [ 8 11 10 13 ]
        :l "ground"
        :library "subtile"
      }
      3998 {
        :x 8
        :y 70
        :id 0
        :type "road-blood"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4167 {
        :x 6
        :y 73
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3002 {
        :x 38
        :y 52
        :id 0
        :type "road-green"
        :index [ 8 11 10 13 ]
        :l "ground"
        :library "subtile"
      }
      2476 {
        :x 25
        :y 43
        :id 0
        :type "road-green"
        :index [ 5 8 7 10 ]
        :l "ground"
        :library "subtile"
      }
      4745 {
        :x 14
        :y 83
        :id 0
        :type "road-blood"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      4232 {
        :x 14
        :y 74
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4568 {
        :x 8
        :y 80
        :id 0
        :type "road-blood"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      3168 {
        :x 33
        :y 55
        :id 0
        :type "road-green"
        :index [ 6 9 6 9 ]
        :l "ground"
        :library "subtile"
      }
      3409 {
        :x 46
        :y 59
        :id 0
        :type "road-green"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      2429 {
        :x 35
        :y 42
        :id 0
        :type "road-green"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3459 {
        :x 39
        :y 60
        :id 0
        :type "road-blood"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      4530 {
        :x 27
        :y 79
        :id 0
        :type "road-blood"
        :index [ 6 9 6 1 ]
        :l "ground"
        :library "subtile"
      }
      4513 {
        :x 10
        :y 79
        :id 0
        :type "road-blood"
        :index [ 9 12 9 12 ]
        :l "ground"
        :library "subtile"
      }
      4359 {
        :x 27
        :y 76
        :id 0
        :type "road-blood"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4512 {
        :x 9
        :y 79
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4456 {
        :x 10
        :y 78
        :id 0
        :type "road-blood"
        :index [ 9 12 9 12 ]
        :l "ground"
        :library "subtile"
      }
      4454 {
        :x 8
        :y 78
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4168 {
        :x 7
        :y 73
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1899 {
        :x 18
        :y 33
        :id 0
        :type "road-ice"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      4455 {
        :x 9
        :y 78
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4746 {
        :x 15
        :y 83
        :id 0
        :type "road-blood"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      4233 {
        :x 15
        :y 74
        :id 0
        :type "road-blood"
        :index [ 9 12 9 12 ]
        :l "ground"
        :library "subtile"
      }
      4414 {
        :x 25
        :y 77
        :id 0
        :type "road-blood"
        :index [ 9 9 10 10 ]
        :l "ground"
        :library "subtile"
      }
      2317 {
        :x 37
        :y 40
        :id 0
        :type "road-green"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4063 {
        :x 16
        :y 71
        :id 0
        :type "road-blood"
        :index [ 9 9 9 1 ]
        :l "ground"
        :library "subtile"
      }
      4403 {
        :x 14
        :y 77
        :id 0
        :type "road-blood"
        :index [ 6 12 7 13 ]
        :l "ground"
        :library "subtile"
      }
      2310 {
        :x 30
        :y 40
        :id 0
        :type "road-green"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4399 {
        :x 10
        :y 77
        :id 0
        :type "road-blood"
        :index [ 8 8 9 1 ]
        :l "ground"
        :library "subtile"
      }
      4398 {
        :x 9
        :y 77
        :id 0
        :type "road-blood"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4360 {
        :x 28
        :y 76
        :id 0
        :type "road-blood"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4109 {
        :x 5
        :y 72
        :id 0
        :type "road-blood"
        :index [ 6 9 6 9 ]
        :l "ground"
        :library "subtile"
      }
      3232 {
        :x 40
        :y 56
        :id 0
        :type "road-green"
        :index [ 5 8 7 10 ]
        :l "ground"
        :library "subtile"
      }
      3999 {
        :x 9
        :y 70
        :id 0
        :type "road-blood"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3726 {
        :x 21
        :y 65
        :id 0
        :type "road-blood"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      3457 {
        :x 37
        :y 60
        :id 0
        :type "road-blood"
        :index [ 5 8 7 10 ]
        :l "ground"
        :library "subtile"
      }
      2477 {
        :x 26
        :y 43
        :id 0
        :type "road-green"
        :index [ 8 11 10 13 ]
        :l "ground"
        :library "subtile"
      }
      4747 {
        :x 16
        :y 83
        :id 0
        :type "road-blood"
        :index [ 8 11 10 13 ]
        :l "ground"
        :library "subtile"
      }
      3298 {
        :x 49
        :y 57
        :id 0
        :type "road-green"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1329 {
        :x 18
        :y 23
        :id 0
        :type "road-ice"
        :index [ 5 8 7 10 ]
        :l "ground"
        :library "subtile"
      }
      3169 {
        :x 34
        :y 55
        :id 0
        :type "road-green"
        :index [ 9 9 9 1 ]
        :l "ground"
        :library "subtile"
      }
      1843 {
        :x 19
        :y 32
        :id 0
        :type "road-ice"
        :index [ 9 9 9 1 ]
        :l "ground"
        :library "subtile"
      }
      2430 {
        :x 36
        :y 42
        :id 0
        :type "road-green"
        :index [ 9 12 9 12 ]
        :l "ground"
        :library "subtile"
      }
      3639 {
        :x 48
        :y 63
        :id 0
        :type "road-green"
        :index [ 6 12 7 13 ]
        :l "ground"
        :library "subtile"
      }
      1050 {
        :x 24
        :y 18
        :id 0
        :type "road-ice"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      4366 {
        :x 34
        :y 76
        :id 0
        :type "road-blood"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      4361 {
        :x 29
        :y 76
        :id 0
        :type "road-blood"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4111 {
        :x 7
        :y 72
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4364 {
        :x 32
        :y 76
        :id 0
        :type "road-blood"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      4053 {
        :x 6
        :y 71
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      2366 {
        :x 29
        :y 41
        :id 0
        :type "road-green"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      4362 {
        :x 30
        :y 76
        :id 0
        :type "road-blood"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4358 {
        :x 26
        :y 76
        :id 0
        :type "road-blood"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4357 {
        :x 25
        :y 76
        :id 0
        :type "road-blood"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4355 {
        :x 23
        :y 76
        :id 0
        :type "road-blood"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      4353 {
        :x 21
        :y 76
        :id 0
        :type "road-blood"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      2318 {
        :x 38
        :y 40
        :id 0
        :type "road-green"
        :index [ 8 8 9 1 ]
        :l "ground"
        :library "subtile"
      }
      3052 {
        :x 31
        :y 53
        :id 0
        :type "road-green"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      4352 {
        :x 20
        :y 76
        :id 0
        :type "road-blood"
        :index [ 5 8 7 10 ]
        :l "ground"
        :library "subtile"
      }
      4347 {
        :x 15
        :y 76
        :id 0
        :type "road-blood"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      1677 {
        :x 24
        :y 29
        :id 0
        :type "road-ice"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      4346 {
        :x 14
        :y 76
        :id 0
        :type "road-blood"
        :index [ 6 9 6 1 ]
        :l "ground"
        :library "subtile"
      }
      1426 {
        :x 1
        :y 25
        :id 0
        :type "road-ice"
        :index [ 5 8 7 10 ]
        :l "ground"
        :library "subtile"
      }
      4339 {
        :x 7
        :y 76
        :id 0
        :type "road-blood"
        :index [ 9 12 9 12 ]
        :l "ground"
        :library "subtile"
      }
      3233 {
        :x 41
        :y 56
        :id 0
        :type "road-green"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      1171 {
        :x 31
        :y 20
        :id 0
        :type "road-ice"
        :index [ 5 8 7 10 ]
        :l "ground"
        :library "subtile"
      }
      3727 {
        :x 22
        :y 65
        :id 0
        :type "road-blood"
        :index [ 9 9 10 10 ]
        :l "ground"
        :library "subtile"
      }
      3458 {
        :x 38
        :y 60
        :id 0
        :type "road-blood"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      4000 {
        :x 10
        :y 70
        :id 0
        :type "road-blood"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4338 {
        :x 6
        :y 76
        :id 0
        :type "road-blood"
        :index [ 6 9 6 9 ]
        :l "ground"
        :library "subtile"
      }
      4290 {
        :x 15
        :y 75
        :id 0
        :type "road-blood"
        :index [ 9 12 9 12 ]
        :l "ground"
        :library "subtile"
      }
      4289 {
        :x 14
        :y 75
        :id 0
        :type "road-blood"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      3170 {
        :x 35
        :y 55
        :id 0
        :type "road-green"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      3411 {
        :x 48
        :y 59
        :id 0
        :type "road-green"
        :index [ 9 9 9 1 ]
        :l "ground"
        :library "subtile"
      }
      4283 {
        :x 8
        :y 75
        :id 0
        :type "road-blood"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      4281 {
        :x 6
        :y 75
        :id 0
        :type "road-blood"
        :index [ 6 9 6 9 ]
        :l "ground"
        :library "subtile"
      }
      4587 {
        :x 27
        :y 80
        :id 0
        :type "road-blood"
        :index [ 6 12 6 12 ]
        :l "ground"
        :library "subtile"
      }
      3680 {
        :x 32
        :y 64
        :id 0
        :type "road-blood"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4363 {
        :x 31
        :y 76
        :id 0
        :type "road-blood"
        :index [ 8 8 9 1 ]
        :l "ground"
        :library "subtile"
      }
      4064 {
        :x 17
        :y 71
        :id 0
        :type "road-blood"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      4395 {
        :x 6
        :y 77
        :id 0
        :type "road-blood"
        :index [ 6 9 6 9 ]
        :l "ground"
        :library "subtile"
      }
      4172 {
        :x 11
        :y 73
        :id 0
        :type "road-blood"
        :index [ 9 9 10 10 ]
        :l "ground"
        :library "subtile"
      }
      2367 {
        :x 30
        :y 41
        :id 0
        :type "road-green"
        :index [ 9 9 10 10 ]
        :l "ground"
        :library "subtile"
      }
      2877 {
        :x 27
        :y 50
        :id 0
        :type "road-green"
        :index [ 6 9 6 1 ]
        :l "ground"
        :library "subtile"
      }
      1900 {
        :x 19
        :y 33
        :id 0
        :type "road-ice"
        :index [ 9 12 3 12 ]
        :l "ground"
        :library "subtile"
      }
      4171 {
        :x 10
        :y 73
        :id 0
        :type "road-blood"
        :index [ 9 9 10 10 ]
        :l "ground"
        :library "subtile"
      }
      4170 {
        :x 9
        :y 73
        :id 0
        :type "road-blood"
        :index [ 9 9 9 1 ]
        :l "ground"
        :library "subtile"
      }
      4169 {
        :x 8
        :y 73
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      2319 {
        :x 39
        :y 40
        :id 0
        :type "road-green"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      3053 {
        :x 32
        :y 53
        :id 0
        :type "road-green"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      1669 {
        :x 16
        :y 29
        :id 0
        :type "road-ice"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      2316 {
        :x 36
        :y 40
        :id 0
        :type "road-green"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      2543 {
        :x 35
        :y 44
        :id 0
        :type "road-green"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      4118 {
        :x 14
        :y 72
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3282 {
        :x 33
        :y 57
        :id 0
        :type "road-green"
        :index [ 6 12 7 13 ]
        :l "ground"
        :library "subtile"
      }
      3728 {
        :x 23
        :y 65
        :id 0
        :type "road-blood"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      3234 {
        :x 42
        :y 56
        :id 0
        :type "road-green"
        :index [ 8 8 3 9 ]
        :l "ground"
        :library "subtile"
      }
      4117 {
        :x 13
        :y 72
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4173 {
        :x 12
        :y 73
        :id 0
        :type "road-blood"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      1549 {
        :x 10
        :y 27
        :id 0
        :type "road-ice"
        :index [ 5 8 7 10 ]
        :l "ground"
        :library "subtile"
      }
      1282 {
        :x 28
        :y 22
        :id 0
        :type "road-ice"
        :index [ 6 12 7 13 ]
        :l "ground"
        :library "subtile"
      }
      4115 {
        :x 11
        :y 72
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3686 {
        :x 38
        :y 64
        :id 0
        :type "road-blood"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      1330 {
        :x 19
        :y 23
        :id 0
        :type "road-ice"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      4113 {
        :x 9
        :y 72
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3412 {
        :x 49
        :y 59
        :id 0
        :type "road-green"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      1844 {
        :x 20
        :y 32
        :id 0
        :type "road-ice"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      4001 {
        :x 11
        :y 70
        :id 0
        :type "road-blood"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1051 {
        :x 25
        :y 18
        :id 0
        :type "road-ice"
        :index [ 8 8 3 9 ]
        :l "ground"
        :library "subtile"
      }
      3681 {
        :x 33
        :y 64
        :id 0
        :type "road-blood"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4365 {
        :x 33
        :y 76
        :id 0
        :type "road-blood"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      4110 {
        :x 6
        :y 72
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4397 {
        :x 8
        :y 77
        :id 0
        :type "road-blood"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      2368 {
        :x 31
        :y 41
        :id 0
        :type "road-green"
        :index [ 9 9 10 10 ]
        :l "ground"
        :library "subtile"
      }
      4174 {
        :x 13
        :y 73
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      2878 {
        :x 28
        :y 50
        :id 0
        :type "road-green"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      4060 {
        :x 13
        :y 71
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4058 {
        :x 11
        :y 71
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4057 {
        :x 10
        :y 71
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      2320 {
        :x 40
        :y 40
        :id 0
        :type "road-green"
        :index [ 8 11 10 13 ]
        :l "ground"
        :library "subtile"
      }
      3299 {
        :x 50
        :y 57
        :id 0
        :type "road-green"
        :index [ 9 9 9 1 ]
        :l "ground"
        :library "subtile"
      }
      3054 {
        :x 33
        :y 53
        :id 0
        :type "road-green"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4056 {
        :x 9
        :y 71
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      2544 {
        :x 36
        :y 44
        :id 0
        :type "road-green"
        :index [ 9 12 9 12 ]
        :l "ground"
        :library "subtile"
      }
      1678 {
        :x 25
        :y 29
        :id 0
        :type "road-ice"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      4055 {
        :x 8
        :y 71
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1427 {
        :x 2
        :y 25
        :id 0
        :type "road-ice"
        :index [ 8 8 3 9 ]
        :l "ground"
        :library "subtile"
      }
      3729 {
        :x 24
        :y 65
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1172 {
        :x 32
        :y 20
        :id 0
        :type "road-ice"
        :index [ 8 8 3 9 ]
        :l "ground"
        :library "subtile"
      }
      2645 {
        :x 23
        :y 46
        :id 0
        :type "road-green"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4175 {
        :x 14
        :y 73
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1957 {
        :x 19
        :y 34
        :id 0
        :type "road-ice"
        :index [ 6 12 7 13 ]
        :l "ground"
        :library "subtile"
      }
      4002 {
        :x 12
        :y 70
        :id 0
        :type "road-blood"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3673 {
        :x 25
        :y 64
        :id 0
        :type "road-blood"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4051 {
        :x 4
        :y 71
        :id 0
        :type "road-blood"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      4008 {
        :x 18
        :y 70
        :id 0
        :type "road-blood"
        :index [ 8 11 10 13 ]
        :l "ground"
        :library "subtile"
      }
      2701 {
        :x 22
        :y 47
        :id 0
        :type "road-green"
        :index [ 6 9 6 9 ]
        :l "ground"
        :library "subtile"
      }
      4007 {
        :x 17
        :y 70
        :id 0
        :type "road-blood"
        :index [ 8 8 9 1 ]
        :l "ground"
        :library "subtile"
      }
      4006 {
        :x 16
        :y 70
        :id 0
        :type "road-blood"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4005 {
        :x 15
        :y 70
        :id 0
        :type "road-blood"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      2192 {
        :x 26
        :y 38
        :id 0
        :type "road-green"
        :index [ 6 12 7 13 ]
        :l "ground"
        :library "subtile"
      }
      3682 {
        :x 34
        :y 64
        :id 0
        :type "road-blood"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4367 {
        :x 35
        :y 76
        :id 0
        :type "road-blood"
        :index [ 8 11 10 13 ]
        :l "ground"
        :library "subtile"
      }
      3679 {
        :x 31
        :y 64
        :id 0
        :type "road-blood"
        :index [ 5 8 6 9 ]
        :l "ground"
        :library "subtile"
      }
      4112 {
        :x 8
        :y 72
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      2369 {
        :x 32
        :y 41
        :id 0
        :type "road-green"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      3995 {
        :x 5
        :y 70
        :id 0
        :type "road-blood"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4176 {
        :x 15
        :y 73
        :id 0
        :type "road-blood"
        :index [ 9 12 9 12 ]
        :l "ground"
        :library "subtile"
      }
      3993 {
        :x 3
        :y 70
        :id 0
        :type "road-blood"
        :index [ 5 8 7 10 ]
        :l "ground"
        :library "subtile"
      }
      308 {
        :x 23
        :y 5
        :id 0
        :type "road-heaven"
        :index [ 5 8 7 10 ]
        :l "ground"
        :library "subtile"
      }
      3909 {
        :x 33
        :y 68
        :id 0
        :type "road-blood"
        :index [ 6 12 6 12 ]
        :l "ground"
        :library "subtile"
      }
      3794 {
        :x 32
        :y 66
        :id 0
        :type "road-blood"
        :index [ 6 9 6 9 ]
        :l "ground"
        :library "subtile"
      }
      3300 {
        :x 51
        :y 57
        :id 0
        :type "road-green"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      3055 {
        :x 34
        :y 53
        :id 0
        :type "road-green"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1909 {
        :x 28
        :y 33
        :id 0
        :type "road-ice"
        :index [ 5 8 7 10 ]
        :l "ground"
        :library "subtile"
      }
      1108 {
        :x 25
        :y 19
        :id 0
        :type "road-ice"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      4627 {
        :x 10
        :y 81
        :id 0
        :type "road-blood"
        :index [ 9 9 9 1 ]
        :l "ground"
        :library "subtile"
      }
      3845 {
        :x 26
        :y 67
        :id 0
        :type "road-blood"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      3685 {
        :x 37
        :y 64
        :id 0
        :type "road-blood"
        :index [ 8 8 9 1 ]
        :l "ground"
        :library "subtile"
      }
      3730 {
        :x 25
        :y 65
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3236 {
        :x 44
        :y 56
        :id 0
        :type "road-green"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4400 {
        :x 11
        :y 77
        :id 0
        :type "road-blood"
        :index [ 8 11 10 13 ]
        :l "ground"
        :library "subtile"
      }
      3239 {
        :x 47
        :y 56
        :id 0
        :type "road-green"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1550 {
        :x 11
        :y 27
        :id 0
        :type "road-ice"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      4003 {
        :x 13
        :y 70
        :id 0
        :type "road-blood"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3786 {
        :x 24
        :y 66
        :id 0
        :type "road-blood"
        :index [ 9 9 10 10 ]
        :l "ground"
        :library "subtile"
      }
      3742 {
        :x 37
        :y 65
        :id 0
        :type "road-blood"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      1331 {
        :x 20
        :y 23
        :id 0
        :type "road-ice"
        :index [ 8 8 3 9 ]
        :l "ground"
        :library "subtile"
      }
      2702 {
        :x 23
        :y 47
        :id 0
        :type "road-green"
        :index [ 9 9 9 1 ]
        :l "ground"
        :library "subtile"
      }
      3741 {
        :x 36
        :y 65
        :id 0
        :type "road-blood"
        :index [ 9 9 10 10 ]
        :l "ground"
        :library "subtile"
      }
      3739 {
        :x 34
        :y 65
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      317 {
        :x 32
        :y 5
        :id 0
        :type "road-heaven"
        :index [ 8 11 10 13 ]
        :l "ground"
        :library "subtile"
      }
      4628 {
        :x 11
        :y 81
        :id 0
        :type "road-blood"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      1052 {
        :x 26
        :y 18
        :id 0
        :type "road-ice"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3687 {
        :x 39
        :y 64
        :id 0
        :type "road-blood"
        :index [ 8 11 10 13 ]
        :l "ground"
        :library "subtile"
      }
      4114 {
        :x 10
        :y 72
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3844 {
        :x 25
        :y 67
        :id 0
        :type "road-blood"
        :index [ 6 9 6 1 ]
        :l "ground"
        :library "subtile"
      }
      2370 {
        :x 33
        :y 41
        :id 0
        :type "road-green"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1229 {
        :x 32
        :y 21
        :id 0
        :type "road-ice"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      1484 {
        :x 2
        :y 26
        :id 0
        :type "road-ice"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      3351 {
        :x 45
        :y 58
        :id 0
        :type "road-green"
        :index [ 9 9 10 10 ]
        :l "ground"
        :library "subtile"
      }
      3683 {
        :x 35
        :y 64
        :id 0
        :type "road-blood"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      2816 {
        :x 23
        :y 49
        :id 0
        :type "road-green"
        :index [ 6 12 7 13 ]
        :l "ground"
        :library "subtile"
      }
      3795 {
        :x 33
        :y 66
        :id 0
        :type "road-blood"
        :index [ 9 9 9 1 ]
        :l "ground"
        :library "subtile"
      }
      3675 {
        :x 27
        :y 64
        :id 0
        :type "road-blood"
        :index [ 8 11 9 12 ]
        :l "ground"
        :library "subtile"
      }
      1508 {
        :x 26
        :y 26
        :id 0
        :type "road-ice"
        :index [ 5 8 7 10 ]
        :l "ground"
        :library "subtile"
      }
      4052 {
        :x 5
        :y 71
        :id 0
        :type "road-blood"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      3672 {
        :x 24
        :y 64
        :id 0
        :type "road-blood"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1679 {
        :x 26
        :y 29
        :id 0
        :type "road-ice"
        :index [ 8 11 10 13 ]
        :l "ground"
        :library "subtile"
      }
      1428 {
        :x 3
        :y 25
        :id 0
        :type "road-ice"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1671 {
        :x 18
        :y 29
        :id 0
        :type "road-ice"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3731 {
        :x 26
        :y 65
        :id 0
        :type "road-blood"
        :index [ 9 9 9 1 ]
        :l "ground"
        :library "subtile"
      }
      1173 {
        :x 33
        :y 20
        :id 0
        :type "road-ice"
        :index [ 8 8 9 1 ]
        :l "ground"
        :library "subtile"
      }
      3114 {
        :x 36
        :y 54
        :id 0
        :type "road-green"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      3460 {
        :x 40
        :y 60
        :id 0
        :type "road-blood"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      3667 {
        :x 19
        :y 64
        :id 0
        :type "road-blood"
        :index [ 5 8 7 10 ]
        :l "ground"
        :library "subtile"
      }
      4004 {
        :x 14
        :y 70
        :id 0
        :type "road-blood"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      6236 {
        :x 23
        :y 109
        :id 0
        :type "road-hell"
        :index [ 5 8 7 10 ]
        :l "ground"
        :library "subtile"
      }
      3461 {
        :x 41
        :y 60
        :id 0
        :type "road-blood"
        :index [ 8 11 10 13 ]
        :l "ground"
        :library "subtile"
      }
      3668 {
        :x 20
        :y 64
        :id 0
        :type "road-blood"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      1727 {
        :x 17
        :y 30
        :id 0
        :type "road-ice"
        :index [ 6 9 6 9 ]
        :l "ground"
        :library "subtile"
      }
      4531 {
        :x 28
        :y 79
        :id 0
        :type "road-blood"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      3410 {
        :x 47
        :y 59
        :id 0
        :type "road-green"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      3353 {
        :x 47
        :y 58
        :id 0
        :type "road-green"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3352 {
        :x 46
        :y 58
        :id 0
        :type "road-green"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      3684 {
        :x 36
        :y 64
        :id 0
        :type "road-blood"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      4854 {
        :x 9
        :y 85
        :id 0
        :type "road-blood"
        :index [ 6 12 7 13 ]
        :l "ground"
        :library "subtile"
      }
      4116 {
        :x 12
        :y 72
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3110 {
        :x 32
        :y 54
        :id 0
        :type "road-green"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      2371 {
        :x 34
        :y 41
        :id 0
        :type "road-green"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3297 {
        :x 48
        :y 57
        :id 0
        :type "road-green"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3296 {
        :x 47
        :y 57
        :id 0
        :type "road-green"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3293 {
        :x 44
        :y 57
        :id 0
        :type "road-green"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      6237 {
        :x 24
        :y 109
        :id 0
        :type "road-hell"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      3243 {
        :x 51
        :y 56
        :id 0
        :type "road-green"
        :index [ 8 8 9 1 ]
        :l "ground"
        :library "subtile"
      }
      3796 {
        :x 34
        :y 66
        :id 0
        :type "road-blood"
        :index [ 9 9 10 10 ]
        :l "ground"
        :library "subtile"
      }
      315 {
        :x 30
        :y 5
        :id 0
        :type "road-heaven"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      2307 {
        :x 27
        :y 40
        :id 0
        :type "road-green"
        :index [ 5 8 7 10 ]
        :l "ground"
        :library "subtile"
      }
      1388 {
        :x 20
        :y 24
        :id 0
        :type "road-ice"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      1109 {
        :x 26
        :y 19
        :id 0
        :type "road-ice"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      3057 {
        :x 36
        :y 53
        :id 0
        :type "road-green"
        :index [ 9 12 9 12 ]
        :l "ground"
        :library "subtile"
      }
      3237 {
        :x 45
        :y 56
        :id 0
        :type "road-green"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3235 {
        :x 43
        :y 56
        :id 0
        :type "road-green"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3732 {
        :x 27
        :y 65
        :id 0
        :type "road-blood"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      3238 {
        :x 46
        :y 56
        :id 0
        :type "road-green"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1543 {
        :x 4
        :y 27
        :id 0
        :type "road-ice"
        :index [ 6 9 6 1 ]
        :l "ground"
        :library "subtile"
      }
      2999 {
        :x 35
        :y 52
        :id 0
        :type "road-green"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1551 {
        :x 12
        :y 27
        :id 0
        :type "road-ice"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      2993 {
        :x 29
        :y 52
        :id 0
        :type "road-green"
        :index [ 5 8 7 10 ]
        :l "ground"
        :library "subtile"
      }
      2704 {
        :x 25
        :y 47
        :id 0
        :type "road-green"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      311 {
        :x 26
        :y 5
        :id 0
        :type "road-heaven"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      3669 {
        :x 21
        :y 64
        :id 0
        :type "road-blood"
        :index [ 8 8 3 9 ]
        :l "ground"
        :library "subtile"
      }
      1332 {
        :x 21
        :y 23
        :id 0
        :type "road-ice"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3113 {
        :x 35
        :y 54
        :id 0
        :type "road-green"
        :index [ 9 9 9 1 ]
        :l "ground"
        :library "subtile"
      }
      3112 {
        :x 34
        :y 54
        :id 0
        :type "road-green"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3056 {
        :x 35
        :y 53
        :id 0
        :type "road-green"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      309 {
        :x 24
        :y 5
        :id 0
        :type "road-heaven"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      1053 {
        :x 27
        :y 18
        :id 0
        :type "road-ice"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3958 {
        :x 25
        :y 69
        :id 0
        :type "road-blood"
        :index [ 6 12 7 13 ]
        :l "ground"
        :library "subtile"
      }
      1728 {
        :x 18
        :y 30
        :id 0
        :type "road-ice"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3111 {
        :x 33
        :y 54
        :id 0
        :type "road-green"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      2372 {
        :x 35
        :y 41
        :id 0
        :type "road-green"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1230 {
        :x 33
        :y 21
        :id 0
        :type "road-ice"
        :index [ 9 12 3 12 ]
        :l "ground"
        :library "subtile"
      }
      1485 {
        :x 3
        :y 26
        :id 0
        :type "road-ice"
        :index [ 9 9 10 10 ]
        :l "ground"
        :library "subtile"
      }
      313 {
        :x 28
        :y 5
        :id 0
        :type "road-heaven"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      6239 {
        :x 26
        :y 109
        :id 0
        :type "road-hell"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      2997 {
        :x 33
        :y 52
        :id 0
        :type "road-green"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      3797 {
        :x 35
        :y 66
        :id 0
        :type "road-blood"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      2703 {
        :x 24
        :y 47
        :id 0
        :type "road-green"
        :index [ 9 9 10 10 ]
        :l "ground"
        :library "subtile"
      }
      1509 {
        :x 27
        :y 26
        :id 0
        :type "road-ice"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      4054 {
        :x 7
        :y 71
        :id 0
        :type "road-blood"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      2643 {
        :x 21
        :y 46
        :id 0
        :type "road-green"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      1672 {
        :x 19
        :y 29
        :id 0
        :type "road-ice"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1429 {
        :x 4
        :y 25
        :id 0
        :type "road-ice"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      2542 {
        :x 34
        :y 44
        :id 0
        :type "road-green"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      4119 {
        :x 15
        :y 72
        :id 0
        :type "road-blood"
        :index [ 9 9 9 1 ]
        :l "ground"
        :library "subtile"
      }
      1174 {
        :x 34
        :y 20
        :id 0
        :type "road-ice"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      1487 {
        :x 5
        :y 26
        :id 0
        :type "road-ice"
        :index [ 9 9 9 1 ]
        :l "ground"
        :library "subtile"
      }
      1166 {
        :x 26
        :y 20
        :id 0
        :type "road-ice"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      2484 {
        :x 33
        :y 43
        :id 0
        :type "road-green"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      2994 {
        :x 30
        :y 52
        :id 0
        :type "road-green"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      2308 {
        :x 28
        :y 40
        :id 0
        :type "road-green"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      6240 {
        :x 27
        :y 109
        :id 0
        :type "road-hell"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      3670 {
        :x 22
        :y 64
        :id 0
        :type "road-blood"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1785 {
        :x 18
        :y 31
        :id 0
        :type "road-ice"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      1784 {
        :x 17
        :y 31
        :id 0
        :type "road-ice"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      1732 {
        :x 22
        :y 30
        :id 0
        :type "road-ice"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      1674 {
        :x 21
        :y 29
        :id 0
        :type "road-ice"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1673 {
        :x 20
        :y 29
        :id 0
        :type "road-ice"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      2642 {
        :x 20
        :y 46
        :id 0
        :type "road-green"
        :index [ 5 8 7 10 ]
        :l "ground"
        :library "subtile"
      }
      1670 {
        :x 17
        :y 29
        :id 0
        :type "road-ice"
        :index [ 8 8 3 9 ]
        :l "ground"
        :library "subtile"
      }
      4120 {
        :x 16
        :y 72
        :id 0
        :type "road-blood"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      1600 {
        :x 4
        :y 28
        :id 0
        :type "road-ice"
        :index [ 6 12 7 13 ]
        :l "ground"
        :library "subtile"
      }
      2373 {
        :x 36
        :y 41
        :id 0
        :type "road-green"
        :index [ 9 9 9 1 ]
        :l "ground"
        :library "subtile"
      }
    }
  }
  :id 0
  :height 150
  :tilesize 16
}
