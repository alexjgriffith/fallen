{
  :width 10
  :data {
    0 {
      70 {
        :y 7
        :x 0
        :w 10
        :id 135
        :type "sun"
        :h 8
        :l 0
        :library "tile"
      }
      92 {
        :y 9
        :x 2
        :w 3
        :id 135
        :type "cloud"
        :h 2
        :l 0
        :library "tile"
      }
    }
    1 {
      122 {
        :x 2
        :y 12
        :id 135
        :type "road"
        :index [ 6 9 7 10 ]
        :l 1
        :library "subtile"
      }
      114 {
        :x 4
        :y 11
        :id 135
        :type "road"
        :index [ 8 8 9 1 ]
        :l 1
        :library "subtile"
      }
      134 {
        :x 4
        :y 13
        :id 135
        :type "road"
        :index [ 9 12 10 13 ]
        :l 1
        :library "subtile"
      }
      123 {
        :x 3
        :y 12
        :id 135
        :type "road"
        :index [ 9 9 3 9 ]
        :l 1
        :library "subtile"
      }
      115 {
        :x 5
        :y 11
        :id 135
        :type "road"
        :index [ 8 8 10 10 ]
        :l 1
        :library "subtile"
      }
      212 {
        :x 2
        :y 21
        :id 135
        :type "road"
        :index [ 4 12 10 13 ]
        :l 1
        :library "subtile"
      }
      124 {
        :x 4
        :y 12
        :id 135
        :type "road"
        :index [ 9 12 9 12 ]
        :l 1
        :library "subtile"
      }
      116 {
        :x 6
        :index [ 8 8 10 10 ]
        :id 135
        :type "road"
        :y 11
        :l 1
        :library "subtile"
      }
      197 {
        :x 7
        :y 19
        :id 135
        :type "road"
        :index [ 6 2 7 10 ]
        :l 1
        :library "subtile"
      }
      110 {
        :x 0
        :index [ 8 8 10 10 ]
        :id 135
        :type "road"
        :y 11
        :l 1
        :library "subtile"
      }
      209 {
        :x 9
        :y 20
        :id 135
        :type "road"
        :index [ 8 11 3 12 ]
        :l 1
        :library "subtile"
      }
      178 {
        :x 8
        :y 17
        :id 135
        :type "road"
        :index [ 8 11 10 13 ]
        :l 1
        :library "subtile"
      }
      208 {
        :x 8
        :y 20
        :id 135
        :type "road"
        :index [ 6 2 7 10 ]
        :l 1
        :library "subtile"
      }
      117 {
        :x 7
        :index [ 8 8 10 10 ]
        :id 135
        :type "road"
        :y 11
        :l 1
        :library "subtile"
      }
      211 {
        :x 1
        :y 21
        :id 135
        :type "road"
        :index [ 8 8 10 10 ]
        :l 1
        :library "subtile"
      }
      118 {
        :x 8
        :index [ 8 8 10 10 ]
        :id 135
        :type "road"
        :y 11
        :l 1
        :library "subtile"
      }
      210 {
        :x 0
        :y 21
        :id 135
        :type "road"
        :index [ 8 8 10 10 ]
        :l 1
        :library "subtile"
      }
      177 {
        :x 7
        :y 17
        :id 135
        :type "road"
        :index [ 5 8 6 1 ]
        :l 1
        :library "subtile"
      }
      119 {
        :x 9
        :index [ 8 8 10 10 ]
        :id 135
        :type "road"
        :y 11
        :l 1
        :library "subtile"
      }
      219 {
        :x 9
        :y 21
        :id 135
        :type "road"
        :index [ 6 2 7 10 ]
        :l 1
        :library "subtile"
      }
      111 {
        :x 1
        :y 11
        :id 135
        :type "road"
        :index [ 8 8 10 10 ]
        :l 1
        :library "subtile"
      }
      133 {
        :x 3
        :y 13
        :id 135
        :type "road"
        :index [ 6 9 7 10 ]
        :l 1
        :library "subtile"
      }
      198 {
        :x 8
        :y 19
        :id 135
        :type "road"
        :index [ 8 11 3 12 ]
        :l 1
        :library "subtile"
      }
      112 {
        :x 2
        :y 11
        :id 135
        :type "road"
        :index [ 8 8 3 9 ]
        :l 1
        :library "subtile"
      }
      187 {
        :x 7
        :y 18
        :id 135
        :type "road"
        :index [ 6 12 6 12 ]
        :l 1
        :library "subtile"
      }
      202 {
        :x 2
        :y 20
        :id 135
        :type "road"
        :index [ 5 11 6 12 ]
        :l 1
        :library "subtile"
      }
      113 {
        :x 3
        :y 11
        :id 135
        :type "road"
        :index [ 8 8 9 9 ]
        :l 1
        :library "subtile"
      }
    }
    2 {
      84 {
        :y 8
        :x 4
        :w 4
        :id 135
        :type "big-tree"
        :h 6
        :l 2
        :library "tile"
      }
    }
    3 {}
    4 {}
    -1 {}
  }
  :id 135
  :height 25
  :tilesize 16
}
