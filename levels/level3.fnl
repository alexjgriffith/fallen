{
  :width 10
  :data {
    0 {
      141 {
        :id 135
        :x 1
        :y 14
        :w 3
        :type "cloud"
        :h 2
        :l 0
        :library "tile"
      }
      118 {
        :id 135
        :x 8
        :y 11
        :w 3
        :type "cloud"
        :h 2
        :l 0
        :library "tile"
      }
      68 {
        :id 135
        :x 8
        :y 6
        :w 3
        :type "cloud"
        :h 2
        :l 0
        :library "tile"
      }
      176 {
        :id 135
        :x 6
        :y 17
        :w 3
        :type "cloud"
        :h 2
        :l 0
        :library "tile"
      }
      22 {
        :id 135
        :x 2
        :y 2
        :w 3
        :type "cloud"
        :h 2
        :l 0
        :library "tile"
      }
      124 {
        :id 135
        :x 4
        :y 12
        :w 10
        :type "sun"
        :h 8
        :l 0
        :library "tile"
      }
      46 {
        :id 135
        :x 6
        :y 4
        :w 3
        :type "cloud"
        :h 2
        :l 0
        :library "tile"
      }
      81 {
        :id 135
        :x 1
        :y 8
        :w 3
        :type "cloud"
        :h 2
        :l 0
        :library "tile"
      }
    }
    1 {
      122 {
        :x 2
        :y 12
        :id 135
        :type "road"
        :index [ 6 9 7 10 ]
        :l 1
        :library "subtile"
      }
      114 {
        :x 4
        :y 11
        :id 135
        :type "road"
        :index [ 8 8 9 1 ]
        :l 1
        :library "subtile"
      }
      134 {
        :x 4
        :y 13
        :id 135
        :type "road"
        :index [ 9 12 10 13 ]
        :l 1
        :library "subtile"
      }
      123 {
        :x 3
        :y 12
        :id 135
        :type "road"
        :index [ 9 9 3 9 ]
        :l 1
        :library "subtile"
      }
      115 {
        :x 5
        :y 11
        :id 135
        :type "road"
        :index [ 8 11 10 13 ]
        :l 1
        :library "subtile"
      }
      124 {
        :x 4
        :y 12
        :id 135
        :type "road"
        :index [ 9 12 9 12 ]
        :l 1
        :library "subtile"
      }
      194 {
        :x 4
        :index [ 8 11 10 13 ]
        :id 135
        :type "road"
        :y 19
        :l 1
        :library "subtile"
      }
      192 {
        :x 2
        :index [ 8 8 10 10 ]
        :id 135
        :type "road"
        :y 19
        :l 1
        :library "subtile"
      }
      193 {
        :x 3
        :index [ 8 8 10 10 ]
        :id 135
        :type "road"
        :y 19
        :l 1
        :library "subtile"
      }
      110 {
        :x 0
        :index [ 5 8 7 10 ]
        :id 135
        :type "road"
        :y 11
        :l 1
        :library "subtile"
      }
      159 {
        :x 9
        :index [ 8 11 10 13 ]
        :id 135
        :type "road"
        :y 15
        :l 1
        :library "subtile"
      }
      190 {
        :x 0
        :index [ 5 8 7 10 ]
        :id 135
        :type "road"
        :y 19
        :l 1
        :library "subtile"
      }
      111 {
        :x 1
        :y 11
        :id 135
        :type "road"
        :index [ 8 8 10 10 ]
        :l 1
        :library "subtile"
      }
      191 {
        :x 1
        :index [ 8 8 10 10 ]
        :id 135
        :type "road"
        :y 19
        :l 1
        :library "subtile"
      }
      158 {
        :x 8
        :index [ 8 8 10 10 ]
        :id 135
        :type "road"
        :y 15
        :l 1
        :library "subtile"
      }
      112 {
        :x 2
        :y 11
        :id 135
        :type "road"
        :index [ 8 8 3 9 ]
        :l 1
        :library "subtile"
      }
      157 {
        :x 7
        :index [ 5 8 7 10 ]
        :id 135
        :type "road"
        :y 15
        :l 1
        :library "subtile"
      }
      133 {
        :x 3
        :y 13
        :id 135
        :type "road"
        :index [ 6 9 7 10 ]
        :l 1
        :library "subtile"
      }
      113 {
        :x 3
        :y 11
        :id 135
        :type "road"
        :index [ 8 8 9 9 ]
        :l 1
        :library "subtile"
      }
    }
    2 {
      84 {
        :y 8
        :x 4
        :w 4
        :id 135
        :type "big-tree"
        :h 6
        :l 2
        :library "tile"
      }
    }
    3 {}
    4 {}
    -1 {}
  }
  :id 135
  :height 25
  :tilesize 16
}
