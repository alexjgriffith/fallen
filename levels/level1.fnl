{
  :tilesize 16  
  :height 25
  :width 10
  :data {
    0 {
      70 {
        :id 135
        :x 0
        :y 7
        :w 10
        :type "sun"
        :h 8
        :l 0
        :library "tile"
      }
      92 {
        :id 135
        :x 2
        :y 9
        :w 3
        :type "cloud"
        :h 2
        :l 0
        :library "tile"
      }
    }
    1 {
      187 {
        :x 7
        :index [ 6 12 6 12 ]
        :id 135
        :type "road"
        :y 18
        :l 1
        :library "subtile"
      }
      219 {
        :x 9
        :index [ 6 2 7 10 ]
        :id 135
        :type "road"
        :y 21
        :l 1
        :library "subtile"
      }
      134 {
        :x 4
        :index [ 9 12 10 13 ]
        :id 135
        :type "road"
        :y 13
        :l 1
        :library "subtile"
      }
      198 {
        :x 8
        :index [ 8 11 3 12 ]
        :id 135
        :type "road"
        :y 19
        :l 1
        :library "subtile"
      }
      115 {
        :x 5
        :index [ 8 11 10 13 ]
        :id 135
        :type "road"
        :y 11
        :l 1
        :library "subtile"
      }
      212 {
        :x 2
        :index [ 4 12 10 13 ]
        :id 135
        :type "road"
        :y 21
        :l 1
        :library "subtile"
      }
      124 {
        :x 4
        :index [ 9 12 9 12 ]
        :id 135
        :type "road"
        :y 12
        :l 1
        :library "subtile"
      }
      197 {
        :x 7
        :index [ 6 2 7 10 ]
        :id 135
        :type "road"
        :y 19
        :l 1
        :library "subtile"
      }
      133 {
        :x 3
        :index [ 6 9 7 10 ]
        :id 135
        :type "road"
        :y 13
        :l 1
        :library "subtile"
      }
      209 {
        :x 9
        :index [ 8 11 3 12 ]
        :id 135
        :type "road"
        :y 20
        :l 1
        :library "subtile"
      }
      178 {
        :x 8
        :index [ 8 11 10 13 ]
        :id 135
        :type "road"
        :y 17
        :l 1
        :library "subtile"
      }
      208 {
        :x 8
        :index [ 6 2 7 10 ]
        :id 135
        :type "road"
        :y 20
        :l 1
        :library "subtile"
      }
      211 {
        :x 1
        :index [ 8 8 10 10 ]
        :id 135
        :type "road"
        :y 21
        :l 1
        :library "subtile"
      }
      210 {
        :x 0
        :index [ 8 8 10 10 ]
        :id 135
        :type "road"
        :y 21
        :l 1
        :library "subtile"
      }
      177 {
        :x 7
        :index [ 5 8 6 1 ]
        :id 135
        :type "road"
        :y 17
        :l 1
        :library "subtile"
      }
      111 {
        :x 1
        :index [ 5 8 7 10 ]
        :id 135
        :type "road"
        :y 11
        :l 1
        :library "subtile"
      }
      123 {
        :x 3
        :index [ 9 9 3 9 ]
        :id 135
        :type "road"
        :y 12
        :l 1
        :library "subtile"
      }
      122 {
        :x 2
        :index [ 6 9 7 10 ]
        :id 135
        :type "road"
        :y 12
        :l 1
        :library "subtile"
      }
      112 {
        :x 2
        :index [ 8 8 3 9 ]
        :id 135
        :type "road"
        :y 11
        :l 1
        :library "subtile"
      }
      114 {
        :x 4
        :index [ 8 8 9 1 ]
        :id 135
        :type "road"
        :y 11
        :l 1
        :library "subtile"
      }
      202 {
        :x 2
        :index [ 5 11 6 12 ]
        :id 135
        :type "road"
        :y 20
        :l 1
        :library "subtile"
      }
      113 {
        :x 3
        :index [ 8 8 9 9 ]
        :id 135
        :type "road"
        :y 11
        :l 1
        :library "subtile"
      }
    }
    2 {
      84 {
        :id 135
        :x 4
        :y 8
        :w 4
        :type "big-tree"
        :h 6
        :l 2
        :library "tile"
      }
    }
    3 {}
    4 {}
    -1 {}
  }
  :id 135
}
