{
  :width 57
  :data {
    :for1 {}
    :for2 {}
    :sun {
      58 {
        :y 1
        :x 1
        :w 10
        :id 0
        :type "sun"
        :h 8
        :l "sun"
        :library "tile"
      }
    }
    :objs {
      1043 {
        :y 18
        :x 17
        :w 2
        :id 0
        :type "tree"
        :h 4
        :l "objs"
        :library "tile"
      }
      651 {
        :y 11
        :x 24
        :w 2
        :id 0
        :type "tree"
        :h 4
        :l "objs"
        :library "tile"
      }
      499 {
        :y 8
        :x 43
        :w 2
        :id 0
        :type "tree"
        :h 4
        :l "objs"
        :library "tile"
      }
      249 {
        :y 4
        :x 21
        :w 4
        :id 0
        :type "big-tree"
        :h 6
        :l "objs"
        :library "tile"
      }
      803 {
        :y 14
        :x 5
        :w 4
        :id 0
        :type "big-tree"
        :h 6
        :l "objs"
        :library "tile"
      }
    }
    :clouds {
      235 {
        :y 4
        :x 7
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      689 {
        :y 12
        :x 5
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      808 {
        :y 14
        :x 10
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      151 {
        :y 2
        :x 37
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      800 {
        :y 14
        :x 2
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      303 {
        :y 5
        :x 18
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      214 {
        :y 3
        :x 43
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      77 {
        :y 1
        :x 20
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      429 {
        :y 7
        :x 30
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      298 {
        :y 5
        :x 13
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      144 {
        :y 2
        :x 30
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1317 {
        :y 23
        :x 6
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      645 {
        :y 11
        :x 18
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      188 {
        :y 3
        :x 17
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      887 {
        :y 15
        :x 32
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1772 {
        :y 31
        :x 5
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      91 {
        :y 1
        :x 34
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      229 {
        :y 4
        :x 1
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      123 {
        :y 2
        :x 9
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1504 {
        :y 26
        :x 22
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      458 {
        :y 8
        :x 2
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      164 {
        :y 2
        :x 50
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      983 {
        :y 17
        :x 14
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1166 {
        :y 20
        :x 26
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      84 {
        :y 1
        :x 27
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      118 {
        :y 2
        :x 4
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      990 {
        :y 17
        :x 21
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      128 {
        :y 2
        :x 14
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      528 {
        :y 9
        :x 15
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      391 {
        :y 6
        :x 49
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      255 {
        :y 4
        :x 27
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      195 {
        :y 3
        :x 24
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1574 {
        :y 27
        :x 35
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
      1605 {
        :y 28
        :x 9
        :w 3
        :id 0
        :type "cloud"
        :h 2
        :l "clouds"
        :library "tile"
      }
    }
    :ground {
      1148 {
        :x 8
        :y 20
        :id 0
        :type "road-ice"
        :index [ 6 12 7 13 ]
        :l "ground"
        :library "subtile"
      }
      1090 {
        :x 7
        :y 19
        :id 0
        :type "road-ice"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      823 {
        :x 25
        :y 14
        :id 0
        :type "road-ice"
        :index [ 6 9 6 9 ]
        :l "ground"
        :library "subtile"
      }
      979 {
        :x 10
        :y 17
        :id 0
        :type "road-ice"
        :index [ 8 11 10 13 ]
        :l "ground"
        :library "subtile"
      }
      825 {
        :x 27
        :y 14
        :id 0
        :type "road-ice"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      977 {
        :x 8
        :y 17
        :id 0
        :type "road-ice"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1035 {
        :x 9
        :y 18
        :id 0
        :type "road-ice"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      975 {
        :x 6
        :y 17
        :id 0
        :type "road-ice"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      974 {
        :x 5
        :y 17
        :id 0
        :type "road-ice"
        :index [ 8 8 3 9 ]
        :l "ground"
        :library "subtile"
      }
      973 {
        :x 4
        :y 17
        :id 0
        :type "road-ice"
        :index [ 5 8 7 10 ]
        :l "ground"
        :library "subtile"
      }
      954 {
        :x 42
        :y 16
        :id 0
        :type "road-ice"
        :index [ 6 12 7 13 ]
        :l "ground"
        :library "subtile"
      }
      952 {
        :x 40
        :y 16
        :id 0
        :type "road-ice"
        :index [ 6 12 7 13 ]
        :l "ground"
        :library "subtile"
      }
      784 {
        :x 43
        :y 13
        :id 0
        :type "road-ice"
        :index [ 9 9 9 1 ]
        :l "ground"
        :library "subtile"
      }
      786 {
        :x 45
        :y 13
        :id 0
        :type "road-ice"
        :index [ 8 11 10 13 ]
        :l "ground"
        :library "subtile"
      }
      727 {
        :x 43
        :y 12
        :id 0
        :type "road-ice"
        :index [ 9 2 9 9 ]
        :l "ground"
        :library "subtile"
      }
      897 {
        :x 42
        :y 15
        :id 0
        :type "road-ice"
        :index [ 6 12 6 12 ]
        :l "ground"
        :library "subtile"
      }
      1032 {
        :x 6
        :y 18
        :id 0
        :type "road-ice"
        :index [ 9 9 10 10 ]
        :l "ground"
        :library "subtile"
      }
      778 {
        :x 37
        :y 13
        :id 0
        :type "road-ice"
        :index [ 8 8 3 9 ]
        :l "ground"
        :library "subtile"
      }
      780 {
        :x 39
        :y 13
        :id 0
        :type "road-ice"
        :index [ 4 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      782 {
        :x 41
        :y 13
        :id 0
        :type "road-ice"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      895 {
        :x 40
        :y 15
        :id 0
        :type "road-ice"
        :index [ 9 12 3 12 ]
        :l "ground"
        :library "subtile"
      }
      768 {
        :x 27
        :y 13
        :id 0
        :type "road-ice"
        :index [ 8 8 9 1 ]
        :l "ground"
        :library "subtile"
      }
      881 {
        :x 26
        :y 15
        :id 0
        :type "road-ice"
        :index [ 9 12 3 12 ]
        :l "ground"
        :library "subtile"
      }
      841 {
        :x 43
        :y 14
        :id 0
        :type "road-ice"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      1154 {
        :x 14
        :y 20
        :id 0
        :type "road-ice"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      766 {
        :x 25
        :y 13
        :id 0
        :type "road-ice"
        :index [ 8 8 3 9 ]
        :l "ground"
        :library "subtile"
      }
      764 {
        :x 23
        :y 13
        :id 0
        :type "road-ice"
        :index [ 5 8 7 10 ]
        :l "ground"
        :library "subtile"
      }
      839 {
        :x 41
        :y 14
        :id 0
        :type "road-ice"
        :index [ 9 9 10 10 ]
        :l "ground"
        :library "subtile"
      }
      838 {
        :x 40
        :y 14
        :id 0
        :type "road-ice"
        :index [ 9 9 9 1 ]
        :l "ground"
        :library "subtile"
      }
      758 {
        :x 17
        :y 13
        :id 0
        :type "road-ice"
        :index [ 6 12 7 13 ]
        :l "ground"
        :library "subtile"
      }
      1269 {
        :x 15
        :y 22
        :id 0
        :type "road-ice"
        :index [ 9 12 3 12 ]
        :l "ground"
        :library "subtile"
      }
      526 {
        :x 13
        :y 9
        :id 0
        :type "road-ice"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      528 {
        :x 15
        :y 9
        :id 0
        :type "road-ice"
        :index [ 6 9 6 9 ]
        :l "ground"
        :library "subtile"
      }
      530 {
        :x 17
        :y 9
        :id 0
        :type "road-ice"
        :index [ 9 9 9 1 ]
        :l "ground"
        :library "subtile"
      }
      837 {
        :x 39
        :y 14
        :id 0
        :type "road-ice"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      836 {
        :x 38
        :y 14
        :id 0
        :type "road-ice"
        :index [ 9 9 10 10 ]
        :l "ground"
        :library "subtile"
      }
      835 {
        :x 37
        :y 14
        :id 0
        :type "road-ice"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      978 {
        :x 9
        :y 17
        :id 0
        :type "road-ice"
        :index [ 8 8 9 1 ]
        :l "ground"
        :library "subtile"
      }
      976 {
        :x 7
        :y 17
        :id 0
        :type "road-ice"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      785 {
        :x 44
        :y 13
        :id 0
        :type "road-ice"
        :index [ 9 2 10 10 ]
        :l "ground"
        :library "subtile"
      }
      728 {
        :x 44
        :y 12
        :id 0
        :type "road-ice"
        :index [ 8 11 9 12 ]
        :l "ground"
        :library "subtile"
      }
      783 {
        :x 42
        :y 13
        :id 0
        :type "road-ice"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1157 {
        :x 17
        :y 20
        :id 0
        :type "road-ice"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      1153 {
        :x 13
        :y 20
        :id 0
        :type "road-ice"
        :index [ 8 8 3 9 ]
        :l "ground"
        :library "subtile"
      }
      1213 {
        :x 16
        :y 21
        :id 0
        :type "road-ice"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      1211 {
        :x 14
        :y 21
        :id 0
        :type "road-ice"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      724 {
        :x 40
        :y 12
        :id 0
        :type "road-ice"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      781 {
        :x 40
        :y 13
        :id 0
        :type "road-ice"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      779 {
        :x 38
        :y 13
        :id 0
        :type "road-ice"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      412 {
        :x 13
        :y 7
        :id 0
        :type "road-ice"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      413 {
        :x 14
        :y 7
        :id 0
        :type "road-ice"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      414 {
        :x 15
        :y 7
        :id 0
        :type "road-ice"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      415 {
        :x 16
        :y 7
        :id 0
        :type "road-ice"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      777 {
        :x 36
        :y 13
        :id 0
        :type "road-ice"
        :index [ 5 8 7 10 ]
        :l "ground"
        :library "subtile"
      }
      409 {
        :x 10
        :y 7
        :id 0
        :type "road-ice"
        :index [ 5 8 7 10 ]
        :l "ground"
        :library "subtile"
      }
      410 {
        :x 11
        :y 7
        :id 0
        :type "road-ice"
        :index [ 8 8 3 9 ]
        :l "ground"
        :library "subtile"
      }
      411 {
        :x 12
        :y 7
        :id 0
        :type "road-ice"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      419 {
        :x 20
        :y 7
        :id 0
        :type "road-ice"
        :index [ 8 8 9 1 ]
        :l "ground"
        :library "subtile"
      }
      418 {
        :x 19
        :y 7
        :id 0
        :type "road-ice"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1091 {
        :x 8
        :y 19
        :id 0
        :type "road-ice"
        :index [ 9 12 3 12 ]
        :l "ground"
        :library "subtile"
      }
      416 {
        :x 17
        :y 7
        :id 0
        :type "road-ice"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      769 {
        :x 28
        :y 13
        :id 0
        :type "road-ice"
        :index [ 8 11 10 13 ]
        :l "ground"
        :library "subtile"
      }
      422 {
        :x 23
        :y 7
        :id 0
        :type "road-ice"
        :index [ 8 11 10 13 ]
        :l "ground"
        :library "subtile"
      }
      421 {
        :x 22
        :y 7
        :id 0
        :type "road-ice"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      420 {
        :x 21
        :y 7
        :id 0
        :type "road-ice"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      767 {
        :x 26
        :y 13
        :id 0
        :type "road-ice"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      765 {
        :x 24
        :y 13
        :id 0
        :type "road-ice"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      938 {
        :x 26
        :y 16
        :id 0
        :type "road-ice"
        :index [ 6 12 7 13 ]
        :l "ground"
        :library "subtile"
      }
      726 {
        :x 42
        :y 12
        :id 0
        :type "road-ice"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      725 {
        :x 41
        :y 12
        :id 0
        :type "road-ice"
        :index [ 4 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      668 {
        :x 41
        :y 11
        :id 0
        :type "road-ice"
        :index [ 5 8 6 9 ]
        :l "ground"
        :library "subtile"
      }
      670 {
        :x 43
        :y 11
        :id 0
        :type "road-ice"
        :index [ 4 12 9 12 ]
        :l "ground"
        :library "subtile"
      }
      723 {
        :x 39
        :y 12
        :id 0
        :type "road-ice"
        :index [ 5 8 6 9 ]
        :l "ground"
        :library "subtile"
      }
      701 {
        :x 17
        :y 12
        :id 0
        :type "road-ice"
        :index [ 6 12 6 12 ]
        :l "ground"
        :library "subtile"
      }
      669 {
        :x 42
        :y 11
        :id 0
        :type "road-ice"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1031 {
        :x 5
        :y 18
        :id 0
        :type "road-ice"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      1033 {
        :x 7
        :y 18
        :id 0
        :type "road-ice"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      1326 {
        :x 15
        :y 23
        :id 0
        :type "road-ice"
        :index [ 6 12 7 13 ]
        :l "ground"
        :library "subtile"
      }
      600 {
        :x 30
        :y 10
        :id 0
        :type "road-ice"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      644 {
        :x 17
        :y 11
        :id 0
        :type "road-ice"
        :index [ 9 12 3 12 ]
        :l "ground"
        :library "subtile"
      }
      417 {
        :x 18
        :y 7
        :id 0
        :type "road-ice"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      601 {
        :x 31
        :y 10
        :id 0
        :type "road-ice"
        :index [ 8 11 10 13 ]
        :l "ground"
        :library "subtile"
      }
      643 {
        :x 16
        :y 11
        :id 0
        :type "road-ice"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      599 {
        :x 29
        :y 10
        :id 0
        :type "road-ice"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      598 {
        :x 28
        :y 10
        :id 0
        :type "road-ice"
        :index [ 5 8 7 10 ]
        :l "ground"
        :library "subtile"
      }
      880 {
        :x 25
        :y 15
        :id 0
        :type "road-ice"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      894 {
        :x 39
        :y 15
        :id 0
        :type "road-ice"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      587 {
        :x 17
        :y 10
        :id 0
        :type "road-ice"
        :index [ 9 12 9 12 ]
        :l "ground"
        :library "subtile"
      }
      586 {
        :x 16
        :y 10
        :id 0
        :type "road-ice"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      585 {
        :x 15
        :y 10
        :id 0
        :type "road-ice"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      1268 {
        :x 14
        :y 22
        :id 0
        :type "road-ice"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      582 {
        :x 12
        :y 10
        :id 0
        :type "road-ice"
        :index [ 6 12 7 13 ]
        :l "ground"
        :library "subtile"
      }
      531 {
        :x 18
        :y 9
        :id 0
        :type "road-ice"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
      529 {
        :x 16
        :y 9
        :id 0
        :type "road-ice"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      525 {
        :x 12
        :y 9
        :id 0
        :type "road-ice"
        :index [ 6 9 6 1 ]
        :l "ground"
        :library "subtile"
      }
      1034 {
        :x 8
        :y 18
        :id 0
        :type "road-ice"
        :index [ 9 9 9 1 ]
        :l "ground"
        :library "subtile"
      }
      1159 {
        :x 19
        :y 20
        :id 0
        :type "road-ice"
        :index [ 8 11 10 13 ]
        :l "ground"
        :library "subtile"
      }
      613 {
        :x 43
        :y 10
        :id 0
        :type "road-ice"
        :index [ 5 11 6 12 ]
        :l "ground"
        :library "subtile"
      }
      1155 {
        :x 15
        :y 20
        :id 0
        :type "road-ice"
        :index [ 8 8 9 9 ]
        :l "ground"
        :library "subtile"
      }
      1158 {
        :x 18
        :y 20
        :id 0
        :type "road-ice"
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
      }
      467 {
        :x 11
        :y 8
        :id 0
        :type "road-ice"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      470 {
        :x 14
        :y 8
        :id 0
        :type "road-ice"
        :index [ 9 9 10 10 ]
        :l "ground"
        :library "subtile"
      }
      1156 {
        :x 16
        :y 20
        :id 0
        :type "road-ice"
        :index [ 8 8 9 1 ]
        :l "ground"
        :library "subtile"
      }
      1152 {
        :x 12
        :y 20
        :id 0
        :type "road-ice"
        :index [ 5 8 7 10 ]
        :l "ground"
        :library "subtile"
      }
      1212 {
        :x 15
        :y 21
        :id 0
        :type "road-ice"
        :index [ 9 9 9 1 ]
        :l "ground"
        :library "subtile"
      }
      1210 {
        :x 13
        :y 21
        :id 0
        :type "road-ice"
        :index [ 6 9 7 10 ]
        :l "ground"
        :library "subtile"
      }
      471 {
        :x 15
        :y 8
        :id 0
        :type "road-ice"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      468 {
        :x 12
        :y 8
        :id 0
        :type "road-ice"
        :index [ 9 9 3 9 ]
        :l "ground"
        :library "subtile"
      }
      469 {
        :x 13
        :y 8
        :id 0
        :type "road-ice"
        :index [ 9 9 9 1 ]
        :l "ground"
        :library "subtile"
      }
      474 {
        :x 18
        :y 8
        :id 0
        :type "road-ice"
        :index [ 9 9 9 1 ]
        :l "ground"
        :library "subtile"
      }
      475 {
        :x 19
        :y 8
        :id 0
        :type "road-ice"
        :index [ 9 9 10 10 ]
        :l "ground"
        :library "subtile"
      }
      472 {
        :x 16
        :y 8
        :id 0
        :type "road-ice"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      473 {
        :x 17
        :y 8
        :id 0
        :type "road-ice"
        :index [ 9 9 9 9 ]
        :l "ground"
        :library "subtile"
      }
      840 {
        :x 42
        :y 14
        :id 0
        :type "road-ice"
        :index [ 9 9 3 1 ]
        :l "ground"
        :library "subtile"
      }
      824 {
        :x 26
        :y 14
        :id 0
        :type "road-ice"
        :index [ 9 9 9 1 ]
        :l "ground"
        :library "subtile"
      }
      476 {
        :x 20
        :y 8
        :id 0
        :type "road-ice"
        :index [ 9 12 10 13 ]
        :l "ground"
        :library "subtile"
      }
    }
  }
  :id 0
  :height 38
  :tilesize 16
}
