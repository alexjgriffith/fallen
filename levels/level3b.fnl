{
  :width 10
  :data {
    0 {}
    1 {
      122 {
        :x 2
        :y 12
        :id 135
        :type "road"
        :index [ 6 9 7 10 ]
        :l 1
        :library "subtile"
      }
      114 {
        :x 4
        :y 11
        :id 135
        :type "road"
        :index [ 8 8 9 1 ]
        :l 1
        :library "subtile"
      }
      134 {
        :x 4
        :y 13
        :id 135
        :type "road"
        :index [ 9 12 10 13 ]
        :l 1
        :library "subtile"
      }
      123 {
        :x 3
        :y 12
        :id 135
        :type "road"
        :index [ 9 9 3 9 ]
        :l 1
        :library "subtile"
      }
      115 {
        :x 5
        :y 11
        :id 135
        :type "road"
        :index [ 8 11 10 13 ]
        :l 1
        :library "subtile"
      }
      124 {
        :x 4
        :index [ 9 12 9 12 ]
        :id 135
        :type "road"
        :y 12
        :l 1
        :library "subtile"
      }
      194 {
        :x 4
        :y 19
        :id 135
        :type "road"
        :index [ 8 11 10 13 ]
        :l 1
        :library "subtile"
      }
      192 {
        :x 2
        :y 19
        :id 135
        :type "road"
        :index [ 8 8 10 10 ]
        :l 1
        :library "subtile"
      }
      193 {
        :x 3
        :y 19
        :id 135
        :type "road"
        :index [ 8 8 10 10 ]
        :l 1
        :library "subtile"
      }
      110 {
        :x 0
        :y 11
        :id 135
        :type "road"
        :index [ 5 8 7 10 ]
        :l 1
        :library "subtile"
      }
      143 {
        :x 3
        :index [ 6 12 7 13 ]
        :id 135
        :type "road"
        :y 14
        :l 1
        :library "subtile"
      }
      159 {
        :x 9
        :y 15
        :id 135
        :type "road"
        :index [ 8 11 10 13 ]
        :l 1
        :library "subtile"
      }
      190 {
        :x 0
        :y 19
        :id 135
        :type "road"
        :index [ 5 8 7 10 ]
        :l 1
        :library "subtile"
      }
      111 {
        :x 1
        :y 11
        :id 135
        :type "road"
        :index [ 8 8 10 10 ]
        :l 1
        :library "subtile"
      }
      191 {
        :x 1
        :y 19
        :id 135
        :type "road"
        :index [ 8 8 10 10 ]
        :l 1
        :library "subtile"
      }
      158 {
        :x 8
        :y 15
        :id 135
        :type "road"
        :index [ 8 8 10 10 ]
        :l 1
        :library "subtile"
      }
      112 {
        :x 2
        :y 11
        :id 135
        :type "road"
        :index [ 8 8 3 9 ]
        :l 1
        :library "subtile"
      }
      157 {
        :x 7
        :y 15
        :id 135
        :type "road"
        :index [ 5 8 7 10 ]
        :l 1
        :library "subtile"
      }
      133 {
        :x 3
        :y 13
        :id 135
        :type "road"
        :index [ 6 9 6 1 ]
        :l 1
        :library "subtile"
      }
      113 {
        :x 3
        :y 11
        :id 135
        :type "road"
        :index [ 8 8 9 9 ]
        :l 1
        :library "subtile"
      }
    }
    2 {
      172 {
        :id 135
        :x 2
        :y 17
        :w 2
        :type "tree"
        :h 4
        :l 2
        :library "tile"
      }
      84 {
        :y 8
        :x 4
        :w 4
        :id 135
        :type "big-tree"
        :h 6
        :l 2
        :library "tile"
      }
    }
    3 {}
    4 {}
    -1 {}
  }
  :id 135
  :height 25
  :tilesize 16
}
