(fn map-init [pfbs map level]
    ;; Load data from map
    (tset state.game :mapin (require  level))
    (local map-width state.game.mapin.width)
    (local map-height state.game.mapin.height)
    (local tileSize state.game.mapin.tilesize)
    ;; Define the number of tiles that can be displayed at once
    ;; Optimization for later to cut down on the number of requried draw calls
    ;; (tset state.game :tilesDisplayWidth
    ;;       (math.ceil (* state.camera.scale (/ display-width state.game.tileSize))))
    ;; (tset state.game :tilesDisplayHeight
    ;;       (math.ceil (* state.camera.scale (/ display-height state.game.tileSize))))
    ;; define batch sprites
    (tset state.game :tilesetBatch
        (love.graphics.newSpriteBatch  pfbs.sample-sprite-sheet (* map-width map-height tileSize)))
    (tset state.game :clouds
          (love.graphics.newSpriteBatch  pfbs.sample-sprite-sheet (* map-width map-height tileSize)))
    (tset state.game :sun (love.graphics.newSpriteBatch  pfbs.sample-sprite-sheet 2))
    ;; update batch sprites with data from mapin
  (map.updateTilesetBatch state.game.tilesetBatch pfbs.tiles state.game.mapin tileSize [:ground :objs :for1])
  (map.updateTilesetBatch state.game.sun pfbs.tiles state.game.mapin tileSize [:sun])
  (map.updateTilesetBatch state.game.clouds pfbs.tiles state.game.mapin tileSize [:clouds]))

(fn collision-init [bump-plugin bump attack]
    (tset state.game :world (bump.newWorld 64))
    (bump-plugin.add-layer state.game.world state.game.collidables
                           state.game.mapin :ground 0)
    (attack.activate "attack-resolution"))

(fn player-init [pfbs attack]
    ;; NPC Farmer
    ;; (set state.player (pfbs.player (* 0.3 180) (* 3 120) state.game.world
    ;;                                state.game.collidables attack)
    ;;      )
    ;; Knight
    ;; (set state.player (pfbs.player (* 4.5 180) (* 7 120) state.game.world
    ;;                                state.game.collidables attack)
    ;;      )

    ;; NPC 1
    ;; (set state.player (pfbs.player (* 2.5 180) (* 10 120) state.game.world
    ;;                                state.game.collidables attack))
    
    ;;; God Z
    ;; (set state.player (pfbs.player (* 2.5 180) (* 0 120) state.game.world
    ;;                                state.game.collidables attack)
    ;;      )
    ;; Rob O Satan
    ;; (set state.player (pfbs.player (* 2.5 180) (* 14 120) state.game.world
    ;;                                state.game.collidables attack))
    ;; Starting Position
    (set state.player (pfbs.player (* 2.5 180) (* 120 2.3666) state.game.world
                                  state.game.collidables attack))

    )

(fn godz-init [npc]
    (tset state.npcs :godz (npc.make "godz" {:x (/ 1860 5) :y (/ 30 5) :ox -10 :oy -19 :anim "assets/godz" :collision {:ox -1 :oy -4 :w 16 :h 28}
                            :hitbox {:type :godz :color [0.5 0 0.5 1]
                                           :w 12 :h 28 :ox 3 :oy 32}
                            :anim-index "Breath"})))


(fn robo-satan-init [npc]
    (tset state.npcs :robo-satan (npc.make "robo-satan" {:x (* 180 2.8) :y (/ 8000 5) :ox -100 :oy -19 :anim "assets/robo-satan" :collision {:ox -10 :oy -4 :w 8 :h 28}
                            :hitbox {:type :npc :npc "robo-satan" :color [0.5 0 0.5 1]
                                           :w 12 :h 28 :ox -3 :oy 0}
                            :health 15
                            :invincible true
                            :flip true
                            :anim-index "Breath"
                            :ai  "robo-satan-logic"
                            :timer 0
                            :beam-timer 0
                            :sd-timer 0
                            :sd-state :ready
                            :hostile false
                            :has-hit false
                            :auto-killed false
                            ;; [ 0 0.3 0.6 0.9 1.2 1.5 1.8 2.1 2.4 ]
                            :attacks {"Beam" 
                                      {:type :npc-atk :color [0 1 1 1]
                                             :npc "robo-satan"
                                             :start 0 :end 800 :w 200 :h 5
                                             :list [{:time 0 :y 100 :x 10}
                                                    {:time 300 :y 23 :x 10}
                                                   {:time 400 :y 23 :x 30}
                                                   {:time 500 :y 23 :x 200}]}}
                            })))

(fn farmer-init [npc]
    (tset state.npcs :farmer (npc.make "farmer" {:x (* 180 0.5) :y (* 3 120) :ox -55  :oy -80 :anim "assets/Angry Farmer" :collision {:ox 0 :oy 0 :w 8 :h 8} :health 3 :hit false
     :hitbox {:type :npc :npc "farmer" :color [0.5 0 0.5 1]
                                           :w 8 :h 8 :ox -1 :oy -1}
     :ai  "farmer-logic"
     :punching false
     :punching-timer 1000
     :time 0
     
                            :flip false
                            :island-x 20
                            :island-w 110
                            :island-y 380
                            :anim-index "Breath"
                            :hostile false
                            :attacks {"Punch" 
                                      {:type :npc-atk :color [0 1 1 1]
                                             :npc "farmer"
                                 :start 400 :end 800 :w 10 :h 16
                                 :list [{:time 400 :y 0 :x 4}
                                        {:time 500 :y -6 :x 8}]}}
                            }))
    )


(fn knight-init [npc]
    (tset state.npcs :knight (npc.make "knight" {:x (* 4.0 180) :y (* 7 120) :ox -55  :oy -80 :anim "assets/Knight" :collision {:ox 0 :oy 0 :w 8 :h 8} :health 4 :hit false
     :hitbox {:type :npc :npc "knight" :color [0.5 0 0.5 1]
                                           :w 8 :h 8 :ox -1 :oy -1}
     :ai  "knight-logic"
     :punching false
     :punching-timer 1000
     :time 0     

                            :flip false
                            :island-x 640
                            :island-w 200
                            :island-y 880
                            :anim-index "Breath"
                            :hostile false
                            :attacks {"Punch" 
                                      {:type :npc-atk :color [0 1 1 1]
                                             :npc "knight"
                                 :start 400 :end 800 :w 10 :h 16
                                 :list [{:time 400 :y 0 :x 4}
                                        {:time 500 :y -6 :x 8}]}}
                            }))
    )

(fn npc1-init [npc] 
    (tset state.npcs :npc1 (npc.make "npc1" {:x (* 2.3 180) :y (* 10 120) :ox -55  :oy -80 :anim "assets/NPC1" :collision {:ox 0 :oy 0 :w 8 :h 8} :health 5 :hit false
     :hitbox {:type :npc :npc "npc1" :color [0.5 0 0.5 1]
                                           :w 8 :h 8 :ox -1 :oy -1}
     :ai  "npc1-logic"
     :punching false
     :punching-timer 1000
     :time 0     
                            :flip false
                            :island-x 314
                            :island-w 250
                            :island-y 1200
                            :anim-index "Breath"
                            :hostile false
                            :attacks {"Punch" 
                                      {:type :npc-atk :color [0 1 1 1]
                                             :npc "npc1"
                                 :start 400 :end 800 :w 10 :h 16
                                 :list [{:time 400 :y 0 :x 4}
                                        {:time 500 :y -6 :x 8}]}}
                            }))
    )


(fn npcs-init [npc]
    (godz-init npc)
    (robo-satan-init npc)
    (farmer-init npc)
    (knight-init npc)
    (npc1-init npc))

{:map-init map-init :collision-init collision-init :player-init player-init
           :npcs-init npcs-init}
