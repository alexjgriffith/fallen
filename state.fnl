(lambda reset [?state]
  (let [default
        {:npcs {}
          :stones {}
          :scene {:start-opening true}
          :frozen false
          :player nil
          :special {:satan-explode false :godz-explode false}
         :game {:first
                true
                :tilesDisplayWidth nil
                :tilesDisplayHeight nil
                :level nil
                :map-width nil
                :map-height nil
                :mapin {}
                :tilesetBatch nil
                :clouds nil
                :sun nil
                :collidables {}
                :world nil
                :attack nil}
         :camera {:x 0 :y 0 :x2 0.0 :y2 0.0 :scale 1 :speed 500 :off-x 300 :off-y 200
                     :opening {:? false :time 0 :time-max 3000 :x1 -1650 :x2 -1650
                                  :y1 (- (* 120 0.8 5))
                                  :y2 (- (* 120 1.5 5))} }
         :reset reset}]
    (when ?state        
        (each [i v (pairs default)]
          (tset ?state i v))
        )
    default))

(reset)
