(local bump-plugin (require "bump-plugin"))
;; (local bump (require "bump"))

(local map (require "map")) 
(local subtile (require "subtile"))
(local pfbs (require "pfbs")) ;; should not be needed here apart from callbacks


(local tileLayers [:sun :clouds :ground :objs :for1])

(var tileSize nil)
(var tilesDisplayWidth nil)
(var tilesDisplayHeight nil)
(var level nil)
(var map-width nil)
(var map-height nil)
(var mapin {})
(var tilesetBatch nil)
(var cwidth nil)
(var cheight nil)
(var hoverBatch nil)

(local camera {:x 0 :y 0 :x2 0.0 :y2 0.0 :scale 1 :speed 500})

(fn move [dt]
    (when (love.keyboard.isDown "a")
      (set camera.x2 (+ camera.x2 (* dt camera.speed (/ 1 camera.scale))))
       (set camera.x (math.ceil camera.x2)))
    (when (love.keyboard.isDown "d")
      (set camera.x2 (- camera.x2 (* dt camera.speed (/ 1 camera.scale))))
      (set camera.x (math.floor camera.x2)))
    (when (love.keyboard.isDown "w")
      (set camera.y2 (+ camera.y2 (* dt camera.speed (/ 1 camera.scale))))
      (set camera.y (math.floor camera.y2)))
    (when (love.keyboard.isDown "s")
      (set camera.y2 (- camera.y2 (* dt camera.speed (/ 1 camera.scale))))
      (set camera.y (math.floor camera.y2))))

(fn wheelmoved [x y]
    (local new-scale (math.min 5 (math.max 0.5 (+ camera.scale (/ y 10)))))
    ;; (local old-scale camera.scale)
    ;; (local [mouse-x mouse-y ][(love.mouse.getPosition)])
    ;; (local shift-x  (* camera.x (- 1 (/ new-scale old-scale))))
    ;; (local shift-y  (* camera.y (- 1 (/ new-scale old-scale))))
    ;; (set camera.xp shift-x)
    ;; (set camera.yp shift-y)
    ;; (set camera.x (math.floor shift-x))
    ;; (set camera.y (math.floor shift-y))

    (set camera.scale new-scale))

(fn make-brush [brush]
    (fn [map x y]
        (brush map x y)))

(fn draw-tile-hover []
    (local [i j] (map.xy-to-tile [(love.mouse.getPosition)]
                             tileSize camera map-width map-height))
    
    (local tile (math.floor (* tileSize 1)))
    (love.graphics.setColor 0 0 0)
    (love.graphics.rectangle "line" (* tile i ) (* tile  j ) tile tile))

(fn draw-prefab-hover[]
    (love.graphics.draw hoverBatch))

(fn update-prefab-hover [mapin x y]
    (var hover-map (map.new mapin.width mapin.height 1))
    (var brush (make-brush pfbs.editor.brush))
    (brush hover-map x y)
    (each [key tile (pairs (. hover-map.data :ground))]
          (tset hover-map :data :ground key :index
                (subtile.match  (map.get-neighbours tile.type hover-map tile.x tile.y :ground tilesDisplayWidth tilesDisplayHeight))))
        (each [key tile (pairs (. mapin.data :for1))]
              (tset mapin :data :for1 key :index
                    (subtile.match  (map.get-neighbours tile.type mapin tile.x tile.y :for1 tilesDisplayWidth tilesDisplayHeight))))
    (map.updateTilesetBatch hoverBatch pfbs.tiles hover-map tileSize tileLayers))

(fn editor-update [dt set-mode]
    (move dt)
    (when (love.mouse.isDown 1)
      (let [[xp yp] (map.xy-to-tile [(love.mouse.getPosition)] tileSize camera map-width map-height)
            x  xp
            y yp]
        (var brush (make-brush pfbs.editor.brush))
        (brush mapin x y)
        ;; this is critical!
        (each [key tile (pairs (. mapin.data :ground))]
              (tset mapin :data :ground key :index
                    (subtile.match  (map.get-neighbours tile.type mapin tile.x tile.y :ground tilesDisplayWidth tilesDisplayHeight))))
        )
    (each [key tile (pairs (. mapin.data :for1))]
              (tset mapin :data :for1 key :index
                    (subtile.match  (map.get-neighbours tile.type mapin tile.x tile.y :for1 tilesDisplayWidth tilesDisplayHeight))))
    (map.updateTilesetBatch tilesetBatch pfbs.tiles mapin tileSize tileLayers)    
    )
    
    (let [[x y] (map.xy-to-tile [(love.mouse.getPosition)] tileSize camera map-width map-height)]
      (update-prefab-hover mapin x y)))

(fn editor-draw [message canvas-width canvas-height]
    (love.graphics.draw tilesetBatch)
    (draw-prefab-hover)
    (draw-tile-hover)  
    camera)

(fn activate [g-scale cmap-width cmap-height level]
    (set camera.scale g-scale)
    (set tileSize (* pfbs.grid-size 2))
    (set tilesDisplayWidth (math.ceil (* camera.scale (/ cmap-width tileSize))))
    (set tilesDisplayHeight (math.ceil (* camera.scale (/ cmap-height tileSize))))
    ;;(set level 3)
    (set cwidth cmap-width)
    (set cheight cmap-height)
    (set map-width tilesDisplayWidth)
    (set map-height tilesDisplayHeight)
    (set tilesetBatch (love.graphics.newSpriteBatch  pfbs.sample-sprite-sheet (* tilesDisplayWidth  tilesDisplayHeight )))
    (if level
      (do (set mapin (require (.. "levels." level)))
          (set map-width mapin.width)
        (set map-height mapin.height)
        (set cwidth (* tileSize map-width))
        (set cheight (* tileSize map-height)))
      (set mapin (map.new map-width map-height 1 tileSize))
      )
    (set hoverBatch(love.graphics.newSpriteBatch  pfbs.sample-sprite-sheet 100))
    (global g_map mapin)
    (map.updateTilesetBatch tilesetBatch pfbs.tiles mapin tileSize tileLayers))

(fn draw [_message _canvas-width _canvas-height]
    (pfbs.editor.background-draw cwidth cheight)
    (editor-draw "" cwidth cheight)
    (for [i 0 5]
         (for [j 0 14]
         (love.graphics.rectangle "line" (+ 0 (* i 180)) (+ 0 (* j 120)) 180 120)))

    camera)

(fn update [dt set-mode]    
    (editor-update dt set-mode))

(fn write-file [file content]
    (let [f (assert (io.open file,"wb"))
          c (: f :write ((require "lib.fennelview") content))]
      (: f :close)
      c))

(fn keypressed [key set-mode]
    (when (= key "space")
      (write-file "assets/temp.fnl" g_map)
      )
    (pfbs.editor.keypressed key set-mode))

{:activate activate           
 :draw draw
 :update update
 :wheelmoved wheelmoved
 :keypressed keypressed}
