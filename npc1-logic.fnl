(local attack (require "attack"))
(local ui (require "ui"))


(var chat false)
(var once true)
(var attacking false)
(var time 0)

(fn ctrl-atk [which action args?]
    (let [atk (. state.npcs.npc1.attacks which)]
      (if args?
          ((. atk action) atk  (unpack args?))
          (do ((. atk action) atk )))))

(fn foe [dt npc1 player])

(fn friend [dt npc1 player p30 p60 p90 pisland]
    (if (> (# p30) 0)
        (do
         (when (not chat)
           (set chat true)
           (ui.new-scene "chat-npc1"
                         ((require "ui-text-box") "Hi there, hope you're having a great day!" false))
           (ui.toggle-on "chat-npc1"))
         (tset npc1 :anim-index "Talk"))
        (> (# p60) 0)
        (do (set chat false)
            (ui.toggle-off "chat-npc1")
            (tset npc1 :anim-index "Wave"))
        (do
         (set chat false)
         (ui.toggle-off "chat-npc1")
         (tset npc1 :anim-index "Breath")
         (tset npc1 :x (- npc1.x (* 0 dt))))))

(fn chase-text [dt npc1 player p30 p60 p90 pisland]
    (set chat false)
    (if
     (> (# p60) 0)
     (do
         (when (not chat)
           (set chat true)
           (ui.new-scene "chat-npc1"
                         ((require "ui-text-box") "It's my precious!! You CANNOT have it!" false))
           (ui.toggle "chat-npc1")))
        (> (# p90) 0)
        (when (not chat)
          (set chat true)
          (ui.new-scene "chat-npc1"
                        ((require "ui-text-box") "Run, run now while you still have a chance." false))
          (ui.toggle "chat-npc1"))        
        (do (set chat false)
            (ui.toggle-off "chat-npc1")))
    )

(fn chase-on-island [dt npc1 player p30 p60 p90 pisland new-x x-dist dt1000]
    ;; (if (and (> new-x npc1.island-x)
    ;;          (< new-x (+ npc1.island-x npc1.island-w)))      
        (if (or (< x-dist 10) (< npc1.punching-timer 1000))
            (if npc1.punching
                (do
                 (when (> npc1.punching-timer 1000)
                   (set npc1.punching-timer 0)
                   (ctrl-atk "Punch" :begin [state.x state.y]))
                 (ctrl-atk "Punch" :update [npc1.x npc1.y npc1.flip dt])
                  (set npc1.punching-timer (+ npc1.punching-timer dt1000)))
                (do (set npc1.punching true)
                     (tset npc1 :anim-index "Punch")
                  (set npc1.punching-timer dt1000)
                  (ctrl-atk "Punch" :begin [state.x state.y])))
            (do
             (set npc1.punching false)
             (ctrl-atk "Punch" :finish)
             (tset npc1 :anim-index "Walk")
             (tset npc1 :x new-x)))
            
    
    );;)

(fn chase [dt npc1 player p30 p60 p90 pisland]
    (local dt1000 (* 1000 dt))
    (chase-text dt npc1 player p30 p60 p90 pisland)
    (var sign 1)
    (when (and (not npc1.punching) npc1.alive)
      (if (< player.x npc1.x)
        (do (tset npc1 :flip true)
              (set sign -1))
        (tset npc1 :flip false)))
    (local x-dist (math.abs (- player.x npc1.x -2)))
    (local new-x (+ npc1.x (* dt 0.2 npc1.speed sign)))
    (if
     (> (# pisland) 0)
     (chase-on-island dt npc1 player p30 p60 p90 pisland new-x x-dist dt1000)
     (do
      (set npc1.punching false)
      (ctrl-atk "Punch" :finish)
       (if (and (> new-x npc1.island-x)
                (< new-x (+ npc1.island-x npc1.island-w)))
           (do (tset npc1 :x new-x)
               (tset npc1 :anim-index "Walk"))
           (tset npc1 :anim-index "Breath")))))

(fn [dt]
    (set time (+ time dt))
    (local npc1 state.npcs.npc1)
    (local player state.player.state)
    (tset npc1 :ox -60)
    (set chat false)
    (ui.toggle-off "chat-npc1")
    (local [p60 len][(: attack.world :queryRect
                           (- npc1.x 60)
                           (- npc1.y 60) 
                           120 120
                           (fn [item] (= item.type "player")))])
    
    (local [p30 len][(: attack.world :queryRect
                           (- npc1.x 30)
                           (- npc1.y 30) 
                           60 60
                           (fn [item] (= item.type "player")))])

    (local [p90 len][(: attack.world :queryRect
                        (- npc1.x 85)
                        (- npc1.y 85) 
                        170 170
                        (fn [item] (= item.type "player")))])

    (local [pisland len]
           [(: attack.world :queryRect
               npc1.island-x
               npc1.island-y
               npc1.island-w
               200
               (fn [item] (= item.type "player")))])
    (if
     (not npc1.alive)
     (do (tset npc1 :exists false)
         (when  (: state.game.world :hasItem npc1)
         (: state.game.world :remove npc1))
         (tset npc1 :anim-index "Dead"))
     npc1.hostile
        (chase dt npc1 player p30 p60 p90 pisland)
        (friend dt npc1 player p30 p60 p90 pisland))
    )
