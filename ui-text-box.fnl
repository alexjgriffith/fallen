
(local image (love.graphics.newImage (.. "assets/" "ui.png")))

(local sound (require "sound"))

(fn grid-to-quad [obj ?times]
    (local grid-size (or ?times 1))
    (love.graphics.newQuad
     (* grid-size obj.x)
     (* grid-size obj.y)
     (* grid-size obj.w)
     (* grid-size obj.h)     
     (: image :getWidth) (: image :getHeight)))




(fn click-callback [element] (pp (.. "Element " element.name " was clicked.")))

(fn resume-callback [element dt]
    (tset element :set-scene-inactive true))

(fn draw-text [text element]
    (love.graphics.setColor (/ 132 256) (/ 126 256) (/ 133 256) 1)
    (love.graphics.setFont font.text-font)
    (love.graphics.printf text (+ element.x 5) (+ element.y 5)  element.w "left" 0 0.2 0.2)
    (love.graphics.setColor 1 1 1 1))

(fn escape-draw [text element]
    (local step 1200)
    (local  quad
            (if (< element.time step)
                (grid-to-quad {:x 19 :y 5 :w 3 :h 2} 8)
                (< element.time (* 1.3 step))
                (grid-to-quad {:x 10 :y 3 :w 2 :h 2} 8)
                (< element.time (* 2.3 step))
                (grid-to-quad {:x 12 :y 3 :w 2 :h 2} 8)
                (< element.time (* 2.6 step))
                (grid-to-quad {:x 10 :y 3 :w 2 :h 2} 8)
                (do (tset element :time 0)
                    (grid-to-quad {:x 12 :y 3 :w 2 :h 2} 8)
                  )))
    (love.graphics.draw image quad element.x element.y)
    (draw-text "ESC" element))


(fn text-box-draw [text element]
    (local quad (grid-to-quad {:x 0 :y 0 :w 12 :h 3} 8))
    (love.graphics.draw image quad element.x element.y)
    (draw-text text element))


;; Sample text: Returns a new function which works like its first
(local text-box {:x 45
                    :w (- (* 8 12 5) 40)
                   :y 4
                   :name "text-box"
                   :id 1
                   :click-callback click-callback
                   :default-draw (partial text-box-draw "")})


(local esc {:x 156
                   :w (* 8 2 5)
                   :y 2
                   :name "esc"
                   :id 1
                   :click-callback click-callback
                   :default-draw (partial escape-draw "")})


(fn [text active?]
    (tset text-box :default-draw (partial text-box-draw text))
    (local ret{:1 text-box})
    (when active? (tset ret 2 esc))
    ret)
