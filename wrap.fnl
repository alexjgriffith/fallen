(local sound (require "sound"))
(local repl (require "lib.stdio"))

;;((require "convert-json-to-lua"))

(global ctrl_keys {})
(global state (require "state"))


(global font {})
(set font.title-font (love.graphics.newFont "assets/avara.otf" 80))
(set font.normal-font (love.graphics.newFont "assets/avara.otf" 55))
(set font.very-large-font (love.graphics.newFont "assets/avara.otf" 150))
(set font.credits-font (love.graphics.newFont "assets/avara.otf" 30))
(set font.font-55 (love.graphics.newFont "assets/avara.otf" 70))
(set font.text-font (love.graphics.newFont "assets/avara.otf" 32))
(set font.final-font (love.graphics.newFont "assets/inconsolata.otf"  30))



(math.randomseed (os.time))
(local version "0.2.1")

(var mode-flag :game)
;; (var level "level5")

;;(var mode-flag :editor)
(var level "level5b")

(local canvas-width 900)
(local canvas-height 600)
;;(local canvas-height 2400)
(var map-width canvas-width)
(var map-height 2400)
(var scale 1)

;; set the first mode
(var mode {})
(fn set-mode [mode-name ...]
  (set mode (require mode-name))
  (when mode.activate
    (mode.activate ...)))

(var canvas (love.graphics.newCanvas canvas-width canvas-height))
(var ui (love.graphics.newCanvas canvas-width canvas-height))

(fn love.load []
    (: ui :setFilter "nearest" "nearest")
    (: canvas :setFilter "nearest" "nearest")
    (sound.play :opening)
    (repl.start))



(if (= mode-flag :game)
    (do
     ;;(pp "game")
     ;;(set-mode "final"))
     ;;(set-mode "game" scale canvas-width canvas-height level))
     (set-mode "opening" scale canvas-width canvas-height level))
    (do
     (set-mode "editor-ctrl" scale map-width map-height level)))

;;(love.graphics.setBackgroundColor 1 1 1 1 )


;;(set-mode "mode-intro" "sample-map-player" "sample-side-bar" "sample-info-bar")

(fn love.draw []
    (love.graphics.setColor 0 0 0)
    (love.graphics.rectangle "fill" 0 0 canvas-width canvas-height)
    (love.graphics.draw canvas 0 0 0)
    (love.graphics.setCanvas canvas)   
    (love.graphics.clear)
    (love.graphics.setColor 1 1 1)
    (let [camera (mode.draw "" canvas-width canvas-height)]
      (love.graphics.setCanvas)
      (love.graphics.setColor 1 1 1)
      (if (= mode-flag :game)
          (love.graphics.draw canvas)
          (love.graphics.draw canvas camera.x camera.y 0 camera.scale camera.scale))
    (love.graphics.setCanvas ui)
    (love.graphics.setColor 0 0 0 0)
    (love.graphics.clear)
    (love.graphics.setColor 1 0 0 1)
    ;;(love.graphics.print (.. "FPS: " (love.timer.getFPS)) 10 10)
    ;; (love.graphics.print (.. "X: " (math.floor state.player.state.x) " Y: " (math.floor state.player.state.y)) 10 580)
    (love.graphics.setCanvas)
    (love.graphics.draw ui 0 0)))

(fn love.update[dt]
    (mode.update dt set-mode))


(fn switch-to-arial []
    (set font.title-font (love.graphics.newFont "assets/avara.ttf" 80))
    (set font.normal-font (love.graphics.newFont "assets/avara.ttf" 55))
    (set font.very-large-font (love.graphics.newFont "assets/avara.ttf" 150))
    (set font.credits-font (love.graphics.newFont "assets/avara.ttf" 30))
    (set font.font-55 (love.graphics.newFont "assets/avara.ttf" 70))
    (set font.text-font (love.graphics.newFont "assets/avara.ttf" 32))
    (set font.final-font (love.graphics.newFont "assets/avara.ttf"  30)))
             

(fn love.keypressed [key]
    (if
     (and (love.keyboard.isDown "lctrl" "rctrl" "capslock") (= key "q"))
     (love.event.quit)
     (= key "f")
       (switch-to-arial)
     ;; add what each keypress should do in each mode
     (mode.keypressed key set-mode)
     ))
    

(fn love.resize [ w h ]
    (set canvas (love.graphics.newCanvas w h))
    (when mode.resize
      (mode.resize w h)))


(fn love.wheelmoved [x y]
    (when mode.wheelmoved
      (set scale (math.min 5 (math.max 0.5 (+ scale (/ y 10)))))
      (mode.wheelmoved x y)))
   
(fn love.keyreleased [key]
    (when mode.keyreleased
     ;; add what each keypress should do in each mode
     (mode.keyreleased key)))

(fn love.mousepressed [x y]
    (when mode.mousepressed
     ;; add what each keypress should do in each mode
     (mode.mousepressed x y)))

(fn love.mousemoved [x y]
    (when mode.mousemoved
     ;; add what each keypress should do in each mode
     (mode.mousemoved x y)))
