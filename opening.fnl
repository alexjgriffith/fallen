(local blue [(/ 134 256) (/ 206 256) (/ 236 256) 1])
(local black [(/ 34 256) (/ 32 256) (/ 52 256 ) 1])
(local grey [(/ 132 256) (/ 126 256) (/ 133 256) 1])

(local image (love.graphics.newImage (.. "assets/" "Gabe.png")))

(local cloud (love.graphics.newImage (.. "assets/" "all-tiles.png")))

(local ui (love.graphics.newImage (.. "assets/" "ui.png")))







(fn grid-to-quad [obj ?times ?image]
    (local grid-size (or ?times 1))
    (love.graphics.newQuad
     (* grid-size obj.x)
     (* grid-size obj.y)
     (* grid-size obj.w)
     (* grid-size obj.h)     
     (: (or ?image image) :getWidth) (: (or ?image image) :getHeight)))


(var gabe-timer 0)
(var gabe-index 0)
(fn toggle-gabe-index []
    (if (= gabe-index 0)
        (set gabe-index 1)
        (set gabe-index 0)))

(fn update-gabe [dt]
    (when (> gabe-timer 300)
      (toggle-gabe-index)
      (set gabe-timer 0))
    (set gabe-timer (+ gabe-timer (* dt 1000))))

(fn draw-gabe [x y]
    (var quad (grid-to-quad {:x 4 :y 0 :w 1 :h 1} 32))
    (when
     (= gabe-index 0)(set quad (grid-to-quad {:x 3 :y 0 :w 1 :h 1} 32)))
    (love.graphics.draw image quad x y))


(fn draw-cloud [x y]
    (var quad (grid-to-quad {:x 0 :y 9 :w 3 :h 2} 8 cloud))
    (love.graphics.draw cloud quad x y))




(fn draw-escape [x y]
    (local quad (grid-to-quad {:x 19 :y 5 :w 3 :h 2} 8 ui))
    (love.graphics.draw ui quad x y 0 10 10)
    (love.graphics.setFont font.font-55)
    (love.graphics.setColor (unpack grey))
    (love.graphics.print "ESC" (+ 45 x) (+ 50 y)))


(var title true)
(var title-trans -2)


(var credits true)
(var credits-trans -4)

(var instr true)
(var instr-trans -8)


(fn sigmoid [x xo k l]
    (/ l (+ 1 (math.exp (- (* k ( - x xo)))))))

(fn draw-title []
    (when title
      (love.graphics.setColor (/ 21 256) (/ 27 256) (/ 31 256) (sigmoid title-trans 1 1 1))
      (love.graphics.setFont font.very-large-font)
      (love.graphics.printf "FALLEN" -10 400 700 "center")
      (love.graphics.setColor 1 1 1 1))
    )


(fn draw-instr []
    (when instr
      (love.graphics.setColor (/ 21 256) (/ 27 256) (/ 31 256) (sigmoid instr-trans 1 1 1))
      (love.graphics.setFont font.credits-font)
      (love.graphics.printf "Press ESC to Continue" -10 280 700 "center")
      (love.graphics.printf "Play with W S A SPC" -10 320 700 "center")
      (love.graphics.setColor 1 1 1 1))
    )



(fn draw-credits []
    (when credits
      (love.graphics.setColor (/ 21 256) (/ 27 256) (/ 31 256) (sigmoid credits-trans 1 1 1))
      (love.graphics.setFont font.credits-font)
      (love.graphics.printf  "Music By:     Scott Buckley" 670 430 250 "left")
      (love.graphics.printf "A Game By Alexander Griffith" -10 540 700 "center")
      (love.graphics.setColor 1 1 1 1))
    )


;; sepearate this into a timer library that has a list of timers with draw update
;; and callback
(var fade-to-white-time-max 1000)
(var fade-to-white-time 0)
(var fade-to-white-? false)
(var fade-to-white-callback nil)

(fn fade-to-white [callback]
    (set fade-to-white-? true)
    (set fade-to-white-time 0)
    (set fade-to-white-callback callback))

(fn fade-to-white-update [dt]
    (when (> fade-to-white-time fade-to-white-time-max)
      (fade-to-white-callback))
    (when fade-to-white-?
      (set fade-to-white-time (+ fade-to-white-time (* 1000 dt)))))

(fn fade-to-white-draw []
    (local percent (/ fade-to-white-time fade-to-white-time-max))
    (love.graphics.setColor  1 1 1 percent)
    (love.graphics.rectangle "fill" 0 0 900 600)
    (love.graphics.setColor 1 1 1 1))


(fn draw [message canvas-width canvas-height]
    (local [?tx ?ty ?sx ?sy] [0 0 10 10])
    (love.graphics.setColor (unpack blue))
    (love.graphics.rectangle "fill" 0 0  canvas-width canvas-height)
    (love.graphics.setColor 1 1 1 1)
    (love.graphics.push)
    (love.graphics.translate (math.floor(or ?tx 0))
                             (math.floor(or ?ty 0)))
    (love.graphics.scale  (or ?sx 1) (or ?sy (or ?sx 1)))


    ;; (draw-cloud 4 8)
    ;; (draw-cloud 20 12)
    (draw-cloud 4 -1)
    (draw-cloud 20 5)    
    
    (draw-gabe 50 10)

    (love.graphics.pop)

    ;;(draw-escape 650 2)

    (draw-title)
    (draw-credits)
    (draw-instr)
    (fade-to-white-draw))


(fn update [dt]
    (when title
      (set title-trans (+ title-trans (math.min 1 (* 2 dt)))))
    (when credits
      (set credits-trans (+ credits-trans (math.min 1 (* 2 dt)))))
    (when instr
      (set instr-trans (+ instr-trans (math.min 1 (* 2 dt)))))    
    
    (update-gabe dt)
    (fade-to-white-update dt))

(var scale 5)
(var canvas-width 900)
(var canvas-height 600)
(var level "level5b")


(fn activate [inscale incanvas-width incanvas-height inlevel]
    (set scale inscale)
    (set canvas-width incanvas-width)
    (set canvas-height incanvas-height)
    (set level inlevel))

(fn keypressed [key set-mode]
    (when (= key "escape")
      (fade-to-white (fn [](set-mode "game" scale canvas-width canvas-height level)))))

{:draw draw :update update :keypressed keypressed :activate activate}
