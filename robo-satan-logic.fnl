(local attack (require "attack"))
(local ui (require "ui"))
(local restart (require "restart"))

(fn ctrl-atk [which action args?]
    (let [atk (. state.npcs.robo-satan.attacks which)]
      (if args?
          ((. atk action) atk  (unpack args?))
          (do ((. atk action) atk )))))

(var time 0)
(var chat false)

(local px1 366)
(local px0 490)

(local spoken? false)

(local dialogue-no-gears
       (lume.split (love.filesystem.read "text/scripts/satan-no-gears.txt") "\n"))

(local dialogue-with-gears
       (lume.split (love.filesystem.read "text/scripts/satan-with-gears.txt") "\n"))

(var once true)
(fn pp-once [str]
    (when once
      (set once false)
      (pp str)
      )
    )

(fn reset-anim [name]
    (let [animation (. state.npcs.robo-satan.anim.animations name)]
      (: animation :gotoFrame 1)))

(local dialogue-period 4000)
(var dialogue-time 0)
(var index 1)
(fn run-dialogue [dt satan player dialogue]
    (when (> dialogue-time dialogue-period)
      (set dialogue-time 0)
      (when (~= "" (. dialogue (+ 1 index))) (set index (+ index 1))))
    (when (. dialogue index)
      (ui.new-scene "chat-satan"
                    ((require "ui-text-box") (. dialogue index) false))
      (ui.toggle "chat-satan"))
    (set dialogue-time (+ dialogue-time (* dt 1000))))

(fn friend [dt satan player]
    (set satan.timer 0)
    (run-dialogue dt satan player (if satan.invincible dialogue-no-gears
                                      dialogue-with-gears)))


(fn player-explosion [dt player]
    (when player.explosion.?
      (set player.anim-index "Explode")
      (set player.explosion.time (+ player.explosion.time (* dt 1000)))))

(fn auto-kill [satan player]
    ;;(pp satan)
    (when (not satan.auto-killed)
      (set satan.auto-killed true)
      (set state.frozen true)
      (set player.health 0)
      (tset player :death-suspended true)
      (set state.player.state.anim.animations.Explode.onLoop 'pauseAtEnd')
      (set player.flip false)
      (set player.anim-index "Explode")
      (set player.explosion.? true)
      )
    ;; (when (> satan.timer 3800)
    ;;   (restart)
    ;;   )
    )

(fn explode-check [satan player]
    (if satan.invincible
        (auto-kill satan player)
         (when (and player.alive (not satan.has-hit))
           (tset player :hit true)
           (tset player :health (- state.player.state.health 2))
           (set satan.has-hit true))
        ))
;;(var satan.timer 0)

(fn self-distruct [dt satan player]
    ;; after 1200 set screen white for 2000
    ;;(tset state.special :satan-explode 10 10)
    (set satan.timer (+ satan.timer (* 1000 dt)))
    (set satan.sd-lock true)
    (if (or (= state.npcs.robo-satan.anim.animations.Explode.position 5)
             (= state.npcs.robo-satan.anim.animations.Explode.position 6))
        (do (tset state.special :satan-explode true)
            (explode-check satan player)
          (pp "exploding"))
        
        (do
         (tset state.special :satan-explode false)
         (set satan.has-hit false)))

    (if (< satan.timer 3800)
        (tset satan :anim-index "Explode")
        (do (tset satan :anim-index "Breath")
            (reset-anim "Explode")))
    (when (> satan.timer 7000)
      (set satan.timer 0)      
      (set satan.sd-lock false)))
    
    ;; (if (and (> satan.timer 1200)
    ;;          (not state.special.satan-explode)
    ;;          (= state.sd-state :ready))
    ;;     (do (tset state.special :satan-explode true)
    ;;         (set state.sd-state :white)
    ;;       (explode-check satan player))
        
    ;;     (and (>  satan.timer 3500) (= state.sd-state :white))
    ;;     (do
    ;;      (set satan.has-hit false)
    ;;      (tset state.special :satan-explode false)
    ;;      (set state.sd-state :normal))
        
    ;;     (and (>  satan.timer 3700) (= state.sd-state :normal))
    ;;     (do
    ;;      (set state.sd-state :ready)
    ;;      (set satan.timer 0)
    ;;       (reset-anim "Explode")
    ;;       (set satan.sd-lock false)))
    ;; (if (< satan.timer 3800)
    ;;     (tset satan :anim-index "Explode")
    ;;     (do (tset satan :anim-index "Breath")
    ;;         (set satan.timer 0)
    ;;         (reset-anim "Explode")))
 


(fn beam [dt satan player]
    (set satan.beam-lock true)
    (set satan.timer (+ satan.timer (* dt 1000)))
    (local index satan.anim.animations.Beam.position)
    (when (= index 3)
      (ctrl-atk "Beam" :begin)
      (when (not satan.beam-lock)
                        (ctrl-atk "Beam" :begin)
                        (pp "index 3")
                        (pp satan.attacks.Beam)
                        (pp (. state.npcs.robo-satan.attacks "Beam"))))
    (if (or (= index 5) (= index 4) (= index 6))
        (ctrl-atk "Beam" :update [satan.x satan.y satan.flip  dt])        
        (ctrl-atk "Beam" :finish))
    
    (if (or (= index 5) (= index 6))
        (do
         (set satan.beam-offset (+ satan.beam-offset (* 4000 dt)))
         (set satan.render-beam true))
        (do (set satan.beam-offset 0)
         (set satan.render-beam false)))
    (when (> satan.timer 7000)
      (set satan.timer 0)      
      (set satan.beam-lock false))
    (if (< satan.timer 2400)
        (tset satan :anim-index "Beam")        
         (tset satan :anim-index "Breath")))

(var callback nil)
(fn hostile [dt satan player]
    (set callback beam)
    (when (or (> player.x px0) satan.sd-lock)
      (set callback self-distruct ))
    (when satan.beam-lock
      (set callback beam))
    (when callback
      (callback dt satan player)))

(fn run-satan [dt satan player]
    ;;(pp satan.alive)
    (if
     (not satan.alive) (do (pp "dead")(tset satan :anim-index "Dead"))
     (not satan.hostile) (friend dt satan player)
     (and satan.hostile satan.invincible) (self-distruct dt satan player)
     satan.hostile (hostile dt satan player)
     ))

(var dead-timer 0)
(fn [dt]
    (when state.npcs.robo-satan.exists
      (set time (+ time dt))
      (local satan state.npcs.robo-satan)
      (local player state.player.state)
      (set chat false)
      (ui.toggle-off "chat-satan")    
      (when (< satan.health 1)
        (set dead-timer (+ dead-timer (* dt 1000)))
        (tset satan :alive false)
        (when (> dead-timer 900) 
          (when  (: state.game.world :hasItem state.npcs.robo-satan)
            (: state.game.world :remove state.npcs.robo-satan))
          (tset satan :exists false)
          (tset satan :renderable false)))
      ;;(pp satan.attacks)
      (when player.in-hell
        ;;(self-distruct dt satan player)      
        (run-satan dt satan player)
        )
    (player-explosion dt player))
    )
