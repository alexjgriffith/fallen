(local attack {})

(local bump (require "lib.bump"))

(local atk {:active {}})

(local atk-default {:type :default
                    :color [1 0 0 1]
                    :start 0
                    :end 0
                    :list []
                    :x 0
                    :y 0
                    :w 0
                    :h 0
                    :ox 0
                    :oy 0
                    :flipped false
                    :time 0
                    :added false})


(fn atk.begin [atk]
    (tset atk :time 0)
    (tset atk :has-hit false)
    (tset atk :added false))

(fn atk.hitbox-move [atk x y flipped]
    (tset atk :x  (if flipped (+ x atk.ox 2) (+ x atk.ox)) )
    (tset atk :y (+ y atk.oy))
    (when (not (: attack.world :hasItem atk))
               (: attack.world :add atk atk.x atk.y atk.w atk.h)
)
    (local [aX aY cols len][(: attack.world :move atk atk.x 
         atk.y (fn [item other] "cross"))])
    ((require attack.collision-resolution-file) atk cols len))

(fn atk.update [atk x y flipped dt]
    ;; need to add check for animation list
    (when (not (. atk :time))
      ;;(atk.begin atk)
      (pp atk)
      (pp "attack library did not reset")
      )
    (tset atk :time (+ (* 1000 dt) atk.time))
    (when (and (> atk.time  atk.start ) (not atk.added))        
      (tset atk :x x)
      (tset atk :y y)
      (tset atk :added true)
      (when (not (: attack.world :hasItem atk))
          (: attack.world :add atk atk.x atk.y atk.w atk.h)))
    (when (> atk.time  atk.end)
      (atk.finish atk))
    (when atk.added
      (var off-x 0)
      (var off-y 0)
      (each [_ off (ipairs atk.list)]
            (when (< off.time atk.time)
              (set off-x (if flipped (- off.x) (+ 0.5 off.x)))
              (set off-y off.y)))
      (local [aX aY cols len] [(: attack.world :move atk (+ x off-x) (+ y off-y) (fn [item other] "cross"))])
      (tset atk :x aX)
      (tset atk :y aY)
      ((require attack.collision-resolution-file) atk cols len)
      ))

(fn atk.finish [atk]
    ;;(tset atk :time 0)
    ;;(pp "removing attack")
    (when (: attack.world :hasItem atk)
      (tset atk :added false)
      (: attack.world :remove atk)))

(local atkmt {:__index atk})

(fn attack.activate [collision-resolution-file]
    (set attack.collision-resolution-file collision-resolution-file)
    (set attack.world (bump.newWorld 64)))

(fn attack.draw [?tx ?ty ?sx ?sy]
    (love.graphics.push "all") ;;pop translate scale
    (love.graphics.translate (math.floor(or ?tx 0))
                             (math.floor(or ?ty 0)))
    (love.graphics.scale  (or ?sx 1) (or ?sy (or ?sx 1)))
    (local collidables (: attack.world :getItems))
    (each [_ t (ipairs collidables)]
          (love.graphics.setColor (unpack t.color))
          (love.graphics.rectangle "line" t.x t.y t.w t.h))    
    (love.graphics.pop))

(fn attack.new [parameters]
    (local new-attack
           (setmetatable
            {:type (or parameters.type atk-default.type)
             :color (or parameters.color atk-default.color)
             :start (or parameters.start atk-default.start)
             :end (or parameters.end atk-default.end)
             :list (or parameters.list atk-default.list)
             :x (or parameters.x atk-default.x)
             :y (or parameters.y atk-default.y)
             :ox (or parameters.ox atk-default.ox)
             :oy (or parameters.oy atk-default.oy)              
             :w (or parameters.w atk-default.w)
             :h (or parameters.h atk-default.h)}
            atkmt))
    (each [key value (pairs parameters)]
          (var val value)
          (tset new-attack key val))
    new-attack)

attack
