;; from stl
;; local t = {
;; 	   name       = object.name,
;; 	   type       = object.type,
;; 	   x          = instance.x + map.offsetx + object.x,
;; 	   y          = instance.y + map.offsety + object.y,
;; 	   width      = object.width,
;; 	   height     = object.height,
;; 	   layer      = instance.layer,
;; 	   properties = object.properties
;; 	   }
;; world:add(t, t.x, t.y, t.width, t.height)

;;(local l1 (require "level1"))

(fn add-tile [world collidables id tile tilesize offset-x?]
    (let [offset-x (or offset-x? 0)
          t {:name id
              :type tile.type
              :w (* (or tile.w 1) tilesize)
              :h (* (or tile.h 1) tilesize)
              :x (+ (* tile.x tilesize) offset-x)
              :y (* tile.y tilesize)
              :l tile.l
              :id tile.id
              :library tile.library
              }]
      (: world :add t t.x t.y t.w t.h)
      (table.insert collidables t)))


(fn draw  [collidables world ?tx ?ty ?sx ?sy]
    (love.graphics.push)
    (love.graphics.translate (math.floor(or ?tx 0))
                             (math.floor(or ?ty 0)))
    (love.graphics.scale  (or ?sx 1) (or ?sy (or ?sx 1)))
    (each [_ collidable (pairs (: world :getItems))]
          (love.graphics.rectangle
           "line"
           (: world :getRect collidable)))
    (love.graphics.pop))


(fn add-layer [world collidables map layer offset-x]
    (each [id tile (pairs (. map :data layer))]
          (add-tile world collidables id tile map.tilesize offset-x)))

{:draw draw :add-layer add-layer :add-tile add-tile}
