(local image (love.graphics.newImage (.. "assets/" "all-tiles.png")))

(fn grid-to-quad [obj ?times]
    (local grid-size (or ?times 1))
    (love.graphics.newQuad
     (* grid-size obj.x)
     (* grid-size obj.y)
     (* grid-size obj.w)
     (* grid-size obj.h)     
     (: image :getWidth) (: image :getHeight)))


(fn empty-callback [_])


(fn heart-draw [element]
    (local full (grid-to-quad {:x 6 :y 0 :w 1 :h 1} 16))
    (local half (grid-to-quad {:x 7 :y 0 :w 1 :h 1} 16))
    (local empty (grid-to-quad {:x 8 :y 0 :w 1 :h 1} 16))
    (local health state.player.state.health)
    (for [i 0 2]
         (local x (+ 2(* i 12)))
         (if (> health (+ (* 2 i) 1)) (love.graphics.draw image full x 4)
             (> health (+ (* 2 i) )) (love.graphics.draw image half x 4)
             (love.graphics.draw image empty x 4))))

(fn ice-draw [element]
    (local quad (grid-to-quad {:x 11 :y 15 :w 1 :h 1} 8))
    (when state.player.state.ice-stone      
      (love.graphics.draw image quad (+ 1.2 14) 16)))

(fn green-draw [element]
    (local quad (grid-to-quad {:x 10 :y 15 :w 1 :h 1} 8))
    (when state.player.state.green-stone
      (love.graphics.draw image quad (+ 1.2 2) 16)))

(fn blood-draw [element]
    (local quad (grid-to-quad {:x 12 :y 15 :w 1 :h 1} 8))
    (when state.player.state.blood-stone
      (love.graphics.draw image quad (+ 1.2 26) 16)))


(local hearts     {:x 10
                      :y 10
                      :w 16
                      :h 16
                      :id 1
                      :name "hearts"
                      :click-callback empty-callback
                      :default-draw heart-draw})

(local ice     {:x -200
                      :y -200
                      :w 16
                      :h 16
                      :id 2
                      :name "ice"
                      :click-callback empty-callback
                      :default-draw ice-draw})

(local ice     {:x -200
                      :y -200
                      :w 16
                      :h 16
                      :id 2
                      :name "ice"
                      :click-callback empty-callback
                      :default-draw ice-draw})

(local green     {:x -200
                      :y -200
                      :w 16
                      :h 16
                      :id 3
                      :name "green"
                      :click-callback empty-callback
                      :default-draw green-draw})

(local blood     {:x -200
                      :y -200
                      :w 16
                      :h 16
                      :id 4
                      :name "blood"
                      :click-callback empty-callback
                      :default-draw blood-draw})







{:1 hearts
    :2 ice
    :3 green
    :4 blood
}
