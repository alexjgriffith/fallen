(local image (love.graphics.newImage (.. "assets/" "ui.png")))

(local restart (require "restart"))

(local sound (require "sound"))

(fn grid-to-quad [obj ?times]
    (local grid-size (or ?times 1))
    (love.graphics.newQuad
     (* grid-size obj.x)
     (* grid-size obj.y)
     (* grid-size obj.w)
     (* grid-size obj.h)     
     (: image :getWidth) (: image :getHeight)))


(fn empty-callback [_])

(fn click-callback [element] (pp (.. "Element " element.name " was clicked.")))

(fn quit-callback [element dt]
    (love.event.quit))

(fn restart-callback [element dt]
    (restart)
     )
    
(fn resume-callback [element dt]
    (tset element :set-scene-inactive true))

(fn title-draw [element]
    (love.graphics.setColor (/ 21 256) (/ 27 256) (/ 31 256) 1)
    (love.graphics.setColor (/ 132 256) (/ 126 256) (/ 133 256) 1)
    (love.graphics.setFont font.title-font)
    (love.graphics.printf "FALLEN" element.x element.y  element.w "center" 0 0.2 0.2)
    (love.graphics.setColor 1 1 1 1))

(fn background-draw [element]
    (local quad (grid-to-quad {:x 0 :y 5 :w 12 :h 15} 8))
    (love.graphics.draw image quad element.x element.y))


(fn element-timer-update [element dt]
    (tset element :hovered-count 0)
    (tset element :time (+ element.time (* 1000  dt))))

(fn button-hover-update [element dt]
    (when (= element.hovered-count 0)
      (local snd (.. "bell" (math.random 4)))
      (sound.rewind snd)
      (sound.play snd)
      ;;(pp snd)
      )
    (set element.hovered-count (+ element.hovered-count 1))
    (tset element :time 0))

(fn button-draw [text element]
    (var quad (grid-to-quad {:x 12 :y 5 :w 7 :h 2} 8))
    ;; (local step 1200)
    ;; (if (< element.time step) (set  quad (grid-to-quad {:x 12 :y 5 :w 7 :h 2} 8))
    ;;     (< element.time (* 1.3 step))
    ;;     (set  quad (grid-to-quad {:x 12 :y 7 :w 7 :h 2} 8))
    ;;     (< element.time (* 2.3 step))
    ;;     (set  quad (grid-to-quad {:x 12 :y 9 :w 7 :h 2} 8))
    ;;     (< element.time (* 2.6 step))
    ;;     (set  quad (grid-to-quad {:x 12 :y 7 :w 7 :h 2} 8))
    ;;     (do (tset element :time 0)
    ;;         (set  quad (grid-to-quad {:x 12 :y 5 :w 7 :h 2} 8)) )
    ;;     )
    (love.graphics.draw image quad element.x element.y)
    (love.graphics.setColor (/ 132 256) (/ 126 256) (/ 133 256) 1)
    (love.graphics.setFont font.normal-font)
    (love.graphics.printf text element.x (+ 2.6 element.y)  (* 5 element.w) "center" 0 0.2 0.2)
    (love.graphics.setColor 1 1 1 1))

(fn button-hover-draw [text element]
    ;;(pp text)
    ;;(pp element)
    (local quad (grid-to-quad {:x 12 :y 11 :w 7 :h 2} 8))
    (love.graphics.draw image quad element.x element.y)
    (love.graphics.setColor (/ 132 256) (/ 126 256) (/ 133 256) 1)
    (love.graphics.setFont font.normal-font)
    (love.graphics.printf text element.x (+ 2.6 element.y)  (* 5 element.w) "center" 0 0.2 0.2)
    (love.graphics.setColor 1 1 1 1))

(local background {:x 45
                   :y 4
                   :name "menu-background"
                   :id 1                   
                   :click-callback click-callback
                   :default-draw background-draw})

(local button     {:x (+ 45 (/ (* 8 5) 2))
                      :y (+ 40 (* 6 8))
                      :w (* 8 7)
                      :h (* 8 2)
                      :id 3
                      :name "button"
                      :default-callback element-timer-update
                      :click-callback quit-callback
                      :click-draw (partial button-hover-draw "QUIT")
                   :default-draw (partial button-draw "QUIT")
                   :hover-draw (partial button-hover-draw "QUIT")
                   :hover-callback button-hover-update})


(local resume     {:x (+ 45 (/ (* 8 5) 2))
                      :y  40
                      :w (* 8 7)
                      :h (* 8 2)
                      :id 4
                      :name "resume"
                      :default-callback element-timer-update
                      :click-callback resume-callback
                      :click-draw (partial button-hover-draw "RESUME")
                   :default-draw (partial button-draw "RESUME")
                   :hover-draw (partial button-hover-draw "RESUME")
                   :hover-callback button-hover-update})

(local restart     {:x (+ 45 (/ (* 8 5) 2))
                      :y (+ 40 (* 3 8))
                      :w (* 8 7)
                      :h (* 8 2)
                      :id 4
                      :name "restart"
                      :default-callback element-timer-update
                      :click-callback restart-callback
                      :click-draw (partial button-hover-draw "RESTART")
                   :default-draw (partial button-draw "RESTART")
                   :hover-draw (partial button-hover-draw "RESTART")
                   :hover-callback button-hover-update})


(local title {:x 45
              :y 15
              :w (* 5 96)
              :id 2
              :name "title"
              :click-callback click-callback
              :default-draw title-draw})



{:1 background
    :2 title
    :3 button
    :4 resume
    :5 restart
}
