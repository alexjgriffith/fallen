(local sounds {;;:bgm {:type :music :source (love.audio.newSource "assets/ear_tropic_of_cancer_2.wav" "stream")}
               :opening {:type :music :source (love.audio.newSource "assets/cobalt.mp3" "stream")}
               ;;:click {:type :sfx :source (love.audio.newSource "assets/click.wav" "static")}
               :bell1 {:type :sfx :source (love.audio.newSource "assets/bell_ding1.wav" "static")}
               :bell2 {:type :sfx :source (love.audio.newSource "assets/bell_ding2.wav" "static")}
               :bell3 {:type :sfx :source (love.audio.newSource "assets/bell_ding3.wav" "static")}
               :bell4 {:type :sfx :source (love.audio.newSource "assets/bell_ding4.wav" "static")}
                    })

(local sound-levels {:music 0.4 :sfx 0.1})

;; (: sounds.bgm.source :setLooping true)

(: sounds.opening.source :setLooping true)

;; (: sounds.click.source :setPitch 0.7)

(fn set-sound-levels []
    (each [key value (pairs sound-levels)]
          (lume.map sounds
                    (fn [x]
                        (when (= x.type key)
                          (: x.source :setVolume value))))))

(set-sound-levels)

{:play (fn play [name]
           (when (and (not (: (. sounds name :source) :isPlaying))
                      (not (love.filesystem.getInfo "mute")))
             (: (. sounds name :source) :play)))
       :rewind (fn [name]
                  (: (. sounds name :source) :seek 0))
 :update-sound-levels (fn [type level]
                          (when (and type level)
                            (tset sound-levels type level))
                          (set-sound-levels))
 :stop (fn [name] (: (. sounds name :source) :stop))}
