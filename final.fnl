(local image (love.graphics.newImage (.. "assets/Windows_9X_BSOD.png")))


(local sound (require "sound"))

{:draw (fn[]          
          (love.graphics.rectangle "fill" 0 0 900 600)
          (love.graphics.draw image 130 100)
          (love.graphics.setColor 0 0 0 1)
          (love.graphics.setFont font.final-font)


          (love.graphics.printf "Turns out it was a simulation after all" 100 500 700 "center")
          )
       :activate (fn[dt]
                    (sound.stop :opening))
       :update (fn[dt])
       :keypressed (fn[key] (when (= key "escape") (love.event.quit)))}
