(local anim8 (require "lib.anim8"))

(local subtile {})

(fn subtile.newGrid [...]
    (anim8.newGrid ...))

(fn subtile.square4 [sample-grid x y]
    (sample-grid x y x (+ 1 y) (+ x 1) y (+ 1 x) (+ 1 y)))

(fn subtile.square10 [sample-grid x y a b]
    (sample-grid x y x (+ 1 y) (+ 1 x) y (+ 1 x) (+ 1 y)
                 a       b a       (+ 1 b) a       (+ 2 b)
                 (+ 1 a) b (+ 1 a) (+ 1 b) (+ 1 a) (+ 2 b)
                 (+ 2 a) b (+ 2 a) (+ 1 b) (+ 2 a) (+ 2 b)))

(fn subtile.rect [sample-grid x y w h]
    (let [squares {}]
      (for [i 0 (- w 1)]
           (for [j 0 (- h 1)]
                (tset squares (+ 1(# squares)) (+ x i))
                (tset squares (+ 1(# squares)) (+ y j))))
      (sample-grid (unpack squares) )))


;; batchSprite <- love.graphics.newSpriteBatch
(fn subtile.batch [batchSprite size x y tile-table [tr tl br bl] ?offset-x]
    (local offset-x (or ?offset-x 0))
      (: batchSprite :add (. tile-table tr)
         (+ offset-x (* (* x 2) size)) (* (* y 2) size))
      (: batchSprite :add (. tile-table tl)
         (+ offset-x (* (+ (* x 2) 1) size)) (* (* y 2) size))
      (: batchSprite :add (. tile-table br)
         (+ offset-x (* (* x 2) size)) (* (+ (* y 2) 1) size))
      (: batchSprite :add (. tile-table bl)
         (+ offset-x (* (+ (* x 2) 1) size)) (* (+ (* y 2) 1) size)))

(fn subtile.batch-rect [batchSprite size x y tile-table w h map-width ?offset-x]
    (local offset-x (or ?offset-x 0))
    (var k 0)
    (var xp x)
    (for [i 0  (- w 1) ]
         (for [j 0 (- h 1)]
              (set k (+ 1 k))
              (if (>= (+ (* x 2) i) (* map-width 2))
                  (set xp (- x map-width))
                  (< (+ (* x 2) i) 0)
                  (set xp (- map-width x))
                  (set xp x)
                  )
              (: batchSprite :add (. tile-table k)
                 (+ offset-x (* (+ (* xp 2) i)  size)) (* (+ (* y 2) j)  size)))))

(fn subtile.fixed [_]
    [1 3 2 4])

(fn subtile.match [neigh]
    (let [sel
          (fn [x y z] (+ 1 x (* 2 y) (* 4 z)))
          a
          (. [5 6 5 6 8 4 8 9] (sel neigh.up neigh.up-left neigh.left))
          b
          (. [11 12 11 12 8 2 8 9] (sel neigh.up neigh.up-right neigh.right))
          c
          (. [7 6 7 6 10  3 10 9] (sel neigh.down neigh.down-left neigh.left))
          d
          (. [13 12 13 12 10 1 10 9](sel neigh.down neigh.down-right neigh.right))]
      [a b c d]))


;; debugging only
(fn subtile.neighbour-options []
    (let [tab {}]
      (for [i 0 255]
           (tset tab i {})
           (each [key value
                      (ipairs [:right :left
                                      :up :down
                                      :up-right :up-left
                                      :down-right :down-left])]                 
                 (tset  tab i value
                        (% (math.floor(/ i (math.pow 2 (- key 1)) )) 2))))
       tab))

subtile
